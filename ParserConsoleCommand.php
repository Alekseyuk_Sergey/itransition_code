<?php

/**
 * Class ParserConsoleCommand
 *
 * Класс содержит методы, которые используются для работы с API парсинга
 */
abstract class ParserConsoleCommand extends CAppConsoleCommand
{
    #region Константы
    const OPERATION_SUM = '+';
    const OPERATION_DIFF = '-';
    const OPERATION_MULT = '*';
    const OPERATION_DIV = '/';

    const FUNC_DEFAULT_VALUE = 'setDefaultValue';
    const FUNC_SPLIT = 'splitStringBySymbol';
    const FUNC_NUMBER_FROM_STRING = 'getNumberFromString';
    const FUNC_STRING_FROM_NUMBERS = 'getStringFromNumbersMergedWithSymbol';
    const FUNC_MATH = 'getMathOperation';
    const FUNC_CONVERT = 'convertSize';
    const FUNC_GET_ELEMENT_FROM_ARRAY = 'getElementFromArray';
    const FUNC_REPLACE_STRING = 'replaceStringByString';
    const FUNC_GET_IF_IN_ARRAY = 'getIfInArray';
    #endregion

    #region Поля
    /** @var string адресс API проекта, куда парсим товары */
    protected $apiHost = 'https://platform-api.dom.by';

    protected $apiAntiGateKey = '02a7884d155f8816eb3169fbb13192eb';

    /** @var string ключ авторизации для API */
    protected $authKey = '0b805cc5f38b9445c079c6a6c9481482';

    /** @var bool флажок, который указывает на необходимость обновлять товары при совпадении или пропускать */
    protected $updateProducts = false;

    /** @var  integer id рубрики, которую парсим */
    protected $rubricId;

    /** @var  integer id бренда, который парсится в данный момент */
    protected $brandId;

    /** @var  array массив соотношений название бренда - id бренда */
    protected $brandInfo;

    /** @var  integer id коллекции, которую вставили в БД */
    protected $collectionId;

    /** @var  string название коллекции, с которой работаем в данный момент */
    protected $collectionTitle;

    /** @var array массив фильтров для данной рубрики, полученный из API */
    protected $filters = [];

    /** @var array "архив" фильтров для рубрик, которые были запрошены в процессе работы парсера */
    protected $filtersInfo = [];

    /** @var  string адрес прокси сервера */
    protected $proxyUrl = 'http://site-esp4ik.rhcloud.com/?url=';

    /** @var string адрес прокси для загрузки фото */
    protected $photoProxy = 'http://php-proxy.net/?q=';

    /** @var string user agent */
    protected $userAgent = '';

    /** @var array массив соответствий фильтров на Яндекс Маркете и на dom.by + параметры парсинга значений */
    protected $features = [];

    /** @var array массив значений фильтров по-умолчанию "Фильтр" - "значение по умолчанию" */
    protected $defaultFilterValue = [];

    /** @var int счетчик сохраненнных товаров */
    protected $counter = 0;

    /** @var array массив соответствий названия города и id */
    protected $citiesInfo = [];

    /** @var array массив соответствий названия станции и id */
    protected $metroStationsInfo = [];
    #endregion

    protected $placeId;

    /**
     * Главный экшн
     */
    public function actionIndex(array $range = null)
    {
        // Подготовительные действия перед парсингом
        $this->setParsingOptions();

        // Запускаем парсинг
        $offset = isset($range[0]) ? $range[0] : null;
        $limit = isset($range[1]) ? $range[1] : null;

        $this->actionParse($offset, $limit);
    }

    /**
     * Метод запускающий парсинг
     * @return mixed
     */
    abstract protected function actionParse($offset = null, $limit = null);


    protected function getFileHash($url)
    {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL, $url);
        $data = curl_exec($ch);
        //  curl_close($ch);
        $path = '../../../2.jpg';
        $data = md5($data);
        curl_close($ch);

        return $data;
    }

    protected function getEscapedString($str)
    {

        $str = trim(str_replace(["\r\n", "\r", "\n"], '', $str));

        return $str;
    }

    protected function getAttribute($Xpath, $decode = false, $paid = false, $items = false, $delimiter = false)
    {

        if ($paid == true)
        {
            $paid = 'Бесплатник';
            if ($Xpath->length > 0)
            {
                $paid = 'Платник';
            }

            return $paid;
        }
        $result = '';
        if ($Xpath->length > 0)
        {
            if ($items == false)
            {
                $result = $this->getEscapedString($Xpath->item(0)->nodeValue);
            }
            else
            {
                foreach ($Xpath as $item)
                {
                    if ($delimiter == false)
                    {
                        $result .= $this->getEscapedString($item->nodeValue);
                    }
                    else
                    {
                        $result .= $this->getEscapedString($item->nodeValue) . $delimiter;
                    }
                }
            }
        }
        if ($decode == false)
        {
            return ($result);
        }
        switch ($decode)
        {
            case 'utf8':
                return utf8_decode($result);
                break;
        }
    }


#region Вспомогательные методы для получения исходных страниц при парсинге

    /**
     * Метод устанавливает дополнительные настройки для работы парсера
     */
    protected function setParsingOptions()
    {

        Yii::app()->curl->setOption(CURLOPT_TIMEOUT, 900);
        Yii::app()->curl->setOption(CURLOPT_CONNECTTIMEOUT, 0);
        Yii::app()->curl->setOption(CURLOPT_SSL_VERIFYHOST, 0);
        Yii::app()->curl->setOption(CURLOPT_SSL_VERIFYPEER, 0);
        // Глушим ошибки при работе с HTML файлом
        libxml_use_internal_errors(true);
    }

    /**
     * Метод создает объект класса DOMXPath (по заданному url страницы)
     *
     * @param string $url
     * @param bool $useProxy
     * @param bool $proxyUrl - если false, то прокси берется из поля $this->proxyUrl
     *
     * @return bool|DOMXPath
     */
    protected function getXPathDocumentByUrl($url, $useProxy = true, $proxyUrl = false, $iconv = false, $data = false)
    {
        $htmlPage = is_array($data) ? $this->getRequest($url, $useProxy, $proxyUrl, $data) : $this->getRequest($url, $useProxy, $proxyUrl);

        if (empty($htmlPage))
        {
            return false;
        }
        if ($iconv == true)
        {
            $htmlPage = iconv('cp1251', 'utf-8', $htmlPage);
        }
        // Создадим объект DomDocument для парсинга страницы
        $domDocument = new DOMDocument();
        $domDocument->loadHTML($htmlPage);
        // Найдем ссылки на подробное описание товаров(коллекций)
        $domXPath = new DOMXPath($domDocument);

        return !empty($domXPath) ? $domXPath : false;
    }

    protected function setCurlParams($proxy = null, $cookie = null)
    {
        Yii::app()->curl->setOption(CURLOPT_FOLLOWLOCATION, 1);
        Yii::app()->curl->setOption(CURLOPT_RETURNTRANSFER, 1);
        Yii::app()->curl->setOption(CURLOPT_HEADER, 1);
        Yii::app()->curl->setOption(CONNECTION_TIMEOUT, 5);
        Yii::app()->curl->setOption(CURLOPT_TIMEOUT, 5);
        Yii::app()->curl->setOption(CURLOPT_PROXY, $proxy);
        Yii::app()->curl->setOption(CURLOPT_POST, 1);

        if ($proxy == null)
        {
            Yii::app()->curl->setOption(CURLOPT_HTTPHEADER, ['Content-type: application/json']);
        }
        Yii::app()->curl->setOption(CURLOPT_COOKIE, null);

        if ($cookie != null)
        {
            Yii::app()->curl->setOption(CURLOPT_COOKIE, "__uid=" . $cookie);
        }
    }

    protected function sendAntiGateReCaptcha($siteKey, $address, $url)
    {
        $proxy = explode(":", $address);

        $data = '{
    "clientKey":"' . $this->apiAntiGateKey . '",
    "task":
        {
            "type":"NoCaptchaTask",
            "websiteURL":"' . $url . '",
            "websiteKey":"6Lc1dAoUAAAAABRDQqfrV44cPFVuK5XrBa2oF9h3",
            "proxyType":"http/https",
            "proxyAddress":"' . $proxy[0] . '",
            "proxyPort":"' . $proxy[1] . '",
            "userAgent":"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36"
        }
        }';
        Yii::app()->curl->setOption(CURLOPT_HEADER, false);

        $result = json_decode($this->getRequest('https://api.anti-captcha.com/createTask', false, false, $data));
        $solution = null;

        if ($result->errorId == (int)0)
        {
            $solution = $this->getAntiGateResult($result);
        }

        if ($solution != null)
        {
            $this->setCurlParams($address);

            $result = $this->getRequest('https://zoon.ru/js.php', false, false, [
                'area' => "captcha",
                'action' => 'check',
                'response' => $solution,
                'g-recaptcha-response' => $solution
            ]);

            if (is_int(stripos($result, '{"success":true}')))
            {
                return true;
            }

            return false;
        }

        return false;
    }

    protected function getAntiGateResult($result)
    {
        $data = '{
        "clientKey":"' . $this->apiAntiGateKey . '",
        "taskId": "' . $result->taskId . '"
        }';

        $result = json_decode($this->getRequest('https://api.anti-captcha.com/getTaskResult', false, false, $data));

        $counter = 0;

        while ($result->status == 'processing' && $counter <= 12)
        {
            $counter++;
            $result = json_decode($this->getRequest('https://api.anti-captcha.com/getTaskResult', false, false, $data));
            sleep(10);
        }

        DebugHelper::printR($result);

        if ($result->errorId == (int)0 && isset($result->status) && $result->status == 'ready')
        {
            return $result->solution->gRecaptchaResponse;
        }

        return null;
    }

    protected function writeBadProxy($proxy)
    {
        $file = '../../badproxy.csv';
        $resource = fopen($file, 'a+');
        fwrite($resource, $proxy . ';');
        fclose($resource);

    }

    /**
     * @param $url
     * @param bool $iconv
     * @return bool|DOMXPath
     */
    protected function getXPathDocumentByUrlViaIP($url, $iconv = false)
    {
        $htmlPage = '';
        $proxy = ProxySingleton::getInstance();

        while (!is_int(stripos($htmlPage, 'HTTP/1.1 200 OK')) || is_int(stripos($htmlPage, 'капчу')))
        {
           // $url = 'https://spb.zoon.ru/m/gingivektomiya_v_oblasti_1_zuba/';
            $address = $proxy->nextProxy();
            //$address = '46.148.127.128:8085';
            $this->setCurlParams($address);
            $htmlPage = $this->getRequest($url, false, false);

            if ($htmlPage == false)
            {
                DebugHelper::printR('Bad Ip - ' . $address);
                $this->writeBadProxy($address);
            }

            if (is_int(stripos($htmlPage, 'капчу')) && $htmlPage != false)
            {
                $cookie = $this->getCookie($htmlPage);
                $this->setCurlParams(null, $cookie);
                preg_match_all("!sitekey: {1,}'[a-zA-Z0-9]{1,}',!uis", $htmlPage, $matches);
                $sitekey = preg_match_all("![a-zA-Z0-9]{9,}!,uis", $matches[0][0], $matches)[0][0];

                if ($this->sendAntiGateReCaptcha($sitekey, $address, $url) == true)
                {
                    DebugHelper::printR('Problem Solved!');
                    $this->setCurlParams($address);
                    $htmlPage = $this->getRequest($url, false, false);
                }
            }
        }

        if (empty($htmlPage))
        {
            return false;
        }

        if ($iconv == true)
        {
            $htmlPage = iconv('cp1251', 'utf-8', $htmlPage);
        }
        // Создадим объект DomDocument для парсинга страницы
        $domDocument = new DOMDocument();
        $domDocument->loadHTML($htmlPage);
        // Найдем ссылки на подробное описание товаров(коллекций)
        $domXPath = new DOMXPath($domDocument);

        return !empty($domXPath) ? $domXPath : false;
    }

    protected function getCookie($page)
    {
        preg_match_all('/^Set-Cookie:\s*([^;]*)/mi', $page, $matches);
        $cookies = [];

        foreach ($matches[1] as $item)
        {
            parse_str($item, $cookie);
            $cookies = array_merge($cookies, $cookie);
        }

        return $cookies;
    }


    /**
     * @param string $urls
     * @param $rubrics
     * @return array
     */
    protected function getUrlsArrayRubrics($urls, $rubrics)
    {

        $urls = explode("\n", $urls);
        $rubrics = explode("\n", $rubrics);
        $resultArray = [];
        foreach ($urls as $i => $url)
        {
            $rubrics[$i] = preg_replace('!^А-Яа-я .,!', '', $rubrics[$i]);
            if (!empty($url))
            {
                $resultArray[$rubrics[$i]] = $url;
            }
        }
        $resultArray = array_filter($resultArray);

        return $resultArray;
    }

    /**
     * @param $phone
     * @param $delimiter
     * @return string
     */
    protected function processPhone($phone, $delimiter)
    {
        if (stripos($phone, $delimiter))
        {
            $phones = [];
            $number = explode(',', $phone);
            foreach ($number as $num)
            {
                $phones[] = substr(preg_replace('![^0-9]!', '', $num), -9);
            }
            $phone = implode(', ', array_filter($phones));

        }
        else
        {
            $phone = substr(preg_replace('![^0-9]!', '', $phone), -9);
        }

        return $phone;
    }

    protected function getState($XPath, $reverse = false)
    {
        if ($reverse == false)
        {
            return $XPath->length > 0 ? 'Платный' : 'Бесплатный';
        }
        else
        {
            return $XPath->length > 0 ? 'Бесплатный' : 'Платный';
        }
    }


    /**
     * @param $XPath
     * @param bool $pattern
     * @param bool $replacing
     * @param bool $strLength
     * @param bool $iconv
     * @param string $delimiter
     * @param bool $isArray
     * @param bool $uniqueArray
     * @return array|null|string
     */
    protected function getValue($XPath, $pattern = false, $replacing = false, $strLength = false, $iconv = false, $delimiter = ', ', $isArray = true, $uniqueArray = false)
    {
        $result = null;
        if ($XPath->length > 0)
        {
            $array = [];
            foreach ($XPath as $XPitem)
            {
                /** @var DOMNode $XPitem */
                $item = $XPitem->nodeValue;
                if ($iconv !== false)
                {
                    $item = utf8_decode($item);
                }
                if ($pattern !== false)
                {
                    $item = preg_replace("!" . $pattern . "!uis", '', $item);
                }

                if ($replacing !== false)
                {
                    $item = str_replace($replacing, '', $item);
                }

                if ($strLength !== false)
                {
                    $item = substr($item, $strLength);
                }
                $array[] = $this->getEscapedString($item);
                $array = $uniqueArray !== false ? array_unique($array) : $array;
                $array = array_filter($array, function ($element) {
                    return !empty($element);
                });
            }

            if ($isArray != false)
            {
                return $array;
            }
            $result = implode($delimiter, $array);

        }

        return $result;
    }

    protected function getSimpleValue($XPath, $pattern = false, $replacing = false, $strLength = false, $iconv = false, $delimiter = ', ', $isArray = false, $uniqueArray = false)
    {
        return $this->getValue($XPath, $pattern, $replacing, $strLength, $iconv, $delimiter, $isArray, $uniqueArray);
    }

    protected function getAssocName($XPathHref, $XPathName)
    {
        $result = null;
        if ($XPathHref->length > 0 && $XPathName->length > 0)
        {
            foreach ($XPathHref as $key => $item)
            {
                $result[$XPathName->item($key)->nodeValue] = $item->nodeValue;
            }
        }
        return $result;
    }


    /**
     * @param $urls
     * @return array
     */
    protected function getUrlsArray($urls)
    {
        $resultArray = explode("\n", $urls);

        return $resultArray;
    }

    /**
     * @param $url
     * @param bool $useProxy
     * @param bool $proxyUrl
     * @return mixed
     */
    protected function getRequest($url, $useProxy = true, $proxyUrl = false, $data = false)
    {
        echo $url . PHP_EOL;

        if ($useProxy === true)
        {
            $url = $this->getUrlWithProxy($url, $proxyUrl === false ? $this->proxyUrl : $proxyUrl);
        }

        if (!empty($this->userAgent))
        {
            $url .= '&agent=' . base64_encode(urlencode($this->userAgent));
        }

        try
        {
            return $data != false ? Yii::app()->curl->post($url, $data) : Yii::app()->curl->get($url);
        } catch (CException $e)
        {
            return false;
        }
    }

    /**
     * Метод кодирует url в base64 + привязывает адрес Proxy
     *
     * @param $url
     * @param $proxyUrl
     *
     * @return string
     */
    protected function getUrlWithProxy($url, $proxyUrl)
    {
        return $proxyUrl . base64_encode($url);
    }

#endregion

#region Методы для работы с API парсинга товаров
    /**
     * Метод получает список (устанавливает значение $this->filters) фильтров и их id по названию рубрики
     * @param $rubricTitle
     *
     * @return bool
     */
    protected function getRubricFilters($rubricTitle)
    {
        // Проверяем в "архиве" были ли уже запрошены данные фильтры
        if (!empty($this->filtersInfo[$rubricTitle]))
        {
            $this->filters = $this->filtersInfo[$rubricTitle]['filters'];
            $this->rubricId = $this->filtersInfo[$rubricTitle]['rubricId'];

            return true;
        }

        $time = microtime(true);

        // Получаем id рубрики и id шаблона фильтров
        $out = Yii::app()->curl->get($this->apiHost . '/parser/json/catalog/getRubricByTitle/?ignoreProfiling=1&authorizationKey=' . $this->authKey . '&title=' . urlencode($rubricTitle));

        echo Yii::t('parser', 'API запрос getRubricIdByTitle выполнен за: {time} секунд', [
                '{time}' => (microtime(true) - $time),
            ]) . "\n";

        $response = CJSON::decode($out);

        if (empty($response['response']['rubric']['id']) || empty($response['response']['rubric']['template']['id']))
        {
            echo Yii::t('parser', 'Пустой ответ API getRubricIdByTitle') . "\n";

            return false;
        }

        // Устанавливаем значение ID рубрики
        $this->rubricId = $response['response']['rubric']['id'];

        $this->filtersInfo[$rubricTitle]['rubricId'] = $this->rubricId;

        $time = microtime(true);

        // Получаем фильтры данной рубрики
        $out = Yii::app()->curl->get($this->apiHost . '/parser/json/catalog/getRubricTemplatesFilters/?ignoreProfiling=1&templateId=' . $response['response']['rubric']['template']['id'] . '&authorizationKey=' . $this->authKey);

        echo Yii::t('parser', 'API запрос getRubricTemplatesFilters выполнен за: {time} секунд', [
                '{time}' => (microtime(true) - $time),
            ]) . "\n";

        $response = CJSON::decode($out);

        if (empty($response['response']['rubricTemplatesFilters']))
        {
            echo Yii::t('parser', 'Пустой ответ API getRubricTemplatesFilters') . "\n";

            return false;
        }

        $this->filters = [];

        // Заполняем массив соостветствий: название фильтра - id фильтра
        foreach ($response['response']['rubricTemplatesFilters'] as $filter)
        {
            $this->filters[$filter['title']] = $filter['id'];
        }

        // Запоминаем фильтры
        $this->filtersInfo[$rubricTitle]['filters'] = $this->filters;

        return true;
    }

    /**
     * Метод возвращает id бренда по его названию
     * (+ записывает в массив $this->brandInfo соответствие: название бренда - id бренда)
     * @param $brandTitle
     * @param bool $updateData
     *
     * @return bool|integer
     */
    protected function getBrandIdByTitle($brandTitle, $updateData = false)
    {
        if (isset($this->brandInfo[$brandTitle]))
        {
            $this->brandId = $this->brandInfo[$brandTitle];

            return true;
        }

        $time = microtime(true);

        // Получаем id бренда
        $out = Yii::app()->curl->get($this->apiHost . '/parser/json/catalog/getBrandIdByTitle/?ignoreProfiling=1&authorizationKey=' . $this->authKey . '&title=' . urlencode($brandTitle));

        echo Yii::t('parser', 'API запрос getBrandIdByTitle выполнен за: {time} секунд', [
                '{time}' => (microtime(true) - $time),
            ]) . "\n";

        $response = CJSON::decode($out);

        if (!empty($response['response']['brand']['id']))
        {
            $this->brandInfo[$brandTitle] = $response['response']['brand']['id'];
            $this->brandId = $response['response']['brand']['id'];

            $updateData['id'] = $this->brandId;
        }

        if (!empty($updateData))
        {
            Yii::app()->curl->post($this->apiHost . '/parser/json/catalog/addBrands/?ignoreProfiling=1&authorizationKey=0b805cc5f38b9445c079c6a6c9481482', http_build_query(['brands' => [$updateData]]));
        }

        $out = Yii::app()->curl->get($this->apiHost . '/parser/json/catalog/getBrandIdByTitle/?ignoreProfiling=1&authorizationKey=' . $this->authKey . '&title=' . urlencode($brandTitle));

        $response = CJSON::decode($out);

        if (empty($response['response']['brand']['id']))
        {
            echo Yii::t('parser', 'Пустой ответ API getBrandIdByTitle') . PHP_EOL;

            return false;
        }

        $this->brandId = $response['response']['brand']['id'];

        return true;
    }

    /**
     * Метод добавляет коллекцию через API
     *
     * @param $title string название коллекции
     * @param bool $printInfo
     * @param bool|string $photoUrl string url фото
     * @param bool $description
     *
     * @return bool
     */
    protected function saveCollection($title, $printInfo = false, $photoUrl = false, $description = false)
    {
        $out = Yii::app()->curl->get($this->apiHost . '/parser/json/catalog/getCollection/?ignoreProfiling=1&authorizationKey=0b805cc5f38b9445c079c6a6c9481482&brandId=' . $this->brandId . '&title=' . urlencode($title));

        if (!empty($out))
        {
            $response = CJSON::decode($out);

            $postData = [];

            if (!empty($response['response']['collection']))
            {
                // Если нужной рубрики нет, то добавим id коллекции в пост запрос, чтобы обновить коллекцию
                $postData['id'] = $response['response']['collection']['id'];
            }

            $postData['title'] = $title;
            $postData['status'] = 1;
            $postData['brandId'] = $this->brandId;
            $postData['rubrics'] = [
                [
                    'rubricId' => $this->rubricId,
                ],
            ];
            $postData['proxy'] = $this->photoProxy;

            if (!empty($photoUrl))
            {
                $postData['logoUrl'] = $photoUrl;
            }

            if (!empty($description))
            {
                $postData['description'] = $description;
            }

            $data = [];
            $data['collections'][] = $postData;

            $out = Yii::app()->curl->post($this->apiHost . '/parser/json/catalog/addCollections/?ignoreProfiling=1&authorizationKey=0b805cc5f38b9445c079c6a6c9481482', http_build_query($data));

            if ($printInfo == true)
            {
                DebugHelper::pr(CJSON::decode($out), true);
            }

            $out = Yii::app()->curl->get($this->apiHost . '/parser/json/catalog/getCollection/?ignoreProfiling=1&authorizationKey=0b805cc5f38b9445c079c6a6c9481482' . '&title=' . urlencode($title) . '&brandId=' . $this->brandId);

            $response = CJSON::decode($out);

            if (!empty($response['response']['collection']['id']))
            {
                $this->collectionTitle = $title;
                $this->collectionId = $response['response']['collection']['id'];

                return true;
            }
            else
            {
                return false;
            }
        }

        return false;
    }

    /**
     * Метод отправляет продукт на API и обрабатывает ответ
     *
     * @param $postData
     * @param bool $printInfo
     *
     * @return bool
     */
    protected function saveProduct($postData, $printInfo = true)
    {
        $time = microtime(true);

        $out = Yii::app()->curl->get($this->apiHost . '/parser/json/catalog/getProductIdByUrl/?ignoreProfiling=1&authorizationKey=0b805cc5f38b9445c079c6a6c9481482&url=' . $postData['url']);

        echo Yii::t('parser', 'API запрос getProductIdByUrl выполнен за: {time} секунд', [
                '{time}' => (microtime(true) - $time),
            ]) . "\n";
        $out = json_decode($out);

        if ($out->response->product->id != null)
        {
            $jsonArray = $out;

            if (is_array($jsonArray) && (!empty($jsonArray['response']['product']['id'])))
            {
                // Если нужно обновить товар, то добавляем id в POST
                if ($this->updateProducts == true)
                {
                    $postData['id'] = $jsonArray['response']['product']['id'];
                } // Иначе возвращаем
                else
                {
                    return false;
                }
            }
        }

        $time = microtime(true);

        $out = Yii::app()->curl->post($this->apiHost . '/parser/json/catalog/addProducts/?ignoreProfiling=1&authorizationKey=0b805cc5f38b9445c079c6a6c9481482', http_build_query(['products' => [$postData]]));

        echo Yii::t('parser', 'API запрос addProducts выполнен за: {time} секунд', [
                '{time}' => (microtime(true) - $time),
            ]) . "\n";
        if (empty($out))
        {
            echo Yii::t('parser', 'Ответ API пустой') . "\n";

            return false;
        }

        $this->counter++;

        if ($printInfo == true)
        {
            echo Yii::t('parser', 'Сохранен {counter}-ый товар', ['{counter}' => $this->counter]) . ' ' . date('H:i:s d.m.Y') . "\n";

            $output = CJSON::decode($out);
            DebugHelper::pr($output, true);
        }

        return true;
    }

#endregion

#region Методы для работы с API парсинга заведений

    /**
     * Метод получает данные по городу из АПИ по названию
     * @param $city
     * @return bool|mixed
     */
    protected function getCity($city)
    {
        if (isset($this->citiesInfo[$city]))
        {
            return $this->citiesInfo[$city];
        }

        $time = microtime(true);

        // Получаем id бренда
        $out = Yii::app()->curl->get($this->apiHost . '/parser/json/catalog/getCityByTitle/?ignoreProfiling=1&authorizationKey=' . $this->authKey . '&title=' . urlencode($city));

        echo Yii::t('parser', 'API запрос getCityTitle выполнен за: {time} секунд', [
                '{time}' => (microtime(true) - $time),
            ]) . "\n";

        $response = CJSON::decode($out);

        if (empty($response['response']['city']['id']))
        {
            echo Yii::t('parser', 'Пустой ответ API getCityByTitle') . PHP_EOL;

            return false;
        }

        return $response['response']['city']['id'];
    }

    /**
     * Метод отправляет заведение на API и обрабатывает ответ
     *
     * @param $postData
     * @param bool $printInfo
     *
     * @return bool
     */
    protected function savePlace($postData, $printInfo = true)
    {
        $time = microtime(true);

        $out = Yii::app()->curl->post($this->apiHost . '/parser/json/catalog/addPlaces/?ignoreProfiling=1&authorizationKey=0b805cc5f38b9445c079c6a6c9481482', http_build_query(['places' => [$postData]]));
        echo Yii::t('parser', 'API запрос addPlaces выполнен за: {time} секунд', [
                '{time}' => (microtime(true) - $time),
            ]) . "\n";


        if (empty($out))
        {
            echo Yii::t('parser', 'Ответ API пустой') . "\n";

            return false;
        }

        $this->counter++;

        if ($printInfo == true)
        {
            echo Yii::t('parser', 'Сохранен {counter}-ое заведение', ['{counter}' => $this->counter]) . ' ' . date('H:i:s d.m.Y') . "\n";
            $out = CJSON::decode($out);
            $this->placeId = $out['response']['places']['0']['id'];
            DebugHelper::pr($out, true);
        }

        return true;
    }

#endregion

#region Методы, которые работают со спрашенными значениями фильтров
    /**
     * Метод работает со значениями фильтров в порядке, описанном в $this->features
     *
     * Процесс получения исходного значения описывается в массиве последовательных операций ($this->features), например:
     *
     *      'Материал' => array(
     *          'Материал корпуса (Варочные поверхности)' => array(
     *               array(self::FUNC_SPLIT => array(':', false)),
     *               array(self::FUNC_GET_ELEMENT_FROM_ARRAY => array(1)),
     *               array(self::FUNC_SPLIT => array(',', false)),
     *               array(self::FUNC_GET_ELEMENT_FROM_ARRAY => array(0)),
     *           ),
     *           'Материал окантовки/панели (Варочные поверхности)' => array(...)
     *
     *  т.е. в данном примере получаем, что парсер возьмет характеристику "Материал", и будет распаршивать её в 2 фильтра,
     *  причем над значением будут выполнены последовательно 4 операции (результат предыдущей ф-ии, является входящим значением для последующей)
     *  параметры указываются в array(':', false) (см. описание соответствующих методов)
     *
     * @param [] $postData массив со спаршенными данными
     * @param string $feature спаршенное название характеристики
     * @param string $value спаршенное значение характеристики
     *
     * @return mixed
     */
    protected function getFilterValue($postData, $feature, $value)
    {
        // Проверяем нужна ли нам эта характеристика
        if (isset($this->features[$feature]))
        {
            // Проходим по всем фильтрам, которые парсим из данной характеристики
            foreach ($this->features[$feature] as $filter => $operations)
            {
                // Запоминаем первоначальное значение на случай, если одна характеристика соответствует двум фильтрам на доме
                $iterationValue = $value;

                // Проверяем есть ли на нашем сервере такой фильтр
                if (isset($this->filters[$filter]))
                {
                    foreach ($operations as $operationArray)
                    {
                        // Проводим последовательные операции над значениеями характеристик, указанные в массиве $this->features
                        foreach ($operationArray as $operation => $args)
                        {
                            // Проверяем есть ли такая операция (метод)
                            if (is_callable([$this, $operation]))
                            {
                                // Проверяем правильность входных значений
                                if (!empty($iterationValue) || $operation == self::FUNC_DEFAULT_VALUE)
                                {
                                    // Выполняем необходимую операцию над значением
                                    $iterationValue = call_user_func([$this, $operation], $iterationValue, $args);
                                }
                            }
                        }
                    }

                    // Вставляем значение(я) в массив с фильтрами продукта
                    foreach ((array)$iterationValue as $val)
                    {
                        // Меняем все "да" на "есть"
                        $val = preg_replace("#да#ius", 'есть', $val);

                        $postData['filters'][] = [
                            'id' => $this->filters[$filter],
                            'value' => mb_strtolower($val, 'utf-8'),
                        ];
                    }
                }
            }
        }

        return $postData;
    }

    /**
     * Метод добавляет дефолтные значения фильтров, которые прописаны в $this->defaultFilterValue
     *
     * @param $postData
     *
     * @return mixed
     */
    protected function setDefaultValues($postData)
    {
        // Проходим по списку фильтров
        foreach ($this->filters as $filter => $id)
        {
            $found = false;
            $value = false;

            // Ищем в массиве со спаршенными данными текущий фильтр
            foreach (@$postData['filters'] as $productFilter)
            {
                // Если фильтр найден, то запоминаем его
                if ($productFilter['id'] == $id)
                {
                    $found = true;
                    $value = $productFilter['id'];
                }
            }

            // Если фильтр не был найден или было пустое значение, то пытаемся вставить значение фильтра по-умолчанию
            if (($found == false || empty($value)) && isset($this->defaultFilterValue[$filter]))
            {
                $postData['filters'][] = [
                    'id' => $id,
                    'value' => $this->defaultFilterValue[$filter],
                ];
            }
        }

        return $postData;
    }

#endregion

#region Методы, которые позволяют получить различную информацию из исходных значенйи характеристик (используются в методе getFilterValue)
    /**
     * Функция устанавливает дефолтное значение (для фильтра),
     * при отсутствии данной характеристики в описании товара на Яндексе
     *
     * @param $value
     * @param array $args [0] - дефолтное значение для фильтра,
     * [1] - true - если надо проставить дефолтное значение вне зависимости от текущего значения
     *
     * @return bool
     */
    protected function setDefaultValue($value, array $args)
    {
        if (empty($value) || !empty($args[1]))
        {
            return isset($args[0]) ? $args[0] : false;
        }

        return $value;
    }

    /**
     * Функция разбивает строку на подстроки заданным символом
     *
     * @param $string string входная строка
     * @param $args $args[0] - символ, которым разделяем строку $args[1] - true возвращает строку, false - возвращает массив
     *
     * @return array|string|bool
     */
    protected function splitStringBySymbol($string, array $args)
    {
        if (empty($args) || !is_array($args))
        {
            return false;
        }

        $symbol = isset($args[0]) ? $args[0] : '';
        $returnString = isset($args[1]) ? $args[1] : false;
        $stringArray = [];

        if (substr_count($string, $symbol) > 0)
        {
            foreach ((array)explode($symbol, $string) as $value)
            {
                $stringArray[] = trim($value);
            }
        }
        else
        {
            $stringArray[] = trim($string);
        }

        return $returnString == true ? trim(implode(' ', $stringArray)) : $stringArray;
    }

    /**
     * Функция получает число под заданным номером из строки
     *
     * @param $string string входная строка
     * @param $args $args[0] - индекс возвращаемого числового значения из строки (нумерация как в массиве)
     *
     * @return double|bool
     */
    protected function getNumberFromString($string, array $args)
    {
        if (empty($args) || !is_array($args))
        {
            return false;
        }

        $index = isset($args[0]) ? $args[0] : 0;

        preg_match_all('!-?\d+[\.,]?\d*!', $string, $value);

        return isset($value[0][$index]) ? (double)str_replace(',', '.', $value[0][$index]) : false;
    }

    /**
     * Функция парсит все числа из строки и объединяет их заданным символом
     *
     * @param $string string входная строка, содержащая числа
     * @param $args $args[0] - символ, которым соединяем числа
     *
     * @return string|bool
     */
    protected function getStringFromNumbersMergedWithSymbol($string, array $args)
    {
        if (empty($args) || !is_array($args))
        {
            return false;
        }

        $symbol = isset($args[0]) ? $args[0] : '';

        preg_match_all('!\d+\.*\d*!', $string, $values);

        $string = '';

        foreach ((array)$values[0] as $key => $value)
        {
            $string .= $value;

            if ($key < count((array)$values[0]) - 1)
            {
                $string .= $symbol;
            }
        }

        return !empty($string) ? $string : false;
    }

    /**
     * Функция выполняет заданную арифметическую операцию
     *
     * @param $firstNumber
     * @param $args $args[0] - второе число в арифметической операции, $args[1] - арифметическая операция
     *
     * @return number|bool
     */
    protected function getMathOperation($firstNumber, array $args)
    {
        if (empty($args) || !is_array($args) || !isset($args[0]) || !isset($args[1]))
        {
            return false;
        }

        $secondNumber = $args[0];
        $operation = $args[1];

        switch ($operation)
        {
            case self::OPERATION_SUM:
                return $firstNumber + $secondNumber;
            case self::OPERATION_DIFF:
                return $firstNumber - $secondNumber;
            case self::OPERATION_MULT:
                return $firstNumber * $secondNumber;
            case self::OPERATION_DIV:
                return $firstNumber / $secondNumber;
            default:
                return false;
        }
    }

    /**
     * Функция конвертирует (арифметические операции) размер продукта
     *
     * @param $string string входная строка, содержащая данные о размере
     * @param $args
     *
     * @return string|bool
     */
    protected function convertSize($string, array $args)
    {
        if (empty($args) || !is_array($args) || !isset($args[0]) || !isset($args[1]))
        {
            return false;
        }

        $number = $args[0];
        $operation = $args[1];

        preg_match_all('!\d+\.*\d*!', $string, $values);

        $result = '';

        foreach ((array)$values[0] as $key => $value)
        {
            switch ($operation)
            {
                case self::OPERATION_SUM:
                    $result .= (string)($value + $number);
                    break;
                case self::OPERATION_DIFF:
                    $result .= (string)($value - $number);
                    break;
                case self::OPERATION_MULT:
                    $result .= (string)($value * $number);
                    break;
                case self::OPERATION_DIV:
                    $result .= (string)($value / $number);
                    break;
                default:
                    return false;
            }

            if ($key < count((array)$values[0]) - 1)
            {
                $result .= 'x';
            }
        }

        return (!empty($result)) ? $result : false;
    }

    /**
     * Функция возвращаяет n-ое значение из массива
     *
     * @param $array array входной массив
     * @param array $args - $args[0] - позиция возвращаемого значения
     * @return bool|string
     */
    protected function getElementFromArray($array, array $args)
    {
        if (empty($args) || !is_array($args) || empty($array) || !is_array($array))
        {
            return false;
        }

        if (!is_numeric($args[0]))
        {
            return false;
        }

        return isset($array[$args[0]]) ? $array[$args[0]] : '';
    }

    /**
     * Функция служит для подстановки результирующей строки, если искомая содержит некоторую подстроку
     * Пример: если строка "Hello world" содержит подстроку "hello", то возвращаем "да", иначе "нет"
     * Метод регистронезависимый
     *
     * @param $string - для примера "Hello world"
     * @param array $args - для примера $args[0] - "hello", $args[1] - "да", $args[2] - "нет"
     *
     * @return bool
     */
    protected function replaceStringByString($string, array $args)
    {
        if (empty($args) || !is_array($args) || !is_string($string))
        {
            return false;
        }

        if (mb_stripos($string, $args[0], null, 'utf-8') === false)
        {
            return $args[2];
        }
        else
        {
            return $args[1];
        }
    }

    /**
     * Функция отдает то значение, которое было найдено в исходной строке
     * из списка переданных в массиве $args
     *
     * @param $string
     * @param array $args
     *
     * @return bool
     */
    protected function getIfInArray($string, array $args)
    {
        if (empty($string) || empty($args) || !is_array($args) || !is_string($string))
        {
            return false;
        }

        foreach ($args as $arg)
        {
            if (substr_count($string, $arg) > 0)
            {
                return $arg;
            }
        }

        return false;
    }
#endregion
}
 