<?php

Class ParserZoonuaCommand extends ParserConsoleCommand
{
    protected $apiHost = 'https://platform-api.103.ua'; //когда всё гуд поменять
    const SITE = 'http://zoon.com.ua/kiev/medical/?action=list&type=service&need%5B%5D=items&search_query_form=1&page=';
    private $url = 'http://zoon.com.ua/kiev/medical/?action=list&type=service&need%5B%5D=items&search_query_form=1&page=';
    const YANDEX_GEOCODE_API = 'https://geocode-maps.yandex.ru/1.x/?lotlanglanglot&format=json&kind=street&results=1&lang=ru_RU&geocode=';
    private $page = 1;
    /** @var array */
    protected $rubrics = 2; //заходить в админку и брать номер рубрики из urla
    protected $update = true;
    /** @var array */
    protected $cities = 10184;

    /** @var int|null id страны, в которую парсим заведения */
    protected $country = 9908;

    /** @var int|null id города, в который парсим заведения */
    protected $city = 10184;

    protected $days = [
        'пн', 'вт', 'ср', 'чт', 'пт', 'сб', 'вс', 'пн', 'пт',
    ];
    protected $time = [
        "пн" => '0',
        "вт" => '1',
        "ср" => '2',
        "чт" => '3',
        "пт" => '4',
        "сб" => '5',
        "вс" => '6',
    ];
    /** @var array перевод станций метро на украинский */
    protected $metro = [
        "Академгородок" => "Академмістечко",
        "Житомирская" => "Житомирська",
        "Святошин" => "Святошин",
        "Нивки" => "Нивки",
        "Берестейская" => "Берестейська",
        "Шулявская" => "Шулявська",
        "Политехнический институт" => "Політехнічний інститут",
        "Политехнический Институт" => "Політехнічний інститут",
        "Вокзальная" => "Вокзальна",
        "Университет" => "Університет",
        "Театральная" => "Театральна",
        "Крещатик" => "Хрещатик",
        "Арсенальная" => "Арсенальна",
        "Днепр" => "Дніпро",
        "Гидропарк" => "Гідропарк",
        "Левобережная" => "Лівобережна",
        "Дарница" => "Дарниця",
        "Черниговская" => "Чернігівська",
        "Лесная" => "Лісова",
        "Героев Днепра" => "Героїв Дніпра",
        "Минская" => "Мінська",
        "Оболонь" => "Оболонь",
        "Петровка" => "Петрівка",
        "Тараса Шевченко" => "Тараса Шевченка",
        "Контрактовая площадь" => "Контрактова площа",
        "Контрактовая Площадь" => "Контрактова площа",
        "Почтовая площадь" => "Поштова площа",
        "Почтовая Площадь" => "Поштова площа",
        "Площадь Независимости" => "Майдан Незалежності",
        "Площадь Льва Толстого" => "Площа Льва Толстого",
        "Олимпийская" => "Олімпійська",
        "Дворец Украина" => "Палац «Україна»",
        "Лыбедская" => "Либідська",
        "Демиевская" => "Деміївська",
        "Голосеевская" => "Голосіївська",
        "Васильковская" => "Васильківська",
        "Выставочный центр" => "Виставковий центр",
        "Выставочный Центр" => "Виставковий центр",
        "Ипподром" => "Іподром",
        "Теремки" => "Теремки",
        "Сырец" => "Сирець",
        "Дорогожичи" => "Дорогожичі",
        "Лукьяновская" => "Лук'янівська",
        "Золотые ворота" => "Золоті ворота",
        "Дворец спорта" => "Палац спорту",
        "Дворец Спорта" => "Палац спорту",
        "Кловская" => "Кловська",
        "Печерская" => "Печерська",
        "Дружбы народов" => "Дружби народів",
        "Дружбы Народов" => "Дружби народів",
        "Выдубичи" => "Видубичі",
        "Славутич" => "Славутич",
        "Осокорки" => "Осокорки",
        "Позняки" => "Позняки",
        "Харьковская" => "Харківська",
        "Вырлица" => "Вирлиця",
        "Бориспольская" => "Бориспільська",
        "Красный хутор" => "Червоний хутір",
        "Красный Хутор" => "Червоний хутір",
    ];
    protected $proxyUrl = 'http://php-proxy.net/index.php?q=';

    protected function getUrlWithProxy($url, $proxyUrl)
    {
        return $proxyUrl . base64_encode($url);
    }

    public function actionParse($offset = null, $limit = null)
    {
        $this->parse($this->url . $this->page);
    }

    protected function parse($url)
    {
        $domXPath = $this->getXPathDocumentByUrl($url);
        if (empty($domXPath))
        {
            return false;
        }

        $places = $domXPath->query("//a[@class='js-item-url']/@href");
        if ($places->length < 1)
        {
            return false;
        }
        $metroXPath = $domXPath->query("//address[@class='invisible-links']/a");
        $placesNames = $domXPath->query("//a[@class='js-item-url']");
        $placesType = $domXPath->query("//address[@class='invisible-links gray']/a");
        foreach ($places as $k => $item)
        {
            $this->sendItem($item->nodeValue, $this->getEscapedString(utf8_decode($placesType->item($k)->nodeValue)), $this->getEscapedString(utf8_decode(preg_replace("!" . utf8_encode('&nbsp;') . "!", ' ', $placesNames->item($k)->nodeValue))), $this->getEscapedString(utf8_decode($metroXPath->item($k)->nodeValue)));
        }

        $this->page++;
        sleep(10);
        $this->parse($this->url . $this->page);
    }

    private function sendItem($url, $text, $name, $metro)
    {

        $domXPath = $this->getXPathDocumentByUrl($url);
        if (empty($domXPath))
        {
            return false;
        }
        $data = [
            'place' => ['type' => 'default'],
            'placeInfo' => $this->getPlaceInfo($domXPath, $text, $name),
            'placesRubrics' => $this->getPlacesRubrics(),
            'placesCities' => $this->getPlacesCity(),
            'network' => $this->getNetwork($domXPath, $this->getPlaceInfo($domXPath, $text, $name)['title']),
            'driveway' => $this->getPlaceDriveway($domXPath, $metro)['0'],
            'address' => $this->getPlaceDriveway($domXPath, $metro)['1'],
            'placePhone' => $this->getPhones($domXPath),
            'worktime' => $this->getWorktime($domXPath),
            'placeContact' => $this->getContacts($domXPath),
            'filters' => [$this->getRegion($domXPath)],
        ];
        $this->savePlace($data);
        sleep(1);

    }

    protected function getRegion($domXPath)
    {
        $regionXPath = $domXPath->query("//dd[@class='simple-text invisible-links']//a[contains(.,'Район')]");
        if ($regionXPath->length < 1)
        {
            $regionXPath = $domXPath->query("//dd[@class='simple-text invisible-links']//a[contains(.,'район')]");
            if ($regionXPath->length < 1)
            {
                return null;
            }
        }
        $result = [
            'id' => '12',
            'value' => trim(str_ireplace('Район', '', $regionXPath->item(0)->nodeValue)),];

        return $result;
    }

    protected function getContacts($domXPath)
    {
        $webSiteXPath = $domXPath->query("//div[@class='service-website']");
        if ($webSiteXPath->length < 1)
        {
            return null;
        }
        $values = [
            'instagram' => 'Instagram',
            'facebook' => 'Facebook',
            'twitter' => 'Twitter',
            'vk' => 'Вконтакте',
        ];

        $contacts = [];

        foreach ($webSiteXPath as $item)
        {
            $info = explode(":", trim($item->nodeValue));
            if (in_array(trim($info[0]), $values))
            {
                $contacts[] = [
                    'type' => array_flip($values)[trim($info['0'])],
                    'value' => $values[array_flip($values)[trim($info['0'])]],
                    'href' => "http://" . trim($info['1']),
                ];
            }

        }

        return $contacts;
    }

    protected function getPlaceDriveway($domXPath, $metro)
    {
        $streetXPath = $domXPath->query("//address");

        if ($streetXPath->length > 0)
        {
            $address = $this->addressRemodel($streetXPath);
            $metro = $this->getMetro($metro);
            $result = $this->getCoords($address, $metro);
        }

        return empty($result) ? null : $result;
    }

    protected function getMetro($metro)
    {
        if (!empty($metro))
        {
            $metro = substr($metro, 4);
        }
        $metro = trim($metro);

        return !empty($metro) ? $this->metro[trim($metro)] : null;
    }

    protected function getPlaceInfo($domXPath, $titleText, $title)
    {
        $result = [];
        if (empty($title))
        {
            return false;
        }

        if (stripos($title, "лазерной хирургии Олега Колибабы") !== false)
        {
            $title = 'Центр пластической и лазерной хирургии Олега Колибабы';
        }
        if (stripos($title, "(Нативита)") !== false)
        {
            $title = 'Nativita(Нативита)';
        }
        if ($this->getPlaceUrl($title) == 'myata')
        {
            $title = 'Мята';
        }
        $result['title'] = $title;
        $result['text_type'] = $titleText;
        $result['url'] = $this->getPlaceUrl($result['title']);
        $result['site'] = $this->getSite($domXPath);
        $result['is_enable_comments'] = '1';
        $result['as_advertising'] = '1';
        $result['state'] = 'free';

        return $result;
    }

    protected function getCoords($address, $metro)
    {
        $url = self::YANDEX_GEOCODE_API . urlencode(str_replace(",", ' ', $address['address']));
        $response = Yii::app()->curl->get($url);
        $response = CJSON::decode($response);
        $coords = explode(" ", $response['response']['GeoObjectCollection']['featureMember']['0']['GeoObject']['Point']['pos']);
        $result = ['coord_x' => $coords['0'], 'coord_y' => $coords['1'], 'coord_center_x' => $coords['0'], 'coord_center_y' => $coords['1'], 'zoom' => '10', 'show_map' => '1'];
        if (!empty($response['response']['GeoObjectCollection']['featureMember'][0]['GeoObject']['name']))
        {
            $street = $response['response']['GeoObjectCollection']['featureMember'][0]['GeoObject']['name'];
            $street = trim(str_ireplace([
                'улица',
                'проспект',
                'переулок',
                'тракт',
                'магистраль',
                'бульвар',
                'аллея',
                'проезд',
            ], '', $street));

            $street = ['country_id' => $this->country, 'city_id' => $this->city, 'address' => !empty(explode(',', $street)['0']) ? explode(',', $street)['0'] : null, 'building' => !empty(explode(',', $street)['1']) ? preg_replace('![^0-9]!u', '', explode(',', $street)['1']) : null, 'metro_station' => $metro, 'handbook' => null, 'floor' => preg_replace('![^0-9]!u', '', $address['floor'])];
            $result = ['0' => $result, '1' => $street];
        }

        return $result;
    }


    protected function addressRemodel($streetXPath)
    {
        $address = $streetXPath->item(0)->nodeValue;
        $address = explode(',', $address);
        $result = [];
        $floor = '';
        foreach ($address as $addr)
        {
            if (strpos($addr, 'район') == false)
            {
                $result[] = $this->getEscapedString($addr);
            }
            if (strpos($addr, 'этаж'))
            {
                $floor = str_replace('этаж', '', $addr);
            }
        }

        $address = implode(', ', $result);
        $result = ['address' => $address, 'floor' => $floor];

        return $result;
    }

    protected function getNetwork($domXPath, $title)
    {
        return $domXPath->query("//dd[contains(.,'сеть')]")->length > 0 ? ['title' => $title] : null;
    }

    protected function getPlacesRubrics()
    {
        $rubrics = ['0' => ['rubric_id' => $this->rubrics]];

        return $rubrics;
    }

    protected function getPlacesCity()
    {
        $rubrics = ['0' => ['city_id' => $this->city]];

        return $rubrics;
    }

    protected function getPhones($domXPath)
    {
        $phonesXPath = $domXPath->query("//div[@class='service-phones-box']//span[@data-number]/@data-number");
        $phones = [];
        if ($phonesXPath->length < 0)
        {
            return null;
        }

        foreach ($phonesXPath as $phoneItem)
        {
            $phone = preg_replace('![^0-9+]!', '', $phoneItem->nodeValue);
            $phone = str_ireplace('+380', '', $phone);
            $phones[] = [
                'country_id' => $this->country,
                'phone_code' => substr($phone, 0, 2),
                'phone' => substr($phone, 2),
                'description' => null,
            ];
        }

        return $phones;
    }


    protected function getSite($domXPath)
    {
        $siteXPath = $domXPath->query("//div[@class='service-website']/a");
        $site = $siteXPath->length > 0 ? $siteXPath->item(0)->nodeValue : null;
        $site = substr($site, -1) == '/' ? substr($site, 0, -1) : $site;
        if ($site == 'site - http://svyatogo-pantelejmona-m..')
        {
            $site = 'http://svyatogo-pantelejmona-m';
        }

        return $site;
    }

    protected function getPlaceUrl($title)
    {
        if (empty($title))
        {
            return null;
        }

        return TranslitHelper::transliterate($title);
    }

    protected function getWorktime($domXPath)
    {
        $workTimeXPath = $domXPath->query("//div[strong[contains(.,'Время работы')]]");
        if ($workTimeXPath->length < 1)
        {
            return null;
        }
        $workTime = str_ireplace("Время работы:", '', $workTimeXPath->item(0)->nodeValue);
        $workTime = str_ireplace("по предварительной записи", '', $workTime);
        $workTime = str_ireplace("Без выходных", '', $workTime);

        if (stripos(trim($workTime), 'круглосуточно;') !== false || stripos(trim($workTime), 'круглосуточно') !== false)
        {
            $result['0'] = [
                'start_day' => '0',
                'end_day' => '6',
                'start_time' => '',
                'end_time' => '',
                'fulltime' => 1,
            ];

            return $result;
        }
        else
        {
            $workTime = str_ireplace(",", ";", $workTime);
            preg_match_all("![0-9]{1,2}(:|-)[0-9]{1,2}!", $workTime, $days);
            foreach ($days['0'] as $item)
            {
                if (strlen(preg_replace('![^0-9]!', '', $item)) > 0)
                {
                    $timeTry[] = preg_replace('!-!', ':', $item);
                }
            }
            preg_match_all("!(( |)[а-я]{2}-[а-я]{2}|[^а-яА-Я]{1}[а-я]{2}[^а-я0-9])!u", trim($workTime), $matches);

            $i = 0;
            if (!empty($timeTry))
            {
                foreach ($timeTry as $key => $value)
                {
                    if ($key % 2 == 0 || $key == 0)
                    {
                        if (count($matches['0']) !== 0)
                        {

                            if (count(explode('-', $matches['0'][$i])) < 2)
                            {
                                $result[$i] = ['start_day' => $this->time[preg_replace("![^а-я]!u", "", $matches['0'][$i])],
                                    'end_day' => $this->time[preg_replace("![^а-я]!u", "", $matches['0'][$i])],
                                    'start_time' => $timeTry[$key],
                                    'end_time' => $timeTry[$key + 1],
                                    'fulltime' => '0',];
                                $i++;
                            }
                            else
                            {
                                $day = explode('-', $matches['0'][$i]);
                                $result[$i] = ['start_day' => $this->time[preg_replace("![^а-я]!u", "", $day['0'])],
                                    'end_day' => $this->time[preg_replace("![^а-я]!u", "", $day['1'])],
                                    'start_time' => $timeTry[$key],
                                    'end_time' => $timeTry[$key + 1],
                                    'fulltime' => '0',];
                                $i++;
                            }

                        }
                        else
                        {
                            $result[$i] = ['start_day' => 0,
                                'end_day' => 6,
                                'start_time' => $timeTry[$key],
                                'end_time' => $timeTry[$key + 1],
                                'fulltime' => '0',];
                        }

                    }
                }
            }

        }

        return !empty($result) ? $result : null;
    }
}