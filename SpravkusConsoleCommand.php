<?php


abstract class SpravkusConsoleCommand extends ParserConsoleCommand
{
    const YANDEX_GEOCODE_API = 'https://geocode-maps.yandex.ru/1.x/?format=json&kind=street&results=1&lang=ru_RU&geocode=';
    const YANDEX_METRO_GEOCODE = 'https://geocode-maps.yandex.ru/1.x/?format=json&kind=metro&sco=latlong&geocode=';
    const YANDEX_OKRUG_GEOCODE = 'https://geocode-maps.yandex.ru/1.x/?format=json&kind=district&sco=latlong&geocode=';

    const YANDEX_GEOCODE_CITY = '';

    protected $urls;

    protected $days = [
        'пн' => 0,
        'вт' => 1,
        'ср' => 2,
        'чт' => 3,
        'пт' => 4,
        'сб' => 5,
        'вс' => 6,
    ];

    protected $worktimes;

    protected $cities;

    protected $city;

    protected $country = 3159;

    private $fp;

    private $rubric;

    private $rubrics = [];

    private $textType;

    protected $coordX;

    protected $coordY;

    protected $filtersCustom = [];

    protected $mainRubric;

    protected $filters1 = [];

    private $rubrics1 = [];

    private $currentCity;

    private $export = true;

    protected $apiHost = 'http://platform-api.relax.ru';

    protected function startParse()
    {
        foreach ($this->city as $key => $city)
        {
            $this->fp = fopen('../../../Spravkus_food_' . $city . "_" . time() . '.csv', 'w');
            $this->currentCity = $city;


            foreach ($this->urls AS $url => $array)
            {


                if (isset($array['filter']))
                {
                    foreach ($array['filter'] as $filter)
                    {
                        $this->filters1[count($this->filters1)] = $filter;
                    }
                }
                $this->rubrics1[count($this->rubrics1)] = [
                    'rubric_id' => $array['rubric'],
                    'is_main' => Constants::YES,
                ];
                $this->mainRubric = $array['rubric'];
                $this->textType = $array['textType'];
                $this->getPlaces($key . $url);

                $this->rubrics1 = [];
                $this->filters1 = [];
            }

            fclose($this->fp);
        }
    }

    private function getPlaces($url, $page = false)
    {
        $url = $page == true ? $url : $url . "/list";
        $domXPath = $this->getXPathDocumentByUrl($url);

        if (empty($domXPath))
        {
            return false;
        }

        $places = $domXPath->query("//td/a/@href");
        if ($places->length < 1)
        {
            return false;
        }

        foreach ($places as $item)
        {
            if(is_int(stripos($item->nodeValue,'.spravkus.com/')) || is_int(stripos($item->nodeValue,'http://spravkus.com/kz14/shymkent/')))
            {
                $this->getInfoFromItem($item->nodeValue);
            }else{
                $this->getInfoFromItem('http://spravkus.com'.$item->nodeValue);
            }
        }

        if ($page != true)
        {
            $pages = $this->getValue($domXPath->query("//ul/li/a/@href"), false, false, false, false, false, true);

            foreach ($pages as $key => $item)
            {
                if ($key > 0)
                {
                    $this->getPlaces($item, true);
                }
            }
        }

        return true;
    }

    private function getInfoFromItem($url)
    {
        $domXPath = $this->getXPathDocumentByUrl($url);

        if (empty($domXPath))
        {
            return;
        }
        $this->setFilters($domXPath);
        $data = [
            'place' => $this->getPlace(),
            'placeInfo' => $this->getPlaceInfo($domXPath),
            'placesRubrics' => $this->getPlacesRubrics(),
            'placesCities' => $this->getPlacesCities(),
            'network' => null,
            'address' => $this->getPlaceAddress($domXPath)['0'],
            'driveway' => $this->getPlaceDriveway(),
            'placePhone' => $this->getPlacePhones($domXPath),
            'worktime' => $this->getPlaceWorktime($domXPath),
            'filters' => $this->getPlaceFilters(),
        ];

        if (!empty($data['address']['address']) && !empty($data['address']['building']) && strlen($data['address']['building']) > 0 && $this->export != true)
        {
            $this->savePlace($data);
        }
        else
        {
            $this->exportPlaces($data);
        }

        $this->filtersCustom = [];
        $this->rubrics = [];
        $this->rubric = '';
        $this->coordX = '';
        $this->coordY = '';
    }

    /**Устанавливает фильтры
     * @param DOMXPath $domXPath
     */
    private function setFilters($domXPath)
    {
        $customValues = $this->getValue($domXPath->query("//ul[@class='firm-info-list']/li/ul/li"), false, false, false, true, false, true);
        if (!empty($customValues))
        {
            foreach ($customValues as $position => $value)
            {
                $this->setFiltersAndRubrics($value);
            }
        }
    }


    /**
     * Это самый трешак, запускается для каждого типа фильтров
     * @param $value
     * @return mixed
     */
    abstract protected function setFiltersAndRubrics($value);

    final protected function setKitFilter($haystack, $needle, $filtersArray)
    {
        if (is_int(stripos($haystack, $needle)))
        {
            $typesSet = str_ireplace($needle . " ", '', $haystack);
            $typesSet = explode(", ", $typesSet);
            foreach ($typesSet as $type)
            {
                if (isset($filtersArray[trim($type)]))
                {
                    if (!empty($filtersArray[trim($type)]['filters']))
                    {
                        foreach ($filtersArray[trim($type)]['filters'] as $filter)
                        {
                            $this->setOneFilter($filter['id'], $filter['value']);
                        }
                    }

                    if (!empty($filtersArray[trim($type)]['rubrics']))
                    {
                        foreach ($filtersArray[trim($type)]['rubrics'] as $rubric)
                        {
                            $this->setEasyRubric($rubric['id']);
                        }
                    }
                }
            }
        }

    }

    final protected function setFilter($filterId, $haystack, $needle, $value = 'да', $not = false, $rubricId = null, $opposite = null)
    {
        if ($opposite == null)
        {
            $opposite = 'нет';
        }

        if (is_int(stripos($haystack, $needle)))
        {
            if (is_int(stripos($haystack, '- нет')))
            {
                if ($not == true)
                {
                    $this->setOneFilter($filterId, $opposite);
                }
            }
            else
            {
                $this->setOneFilter($filterId, $value);

                if ($rubricId != null)
                {
                    $this->setEasyRubric($rubricId);
                }
            }

        }
    }

    final protected function setEasyFilter($filterId, $haystack, $needle, $value)
    {
        if (is_int(stripos($haystack, $needle)))
        {
            $this->setOneFilter($filterId, $value);
        }
    }

    final protected function setEasyFilterRubric($filterId, $haystack, $needle, $value, $rubricId)
    {
        if (is_int(stripos($haystack, $needle)))
        {
            $this->setOneFilter($filterId, $value);
            $this->setEasyRubric($rubricId);
        }
    }

    final protected function setEasyRubric($rubricId)
    {
        if ($rubricId != $this->mainRubric)
        {
            $this->rubrics[count($this->rubrics)] = [
                'rubric_id' => $rubricId,
                'is_main' => 0,
            ];
        }
    }

    final protected function setOneFilter($filterId, $value)
    {
        $this->filtersCustom[count($this->filtersCustom)] = [
            'id' => $filterId,
            'value' => $value,
        ];
    }

    /**
     * @param DOMXPath $domXPath
     * @param $company
     */
    private function parseFeeds($domXPath, $company)
    {
        $authors = $this->getValue($domXPath->query("//div[@class='item-data']/div[@class='name']"), false, false, false, true, false, true);
        $dates = $this->getValue($domXPath->query("//div[@class='item-data']/div[@class='time']"), false, false, false, true, false, true);
        $text = $this->getValue($domXPath->query("//div[@class='item-data']/div[@class='comment']"), false, false, false, true, false, true);

        if (empty($authors))
        {
            return;
        }
        if (count($authors) == count($dates) && count($authors) == count($text))
        {
            foreach ($authors as $key => $value)
            {
                $data = [
                    '0' => $value,
                    '1' => $dates[$key],
                    '2' => $text[$key],
                ];

                $this->export($data, $company);
            }
        }

    }

    private function export($data, $company)
    {
        $str = $this->placeId . ";" . $this->rubric . ';' . $company . ';' . $data['0'] . ";" . $data['1'] . ";" . $data['2'] . PHP_EOL;

        fwrite($this->fp, mb_convert_encoding($str, 'windows-1251', 'utf-8'));
        echo 'Добавлен отзыв' . PHP_EOL;
    }

    /**
     * @return array
     */
    private function getPlace()
    {
        return [
            'type' => Place::TYPE_DEFAULT,
        ];
    }

    /**
     * @param DOMXPath $item
     * @return array
     */
    private function getPlaceInfo($item)
    {

        return [
            'title' => $this->getPlaceTitle($item)['title'],
            'text_type' => $this->textType,
            'url' => $this->getPlaceUrl($item),
            'site' => $this->getPlaceSite($item),
            'is_enable_comments' => Constants::YES,
            'as_advertising' => Constants::YES,
            'state' => PlaceInfo::STATE_FREE,
        ];
    }

    /**
     * @param DOMXPath $item
     * @return string|null
     */
    private function getPlaceTitle($item)
    {
        $title = $this->getValue($item->query("//h1"), false, false, false, true);

        $result['title'] = $title;

        return !empty($result['title']) ? $result : null;
    }

    /**
     * @param DOMXPath $item
     * @return string|null
     */
    private function getPlaceUrl($item)
    {
        if (empty($title = $this->getPlaceTitle($item)['title']))
        {
            return null;
        }

        return TranslitHelper::transliterate($title);
    }

    /**
     * @param DOMXPath $item
     * @return string|null
     */
    private function getPlaceSite($item)
    {
        $site = $item->query("//ul[@class='firm-info-list']//a[@rel][@href]");

        return isset($site->item(0)->nodeValue) ? $site->item(0)->nodeValue : null;
    }

    /**
     * @return array
     */
    private function getPlacesRubrics()
    {
        foreach ($this->rubrics1 as $rubric)
        {
            $this->rubrics[count($this->rubrics)] = $rubric;
        }

        return $this->rubrics;
    }

    private function getPlaceFilters()
    {
        foreach ($this->filters1 as $filter)
        {
            $this->filtersCustom[count($this->filtersCustom)] = $filter;
        }

        return $this->filtersCustom;
    }

    /**
     * @return array
     */
    private function getPlacesCities()
    {
        $cities = [];
        foreach ($this->cities as $city)
        {
            $cities[] = [
                'city_id' => $city,
            ];
        }

        return $cities;
    }

    /**
     * @param DOMXPath $item
     * @return array
     */
    private function getPlaceAddress($item)
    {
        $address = $this->getValue($item->query("//div[@class='firm-info']"), false, false, false, false, false, false);
        $address = explode("+7", utf8_decode($address));
        $address = explode("8 (", $address[0]);
        $street = trim(preg_replace("!(.{1,}\W{1,4}Адрес:|\W{1,4}Телефон:|\W{1,4}тел./факс:|\W{1,4}Телефоны:|\W{1,4}Сайт:|\W{1,4}Соцсети:|\W{1,4}Режим работы:|\W{1,4}Эл. почта:)!", '', $address['0']));

        $newStreet = preg_replace("![^А-Яа-я .,0-9]!ui", '', $street);

        if ($newStreet == null)
        {
            $street = trim(substr($street, 0, -2));
        }

        $additionalSubjects = [
            'корпус',
            'корп',
            'строение',
            'сооружение',
            'стр',
            'соо',
            'влад',
        ];

        $additional = '';

        foreach ($additionalSubjects as $subject)
        {
            if (is_int(strpos($street, $subject)))
            {
                preg_match_all("!(" . $subject . ".|" . $subject . "[а-яА-Я]{1,})\s{0,5}[0-9]{1,}!uis", $street, $matches);

                if (!empty($matches[0][0]))
                {
                    if (substr($additional, -2) != substr($matches[0][0], 0, 2))
                    {
                        $additional .= $subject != 'влад' ? substr($matches[0][0], 0, 2) : 'вл';
                    }
                    $additional .= preg_replace("![^0-9]!", '', $matches[0][0]);
                }
            }
        }

        $url = self::YANDEX_GEOCODE_API . urlencode(self::YANDEX_GEOCODE_CITY . ', ' . $street);

        // Отправляем запрос на геокодер
        $response = Yii::app()->curl->get($url);

        $response = CJSON::decode($response);

        $street = trim(explode(',', $response['response']['GeoObjectCollection']['featureMember'][0]['GeoObject']['name'])['0']);
        $street = trim(str_ireplace([
            'улица',
            'проспект',
            'переулок',
            'тракт',
            'магистраль',
            'бульвар',
            'аллея',
            'проезд',
            'область',
            'район',
            'город',
            'микрор-н',
            'поселок',
            'агрогородок',
            'деревня',
            'сельсовет',
            'поселковый совет',
            'площадь',
            'шоссе',
            'набережная',
            'тупик',
            'канал',
            'линия',
            'аллея',
        ], [
            'ул.',
            'пр-т',
            'пер.',
            'тракт',
            'м-ль',
            'б-р',
            'ал.',
            'пр-д.',
            'обл.',
            'р-н.',
            'г.',
            'мкр.',
            'пос.',
            'аг.',
            'д.',
            'сс',
            'пс',
            'пл.',
            'ш.',
            'наб.',
            'туп.',
            'к.',
            'лин.',
            'ал.',
        ], $street));


        $building = str_ireplace("-", '', trim(explode(',', $response['response']['GeoObjectCollection']['featureMember'][0]['GeoObject']['name'])['1']));
        $floor = isset(explode(',', $response['response']['GeoObjectCollection']['featureMember'][0]['GeoObject']['name'])['2']) ? preg_replace("![^0-9]!", '', trim(explode(',', $response['response']['GeoObjectCollection']['featureMember'][0]['GeoObject']['name'])['2'])) : null;
        $x = trim(explode(' ', $response['response']['GeoObjectCollection']['featureMember'][0]['GeoObject']['Point']['pos'])['1']);
        $y = trim(explode(' ', $response['response']['GeoObjectCollection']['featureMember'][0]['GeoObject']['Point']['pos'])['0']);

        $this->coordX = $x;
        $this->coordY = $y;

        preg_match_all("![0-9]{1,3}/[0-9]{1,3}|[0-9]{1,3}[a-zA-Z]{1,}|[0-9]{1,3}[а-яА-Я]{1,3}|[0-9]{1,}[^a-zA-Zа-яА-Я]!uis", $building, $matches);

        return ['0' => [
            'country_id' => $this->country,
            'city_id' => $this->currentCity,
            'address' => $street != null ? $street : null,
            'building' => isset($matches[0][0]) ? str_ireplace("-", '', $this->getBuilding($matches[0][0])) . $additional : $this->getBuilding($building) . $additional,
            'metro_station' => $this->export ? '' : $this->getPlaceMetro($x, $y),
            'handbook' => null,
            'floor' => $floor,],
        ];
    }

    private function getBuilding($building)
    {
        if (substr($building, -2) == 'к' || substr($building, -2) == 'с' || substr($building, -2) == 'в')
        {
            return substr($building, 0, -2);
        }

        return $building;
    }

    abstract protected function getPlaceMetro($x, $y);

    /**
     * @return array
     */
    private function getPlaceDriveway()
    {
        return [
            'coord_x' => $this->coordY,
            'coord_y' => $this->coordX,
            'coord_center_x' => $this->coordY,
            'coord_center_y' => $this->coordX,
            'zoom' => 10,
            'show_map' => Constants::YES,
        ];
    }

    /**
     * @param DOMXPath $item
     * @return array
     */
    private function getPlacePhones($item)
    {
        $phone = $this->getValue($item->query("//a[@class='phone-link']/@tooltip-id"), false, false, false, false, false, true);
        $phones = [];

        foreach ($phone as $contact)
        {
            $phone = trim(str_ireplace('+', '', $contact));

            $phones[] = [
                'country_id' => $this->country,
                'phone_code' => substr($phone, 1, -7),
                'phone' => substr($phone, -7),
                'description' => null,
            ];
        }

        return $phones;
    }

    /**
     * @param DOMXPath $item
     * @return array
     */
    private function getPlaceWorktime($item)
    {
        $worktime = $this->getValue($item->query("//div[@class='firm-info']"), false, false, false, true, false, false);
        $worktime = preg_replace("!Сайт: .{1,}!", '', $worktime);
        $worktime = preg_replace("!Соцсети: .{1,}!", '', $worktime);
        $worktime = preg_replace("!Эл. почта: .{1,}!", '', $worktime);
        $worktime = preg_replace("!Категории: .{1,}!", '', $worktime);
        preg_match("!Режим работы.{1,}!", $worktime, $matches);
        $worktime = trim(preg_replace("!.{1,}\W{1,4}Режим работы:!", '', $worktime));
        $worktime[strlen($worktime) - 1] = '';

        $this->worktimes = [];

        if (!empty($worktime))
        {
            $schedule = explode(';', $worktime);
            $schedule = array_filter($schedule);
            if (count($schedule) > 1)
            {
                foreach ($schedule as $timeSet)
                {
                    $this->worktimes[count($this->worktimes)] = $this->rebuildDateTime(trim($timeSet)) != true ?: $this->rebuildDateTime(trim($timeSet));
                }
            }
            else
            {
                $this->worktimes[count($this->worktimes)] = $this->rebuildDateTime($schedule[0]) != true ?: $this->rebuildDateTime($schedule[0]);
            }
        }

        foreach ($this->worktimes as $worktime)
        {
            if ($worktime['fulltime'] == Constants::YES)
            {
                $this->filtersCustom[count($this->filtersCustom)] = [
                    'id' => '4253',
                    'value' => 'Есть',
                ];

            }
        }

        return $this->cleanArray($this->worktimes);
    }

    private function rebuildDateTime($string)
    {
        preg_match_all("![а-я]{1,2}-[а-я]{1,2} [0-9]{2}:[0-9]{2}-[0-9]{2}:[0-9]{2}!uis", $string, $matchesDays);

        if (isset($matchesDays[0][0]))
        {
            preg_match_all("![0-9]{2}:[0-9]{2}-[0-9]{2}:[0-9]{2}!uis", $string, $time);
            $time = $this->getTime($time[0][0]);

            preg_match_all("![а-я]{1,2}-[а-я]{1,2}!uis", $string, $days);
            $days = $this->getDays($days[0][0]);

            return array_merge($time, $days);
        }

        preg_match_all("![а-я]{1,2},[а-я]{1,2} [0-9]{2}:[0-9]{2}-[0-9]{2}:[0-9]{2}!uis", $string, $matchesDays1);

        if (isset($matchesDays1[0][0]))
        {
            preg_match_all("![0-9]{2}:[0-9]{2}-[0-9]{2}:[0-9]{2}!uis", $string, $time);
            $time = $this->getTime($time[0][0]);
            $string = str_ireplace($time, '', $string);

            preg_match_all("![а-я]{1,2}!uis", $string, $days);

            foreach ($days[0] as $day)
            {
                $days = $this->getDays($day);
                $this->worktimes[count($this->worktimes)] = array_merge($time, $days);

            }

            return true;
        }

        preg_match_all("!^[а-я]{1,2} [0-9]{2}:[0-9]{2}-[0-9]{2}:[0-9]{2}!uis", $string, $matchesDay);

        if (isset($matchesDay[0][0]))
        {
            preg_match_all("![0-9]{2}:[0-9]{2}-[0-9]{2}:[0-9]{2}!uis", $string, $time);
            $time = $this->getTime($time[0][0]);

            preg_match_all("![а-я]{1,2}!uis", $string, $days);
            $days = $this->getDays($days[0][0]);

            return array_merge($time, $days);
        }

        preg_match_all("!^[0-9]{2}:[0-9]{2}-[0-9]{2}:[0-9]{2}!uis", $string, $matchesTime);

        if (isset($matchesTime[0][0]))
        {
            $time = $this->getTime($matchesTime[0][0]);

            return array_merge($time, ['start_day' => '0', 'end_day' => '6']);
        }

        preg_match_all("!^круглосуточно!uis", $string, $matchesFullTime);

        if (isset($matchesFullTime[0][0]))
        {
            return [
                'start_time' => '00:00',
                'end_time' => '00:00',
                'start_day' => '0',
                'end_day' => '6',
                'fulltime' => '1',
            ];
        }

        return true;
    }

    private function cleanArray($array)
    {
        $result = [];
        $flag = true;
        foreach ($array as $item)
        {
            if (empty($result))
            {
                $result[] = $item;
            }

            foreach ($result as $value)
            {
                if ($value['start_time'] == $item['start_time'] && $value['end_time'] == $item['end_time'] && $value['end_day'] == $item['end_day'] && $value['start_day'] == $item['start_day'] && count($item) > 3)
                {
                    $flag = false;
                }
            }

            if ($flag != false && count($item) > 1)
            {
                $result[] = $item;
            }
            $flag = true;
        }

        return $result;
    }

    private function getTime($timeSet)
    {
        $timeSet = explode('-', $timeSet);
        $result = [
            'start_time' => trim($timeSet[0]),
            'end_time' => trim($timeSet[1]),
        ];

        $result['fulltime'] = $result['start_time'] == $result['end_time'] ? Constants::YES : Constants::NO;

        return !empty($result) ? $result : null;

    }

    private function getDays($days)
    {
        if (count(explode('-', $days)) > 1)
        {
            return [
                'start_day' => $this->days[explode('-', $days)[0]],
                'end_day' => $this->days[explode('-', $days)[1]],
            ];
        }
        else
        {
            if (count(explode(',', $days)) > 1)
            {
                return [
                    'start_day' => $this->days[explode(',', $days)[0]],
                    'end_day' => $this->days[explode(',', $days)[1]],
                ];
            }
            else
            {
                return [
                    'start_day' => $this->days[explode('-', $days)[0]],
                    'end_day' => $this->days[explode('-', $days)[0]],
                ];
            }
        }

    }

    protected function exportPlaces($data)
    {

        fwrite($this->fp, $this->exportPlace($data));
    }

    abstract protected function exportPlace($data);
#endregion
}
