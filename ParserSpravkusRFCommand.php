<?php

class ParserSpravkusRFCommand extends SpravkusConsoleCommand
{
    const YANDEX_GEOCODE_CITY = 'Москва';

    /** @var array дни недели */
    private $kitchen =
        [
            'австралийская' => [
                'filters' => [
                    ['id' => '3933', 'value' => 'австралийская'],
                    ['id' => '3943', 'value' => 'национальная'],
                ],
            ],
            'австрийская' => [
                'filters' => [
                    ['id' => '3933', 'value' => 'австрийская'],
                    ['id' => '3943', 'value' => 'национальная'],
                ],
            ],
            'авторская' => [
                'filters' => [
                    ['id' => '3943', 'value' => 'высокая (авторская)'],
                ],
            ],
            'азербайджанская' => [
                'filters' => [
                    ['id' => '3933', 'value' => 'азербайджанская'],
                    ['id' => '3943', 'value' => 'национальная'],
                ],
            ],
            'азиатская' => [
                'filters' => [
                    ['id' => '3933', 'value' => 'азиатская'],
                    ['id' => '3943', 'value' => 'национальная'],
                ],
            ],
            'американская' => [
                'filters' => [
                    ['id' => '3933', 'value' => 'американская'],
                    ['id' => '3943', 'value' => 'национальная'],
                ],
            ],
            'английская' => [
                'filters' => [
                    ['id' => '3933', 'value' => 'английская'],
                    ['id' => '3943', 'value' => 'национальная'],
                ],
            ],
            'арабская' => [
                'filters' => [
                    ['id' => '3933', 'value' => 'арабская'],
                    ['id' => '3943', 'value' => 'национальная'],
                ],
            ],
            'аргентинская' => [
                'filters' => [
                    ['id' => '3933', 'value' => 'аргентинская'],
                    ['id' => '3943', 'value' => 'национальная'],
                ],
            ],
            'армянская' => [
                'filters' => [
                    ['id' => '3933', 'value' => 'армянская'],
                    ['id' => '3943', 'value' => 'национальная'],
                ],
            ],
            'африканская' => [
                'filters' => [
                    ['id' => '3933', 'value' => 'африканская'],
                    ['id' => '3943', 'value' => 'национальная'],
                ],
            ],
            'баварская' => [
                'filters' => [
                    ['id' => '3933', 'value' => 'баварская'],
                    ['id' => '3943', 'value' => 'национальная'],
                ],
            ],
            'балканская' => [
                'filters' => [
                    ['id' => '3933', 'value' => 'балканская'],
                    ['id' => '3943', 'value' => 'национальная'],
                ],
            ],
            'белорусская' => [
                'filters' => [
                    ['id' => '3933', 'value' => 'белорусская'],
                    ['id' => '3943', 'value' => 'национальная'],
                ],
            ],
            'бельгийская' => [
                'filters' => [
                    ['id' => '3933', 'value' => 'бельгийская'],
                    ['id' => '3943', 'value' => 'национальная'],
                ],
            ],
            'болгарская' => [
                'filters' => [
                    ['id' => '3933', 'value' => 'болгарская'],
                    ['id' => '3943', 'value' => 'национальная'],
                ],
            ],
            'бразильская' => [
                'filters' => [
                    ['id' => '3933', 'value' => 'бразильская'],
                    ['id' => '3943', 'value' => 'национальная'],
                ],
            ],
            'венгерская' => [
                'filters' => [
                    ['id' => '3933', 'value' => 'венгерская'],
                    ['id' => '3943', 'value' => 'национальная'],
                ],
            ],
            'восточная' => [
                'filters' => [
                    ['id' => '3933', 'value' => 'восточная'],
                    ['id' => '3943', 'value' => 'национальная'],
                ],
            ],
            'вьетнамская' => ['filters' => [
                ['id' => '3933', 'value' => 'вьетнамская'], ['id' => '3943', 'value' => 'национальная'],],],
            'гавайская' => ['filters' => [
                ['id' => '3933', 'value' => 'гавайская'], ['id' => '3943', 'value' => 'национальная'],],],
            'греческая' => ['filters' => [
                ['id' => '3933', 'value' => 'греческая'], ['id' => '3943', 'value' => 'национальная'],],],
            'грузинская' => ['filters' => [
                ['id' => '3933', 'value' => 'грузинская'], ['id' => '3943', 'value' => 'национальная'],],],
            'датская' => ['filters' => [
                ['id' => '3933', 'value' => 'датская'], ['id' => '3943', 'value' => 'национальная'],],],
            'домашняя' => ['filters' => [
                ['id' => '3943', 'value' => 'домашняя'],],],
            'донская' => ['filters' => [
                ['id' => '3933', 'value' => 'донская'], ['id' => '3943', 'value' => 'национальная'],],],
            'еврейская' => ['filters' => [
                ['id' => '3933', 'value' => 'еврейская'], ['id' => '3943', 'value' => 'национальная'],],],
            'европейская' => ['filters' => [
                ['id' => '3933', 'value' => 'европейская'], ['id' => '3943', 'value' => 'национальная'],],],
            'индийская' => ['filters' => [
                ['id' => '3933', 'value' => 'индийская'], ['id' => '3943', 'value' => 'национальная'],],],
            'индонезийская' => ['filters' => [
                ['id' => '3933', 'value' => 'индонезийская'], ['id' => '3943', 'value' => 'национальная'],],],
            'иранская' => ['filters' => [
                ['id' => '3933', 'value' => 'иранская'], ['id' => '3943', 'value' => 'национальная'],],],
            'ирландская' => ['filters' => [
                ['id' => '3933', 'value' => 'ирландская'], ['id' => '3943', 'value' => 'национальная'],],],
            'испанская' => ['filters' => [
                ['id' => '3933', 'value' => 'испанская'], ['id' => '3943', 'value' => 'национальная'],],],
            'итальянская' => ['filters' => [
                ['id' => '3933', 'value' => 'итальянская'], ['id' => '3943', 'value' => 'национальная'],],],
            'кавказская' => ['filters' => [
                ['id' => '3933', 'value' => 'кавказская'], ['id' => '3943', 'value' => 'национальная'],],],
            'казахская' => ['filters' => [
                ['id' => '3933', 'value' => 'казахская'], ['id' => '3943', 'value' => 'национальная'],],],
            'караимская' => ['filters' => [
                ['id' => '3933', 'value' => 'караимская'], ['id' => '3943', 'value' => 'национальная'],],],
            'карибская' => ['filters' => [
                ['id' => '3933', 'value' => 'карибская'], ['id' => '3943', 'value' => 'национальная'],],],
            'китайская' => ['filters' => [
                ['id' => '3933', 'value' => 'китайская'], ['id' => '3943', 'value' => 'национальная'],],],
            'континентальная' => ['filters' => [
                ['id' => '3933', 'value' => 'континентальная'], ['id' => '3943', 'value' => 'национальная'],],],
            'корейская' => ['filters' => [
                ['id' => '3933', 'value' => 'корейская'], ['id' => '3943', 'value' => 'национальная'],],],
            'кубинская' => ['filters' => [
                ['id' => '3933', 'value' => 'кубинская'], ['id' => '3943', 'value' => 'национальная'],],],
            'латиноамериканская' => ['filters' => [
                ['id' => '3933', 'value' => 'латиноамериканская'], ['id' => '3943', 'value' => 'национальная'],],],
            'ливанская' => ['filters' => [
                ['id' => '3933', 'value' => 'ливанская'], ['id' => '3943', 'value' => 'национальная'],],],
            'марокканская' => ['filters' => [
                ['id' => '3933', 'value' => 'марокканская'], ['id' => '3943', 'value' => 'национальная'],],],
            'мексиканская' => ['filters' => [
                ['id' => '3933', 'value' => 'мексиканская'], ['id' => '3943', 'value' => 'национальная'],],],
            'молдавская' => ['filters' => [
                ['id' => '3933', 'value' => 'молдавская'], ['id' => '3943', 'value' => 'национальная'],],],
            'молекулярная' => ['filters' => [
                ['id' => '3943', 'value' => 'молекулярная (кулинарная физика)'],],],
            'монгольская' => ['filters' => [
                ['id' => '3933', 'value' => 'монгольская'], ['id' => '3943', 'value' => 'национальная'],],],
            'морская' => ['filters' => [
                ['id' => '4033', 'value' => 'рыбное'],],],
            'мясная' => ['filters' => [
                ['id' => '4033', 'value' => 'мясное'],],],
            'народов Севера' => ['filters' => [
                ['id' => '3933', 'value' => 'северная'], ['id' => '3943', 'value' => 'национальная'],],],
            'немецкая' => ['filters' => [
                ['id' => '3933', 'value' => 'немецкая'], ['id' => '3943', 'value' => 'национальная'],],],
            'осетинская' => ['filters' => [
                ['id' => '3933', 'value' => 'осетинская'], ['id' => '3943', 'value' => 'национальная'],],],
            'пакистанская' => ['filters' => [
                ['id' => '3933', 'value' => 'пакистанская'], ['id' => '3943', 'value' => 'национальная'],],],
            'паназиатская' => ['filters' => [
                ['id' => '3933', 'value' => 'паназиатская'], ['id' => '3943', 'value' => 'национальная'],],],
            'полинезийская' => ['filters' => [
                ['id' => '3933', 'value' => 'полинезийская'], ['id' => '3943', 'value' => 'национальная'],],],
            'польская' => ['filters' => [
                ['id' => '3933', 'value' => 'польская'], ['id' => '3943', 'value' => 'национальная'],],],
            'португальская' => ['filters' => [
                ['id' => '3933', 'value' => 'португальская'], ['id' => '3943', 'value' => 'национальная'],],],
            'прибалтийская' => ['filters' => [
                ['id' => '3933', 'value' => 'литовская'], ['id' => '3943', 'value' => 'национальная'],],],
            'русская' => ['filters' => [
                ['id' => '3933', 'value' => 'русская'], ['id' => '3943', 'value' => 'национальная'],],],
            'рыбная' => ['filters' => [
                ['id' => '4033', 'value' => 'рыбное'],],],
            'сербская' => ['filters' => [
                ['id' => '3933', 'value' => 'сербская'], ['id' => '3943', 'value' => 'национальная'],],],
            'сибирская' => ['filters' => [
                ['id' => '3933', 'value' => 'сибирская'], ['id' => '3943', 'value' => 'национальная'],],],
            'сингапурская' => ['filters' => [
                ['id' => '3933', 'value' => 'сингапурская'], ['id' => '3943', 'value' => 'национальная'],],],
            'скандинавская' => ['filters' => [
                ['id' => '3933', 'value' => 'скандинавская'], ['id' => '3943', 'value' => 'национальная'],],],
            'славянских народов' => ['filters' => [
                ['id' => '3933', 'value' => 'славянская'], ['id' => '3943', 'value' => 'национальная'],],],
            'словацкая' => ['filters' => [
                ['id' => '3933', 'value' => 'словацкая'], ['id' => '3943', 'value' => 'национальная'],],],
            'средиземноморская' => ['filters' => [
                ['id' => '3933', 'value' => 'средиземноморская'], ['id' => '3943', 'value' => 'национальная'],],],
            'таджикская' => ['filters' => [
                ['id' => '3933', 'value' => 'таджикская'], ['id' => '3943', 'value' => 'национальная'],],],
            'тайваньская' => ['filters' => [
                ['id' => '3933', 'value' => 'тайваньская'], ['id' => '3943', 'value' => 'национальная'],],],
            'тайская' => ['filters' => [
                ['id' => '3933', 'value' => 'тайская'], ['id' => '3943', 'value' => 'национальная'],],],
            'татарская' => ['filters' => [
                ['id' => '3933', 'value' => 'татарская'], ['id' => '3943', 'value' => 'национальная'],],],
            'тибетская' => ['filters' => [
                ['id' => '3933', 'value' => 'тибетская'], ['id' => '3943', 'value' => 'национальная'],],],
            'турецкая' => ['filters' => [
                ['id' => '3933', 'value' => 'турецкая'], ['id' => '3943', 'value' => 'национальная'],],],
            'узбекская' => ['filters' => [
                ['id' => '3933', 'value' => 'узбекская'], ['id' => '3943', 'value' => 'национальная'],],],
            'украинская' => ['filters' => [
                ['id' => '3933', 'value' => 'украинская'], ['id' => '3943', 'value' => 'национальная'],],],
            'филиппинская' => ['filters' => [
                ['id' => '3933', 'value' => 'филиппинская'], ['id' => '3943', 'value' => 'национальная'],],],
            'французская' => ['filters' => [
                ['id' => '3933', 'value' => 'французская'], ['id' => '3943', 'value' => 'национальная'],],],
            'чешская' => ['filters' => [
                ['id' => '3933', 'value' => 'чешская'], ['id' => '3943', 'value' => 'национальная'],],],
            'швейцарская' => ['filters' => [
                ['id' => '3933', 'value' => 'швейцарская'], ['id' => '3943', 'value' => 'национальная'],],],
            'шотландская' => ['filters' => [
                ['id' => '3933', 'value' => 'шотландская'], ['id' => '3943', 'value' => 'национальная'],],],
            'югославская' => ['filters' => [
                ['id' => '3933', 'value' => 'югославская'], ['id' => '3943', 'value' => 'национальная'],],],
            'якутская' => ['filters' => [
                ['id' => '3933', 'value' => 'якутская'], ['id' => '3943', 'value' => 'национальная'],],],
            'ямайская' => ['filters' => [
                ['id' => '3933', 'value' => 'ямайская'], ['id' => '3943', 'value' => 'национальная'],],],
            'японская' => ['filters' => [
                ['id' => '3933', 'value' => 'японская'], ['id' => '3943', 'value' => 'национальная'],],],

        ];

    protected $urls103 = [
        'medtsentry-i-kliniki' =>
            [
                'textType' => 'Медицинский центр',
                'rubric' => '2',
            ],
        'stomatologiya' =>
            [
                'textType' => 'Стоматология',
                'rubric' => '1',
            ],
        'meditsinskie-laboratorii' =>
            [
                'textType' => 'Лаборатория',
                'rubric' => '8',
            ],
        'lechebno-diagnosticheskie-tsentry' =>
            [
                'textType' => 'Консультативно-диагностический центр',
                'rubric' => '10158183',
            ],
        'reabilitatsionnye-tsentry' =>
            [
                'textType' => 'Реабилитационный центр',
                'rubric' => '10158243',
            ],

        'http://yellmed.ru/moskva/plasticheskie-hirurgii' =>
            [
                'textType' => 'Центр эстетической медицины',
                'rubric' => '10158163',
            ],
        'netraditsionnaya-meditsina' =>
            [
                'textType' => 'Центр нетрадиционной медицины',
                'rubric' => '10156316',
            ],
        'korrektsiya-zreniya' =>
            [
                'textType' => 'Офтальмологическая клиника',
                'rubric' => '10158213',
            ],
        'kardiologicheskie-tsentry' =>
            [
                'textType' => 'Кардиологический центр',
                'rubric' => '10158223',
            ],
        'psikhoterapevticheskaya-pomosch' =>
            [
                'textType' => 'Центр психологической помощи',
                'rubric' => '10158203',
            ],
        'narkologicheskie-kliniki' =>
            [
                'textType' => 'Центр лечения зависимостей',
                'rubric' => '106',
            ],
        'ginekologicheskie-kliniki' =>
            [
                'textType' => 'Гинекологическая клиника',
                'rubric' => '10158613',
            ],
        'polikliniki-dlya-vzroslykh' =>
            [
                'textType' => 'Поликлиника',
                'rubric' => '3',
            ],
        'stomatologicheskie-polikliniki' =>
            [
                'textType' => 'Стоматологическая поликлиника',
                'rubric' => '10157263',
            ],
        'bolnitsy-ginekologicheskie' =>
            [
                'textType' => 'Больница',
                'rubric' => '5',
            ],
        'bolnitsy-gorodskie' =>
            [
                'textType' => 'Больница',
                'rubric' => '5',
            ],
        'bolnitsy-infektsionnye' =>
            [
                'textType' => 'Больница',
                'rubric' => '5',
            ],
        'bolnitsy-narkologicheskie' =>
            [
                'textType' => 'Больница',
                'rubric' => '5',
            ],
        'bolnitsy-oblastnye' =>
            [
                'textType' => 'Больница',
                'rubric' => '5',
            ],
        'bolnitsy-onkologicheskie' =>
            [
                'textType' => 'Больница',
                'rubric' => '5',
            ],
        'bolnitsy-ortopedo-khirurgicheskie' =>
            [
                'textType' => 'Больница',
                'rubric' => '5',
            ],
        'bolnitsy-oftalmologicheskie' =>
            [
                'textType' => 'Больница',
                'rubric' => '5',
            ],
        'bolnitsy-psikhiatricheskie' =>
            [
                'textType' => 'Больница',
                'rubric' => '5',
            ],
        'bolnitsy-psikhonevrologicheskie' =>
            [
                'textType' => 'Больница',
                'rubric' => '5',
            ],
        'bolnitsy-tuberkuleznye' =>
            [
                'textType' => 'Больница',
                'rubric' => '5',
            ],
        'bolnitsy-urologicheskie' =>
            [
                'textType' => 'Больница',
                'rubric' => '5',
            ],
        'bolnitsy-fizioterapevticheskie' =>
            [
                'textType' => 'Больница',
                'rubric' => '5',
            ],
        'zhenskie-konsultatsii' =>
            [
                'textType' => 'Женская консультация',
                'rubric' => '30',
            ],
        'rodilnye-doma' =>
            [
                'textType' => 'Родильный дом',
                'rubric' => '31',
            ],
        'dispansery' =>
            [
                'textType' => 'Диспансер',
                'rubric' => '7',
            ],
        'dispansery-vrachebno-fizkulturnye' =>
            [
                'textType' => 'Диспансер',
                'rubric' => '7',
            ],
        'dispansery-kardiologicheskie' =>
            [
                'textType' => 'Диспансер',
                'rubric' => '7',
            ],
        'dispansery-kozhno-venerologicheskie' =>
            [
                'textType' => 'Диспансер',
                'rubric' => '7',
            ],
        'dispansery-mammologicheskie' =>
            [
                'textType' => 'Диспансер',
                'rubric' => '7',
            ],
        'dispansery-narkologicheskie' =>
            [
                'textType' => 'Диспансер',
                'rubric' => '7',
            ],
        'dispansery-onkologicheskie' =>
            [
                'textType' => 'Диспансер',
                'rubric' => '7',
            ],
        'dispansery-protivotuberkuleznye' =>
            [
                'textType' => 'Диспансер',
                'rubric' => '7',
            ],
        'dispansery-psikhonevrologicheskie' =>
            [
                'textType' => 'Диспансер',
                'rubric' => '7',
            ],
        'dispansery-endokrinologicheskie' =>
            [
                'textType' => 'Диспансер',
                'rubric' => '7',
            ],
        'gospitali' =>
            [
                'textType' => 'Госпиталь',
                'rubric' => '10158623',
            ],

        'detskie-polikliniki' =>
            [
                'textType' => 'Детская поликлиника',
                'rubric' => '4',
            ],
        'bolnitsy-detskie' =>
            [
                'textType' => 'Детская больница',
                'rubric' => '6',
            ],
        'apteki' =>
            [
                'textType' => 'Аптека',
                'rubric' => '10158373',
            ],
    ];
    protected $urls = [
        'fitnes-kluby/list?filter=14568' =>
            [
                'textType' => 'Тренажерный зал',
                'rubric' => '10328773',
                'filter' => [
                    '0' => [
                        'id' => '5223',
                        'value' => 'тренажерные залы',
                    ],
                ],
            ],
        'fitnes-kluby' =>
            [
                'textType' => 'Фитнес-клуб',
                'rubric' => '10328763',
                'filter' => [
                    '0' => [
                        'id' => '5223',
                        'value' => 'фитнес-клубы',
                    ],
                ],
            ],
        'shkoly-tantsev' =>
            [
                'textType' => 'Школа танцев',
                'rubric' => '10328783',
                'filter' => [
                    '0' => [
                        'id' => '5223',
                        'value' => 'школы танцев',
                    ],
                ],
            ],
        'yoga' =>
            [
                'textType' => 'Йога',
                'rubric' => '10328793',
                'filter' => [
                    '0' => [
                        'id' => '5223',
                        'value' => 'йога',
                    ],
                ],
            ],
        'basseyny' =>
            [
                'textType' => 'Бассейн',
                'rubric' => '10328803',
                'filter' => [
                    '0' => [
                        'id' => '5223',
                        'value' => 'бассейны',
                    ],
                ],
            ],
        'nochnye-kluby' =>
            [
                'textType' => 'Ночной клуб',
                'rubric' => '10328813',
                'filter' => [
                    '0' => [
                        'id' => '5243',
                        'value' => 'ночные клубы',
                    ],
                ],
            ],
        'bouling-kluby' =>
            [
                'textType' => 'Боулинг',
                'rubric' => '10328823',
                'filter' => [
                    '0' => [
                        'id' => '5243',
                        'value' => 'боулинг',
                    ],
                ],
            ],
        'sauny' =>
            [
                'textType' => 'Сауна',
                'rubric' => '10328843',
                'filter' => [
                    '0' => [
                        'id' => '5243',
                        'value' => 'сауны, бани',
                    ],
                ],
            ],
        'bani' =>
            [
                'textType' => 'Баня',
                'rubric' => '10328843',
                'filter' => [
                    '0' => [
                        'id' => '5243',
                        'value' => 'сауны, бани',
                    ],
                ],
            ],
        'razvlekatelnye-tsentry' =>
            [
                'textType' => 'Детский развлекательный центр',
                'rubric' => '10328853',
                'filter' => [
                    '0' => [
                        'id' => '5243',
                        'value' => 'детские развлекательные центры',
                    ],
                ],
            ],
        'peyntbol' =>
            [
                'textType' => 'Пейнтбол',
                'rubric' => '10328873',
                'filter' => [
                    '0' => [
                        'id' => '5243',
                        'value' => 'пейнтбол',
                    ],
                ],
            ],
        'straykbol' =>
            [
                'textType' => 'Страйкбол',
                'rubric' => '10328883',
                'filter' => [
                    '0' => [
                        'id' => '5243',
                        'value' => 'страйкбол',
                    ],
                ],
            ],
        'lazertag' =>
            [
                'textType' => 'Лазертаг',
                'rubric' => '10328893',
                'filter' => [
                    '0' => [
                        'id' => '5243',
                        'value' => 'лазертаг',
                    ],
                ],
            ],
        'salony-krasoty' =>
            [
                'textType' => 'Салон красоты',
                'rubric' => '10328643',
                'filter' => ['0' => [
                    'id' => '4983',
                    'value' => 'салоны красоты',
                ],

                ],],
        'parikmakherskie' =>
            [
                'textType' => 'Парикмахерская',
                'rubric' => '10328733',
                'filter' => [
                    '0' => [
                        'id' => '4983',
                        'value' => 'парикмахерские',
                    ],
                ],
            ],
        'nogtevye-studii' =>
            [
                'textType' => 'Ногтевая студия',
                'rubric' => '10328713',
                'filter' => '',
            ],
        'spa-salony' =>
            [
                'textType' => 'SPA-салон',
                'rubric' => '10328653',
                'filter' => [
                    '0' => [
                        'id' => '4983',
                        'value' => 'SPA-салоны',
                    ],
                ],
            ],
        'massazhnye-salony' =>
            [
                'textType' => 'Массажный салон',
                'rubric' => '10328703',
                'filter' => ''],
        'tatu-salony' =>
            [
                'textType' => 'Тату-салон',
                'rubric' => '10328693',
                'filter' => [
                    '0' => [
                        'id' => '4983',
                        'value' => 'салоны татуировки и пирсинга',
                    ],
                ],
            ],
        'kosmetologiya-kosmeticheskie-kabinety' =>
            [
                'textType' => 'Косметология',
                'rubric' => '10328663',
                'filter' => '',
            ],
        'vizazhisty-stilisty' =>
            [
                'textType' => 'Стилист-визажист',
                'rubric' => '10328743',
                'filter' => ''],
        'salony-krasoty?filter=3421,3538,3539,3540' =>
            [
                'textType' => 'Brow-бар',
                'rubric' => '10328683',
                'filter' => ['0' => [
                    'id' => '4983',
                    'value' => 'brow-бары',
                ],
                ],
            ],
        'kosmetologiya-kosmeticheskie-kabinety?filter=3003,3060,3061,3062,3063,3064,3065' =>
            [
                'textType' => 'Эпиляция',
                'rubric' => '10328673',
                'filter' => '',
            ],
    ];
    protected $filterArray = [];
    protected $export = true;

    protected $cities = [];

    protected $fp;

    protected $city = [
        'http://novosibirsk.spravkus.com/' => '4549',
        'http://eburg.spravkus.com/' => '5106',
        'http://nnovgorod.spravkus.com/' => '3612',
        'http://kazan.spravkus.com/' => '5269',
        'http://chelyabinsk.spravkus.com/' => '5539',
        'http://omsk.spravkus.com/' => '4580',
        'http://samara.spravkus.com/' => '4917',
        'http://rostov-nd.spravkus.com/' => '4848',
        'http://ufa.spravkus.com/' => '3345',
        'http://krasnoyarsk.spravkus.com/' => '4149',
        'http://astana.spravkus.com/' => '2133',
        'http://alma-ata.spravkus.com/' => '1913',
        'http://anapa.spravkus.com/' => '4055',
        'http://gelendzhik.spravkus.com/' => '4065',
        'http://sochi.spravkus.com/' => '4094',
        'http://volgograd.spravkus.com/' => '3472',
        'http://perm.spravkus.com/' => '4720',
        'http://voronezh.spravkus.com/' => '3438',

    ];

    protected $shimkent = [
        'http://spravkus.com/kz14/shymkent/' => '278027',
    ];
    protected $chel = [
        'http://chelyabinsk.spravkus.com/' => '5539',
    ];

    protected $food = [
        'restorany' =>
            [
                'textType' => 'Ресторан',
                'rubric' => '10328523',
                'filter' => ['0' => [
                    'id' => '4953',
                    'value' => 'рестораны',
                ],

                ],],
        'kafe' =>
            [
                'textType' => 'Кафе',
                'rubric' => '10328533',
                'filter' => ['0' => [
                    'id' => '4953',
                    'value' => 'кафе',
                ],
                ],],
        'pitstserii' =>
            [
                'textType' => 'Пиццерия',
                'rubric' => '10328553',
                'filter' => ['0' => [
                    'id' => '4953',
                    'value' => 'пиццерии',
                ],],],
        'bystroe-pitanie' =>
            [
                'textType' => 'Ресторан быстрого питания',
                'rubric' => '10328563',
                'filter' => ['0' => [
                    'id' => '4953',
                    'value' => 'рестораны быстрого питания',
                ],],],
        'bary-paby' =>
            [
                'textType' => 'Бар, паб',
                'rubric' => '10328573',
                'filter' => ['0' => [
                    'id' => '4953',
                    'value' => 'бары, пабы',
                ],],],
        'kofeyni' =>
            [
                'textType' => 'Кофейня, кондитерская',
                'rubric' => '10328583',
                'filter' => ['0' => [
                    'id' => '4953',
                    'value' => 'кофейни, кондитерские',
                ],],],
        'dostavka-edy-i-obedov' =>
            [
                'textType' => 'Доставка еды',
                'rubric' => '10328593',
                'filter' => ['0' => [
                    'id' => '4953',
                    'value' => 'службы доставки еды',
                ],],],
        'keytering' =>
            [
                'textType' => 'Кейтеринг',
                'rubric' => '10328603',
                'filter' => ['0' => [
                    'id' => '4953',
                    'value' => 'кейтеринг',
                ],],],
        'kalyan-bary' =>
            [
                'textType' => 'Кальянная',
                'rubric' => '10328613',
                'filter' => ['0' => [
                    'id' => '4953',
                    'value' => 'кальянные',
                ],],],
        'antikafe' =>
            [
                'textType' => 'Антикафе',
                'rubric' => '10329693',
                'filter' => ['0' => [
                    'id' => '4953',
                    'value' => 'антикафе',
                ],
                    '1' => [
                        'id' => '5243',
                        'value' => 'антикафе',
                    ],
                ],
            ],
    ];


    protected $rubricsArray = [];

    protected function actionParse($offset = null, $limit = null)
    {
//        $this->urls = $this->food;
        $this->urls = $this->urls103;
        $this->city = $this->chel;
//        $this->city = $this->shimkent;
        $criteria = new CDbCriteria();
        $criteria->select = [
            'title', 'id',
        ];
        $this->filterArray = CatalogFilter::model()->findAll($criteria);

        $criteria = new CDbCriteria();
        $criteria->select = [
            'title', 'id',
        ];
        $this->rubricsArray = Rubric::model()->findAll($criteria);

        $this->startParse();
    }

//    protected $apiHost = 'https://platform-api.103.рф';
//
////    protected $apiHost = 'https://platform-api.relax.ru';
//
////    protected $apiHost = 'http://platform-api.s-alekseyuk.ru'; //когда всё гуд поменять

    protected function findName($id, $array)
    {
        foreach ($array as $object)
        {
            if ($object->id == $id)
            {
                return $object->title;
            }
        }

        return null;
    }

    protected function setFiltersAndRubrics103($value)
    {
        $this->setKitFilter($value, 'подразделения:', [
            'акушерство' => [
                'rubrics' => [
                    ['id' => '32'],
                ],
                'filters' => [
                    ['id' => '458', 'value' => 'Гинеколог'],
                ],
            ],
            'аллергология' => [
                'rubrics' => [
                    ['id' => '33'],
                ],
                'filters' => [
                    ['id' => '442', 'value' => 'Аллерголог'],
                ],
            ],
            'ангиология' => [
                'filters' => [
                    ['id' => '343', 'value' => 'Ангиолог'],
                ],
            ],
            'андрология' => [
                'rubrics' => [
                    ['id' => '10158303'],
                ],
                'filters' => [
                    ['id' => '343', 'value' => 'Андролог'],
                ],
            ],
            'артрология и ревматология' => [
                'rubrics' => [
                    ['id' => '88'],
                ],
                'filters' => [
                    ['id' => '343', 'value' => 'Ревматолог'],
                ],
            ],
            'бальнеолечение' => [
                'filters' => [
                    ['id' => '538', 'value' => 'ванны минеральные'],
                ],
            ],
            'вакцинация' => [
                'rubrics' => [
                    ['id' => '34'],
                ],
            ],
            'венерология' => [
                'rubrics' => [
                    ['id' => '10158313'],
                ],
                'filters' => [
                    ['id' => '343', 'value' => 'Венеролог'],
                ],
            ],
            'вертебрология' => [
                'rubrics' => [
                    ['id' => '10158593'],
                ],
                'filters' => [
                    ['id' => '343', 'value' => 'Вертебролог'],
                ],
            ],
            'вирусология' => [
                'filters' => [
                    ['id' => '343', 'value' => 'Инфекционист'],
                ],
            ],
            'гастроэнтерология' => [
                'rubrics' => [
                    ['id' => '35'],
                ],
                'filters' => [
                    ['id' => '343', 'value' => 'Гастроэнтеролог'],
                ],
            ],
            'гематология' => [
                'rubrics' => [
                    ['id' => '10158323'],
                ],
                'filters' => [
                    ['id' => '343', 'value' => 'Гематолог'],
                ],
            ],
            'генетика' => [
                'rubrics' => [
                    ['id' => '10158333'],
                ],
                'filters' => [
                    ['id' => '343', 'value' => 'Генетик'],
                ],
            ],
            'геронтология' => [
                'filters' => [
                    ['id' => '343', 'value' => 'Геронтолог'],
                ],
            ],
            'гинекология' => [
                'rubrics' => [
                    ['id' => '32'],
                ],
                'filters' => [
                    ['id' => '343', 'value' => 'Гинеколог'],
                ],
            ],
            'гирудология' => [
                'rubrics' => [
                    ['id' => '110'],
                ],

                'filters' => [
                    ['id' => '343', 'value' => 'Гирудотерапевт'],
                ],
            ],
            'гомеопатия' => [
                'rubrics' => [
                    ['id' => '113'],
                ],
                'filters' => [
                    ['id' => '343', 'value' => 'Гомеопат'],
                ],
            ],
            'дерматология' => [
                'rubrics' => [
                    ['id' => '37'],
                ],
                'filters' => [
                    ['id' => '343', 'value' => 'Дерматолог'],
                ],
            ],
            'дефектология и сурдология' => [
                'filters' => [
                    ['id' => '343', 'value' => 'Сурдолог'],
                ],
            ],
            'диетология' => [
                'rubrics' => [
                    ['id' => '38'],
                ],
                'filters' => [
                    ['id' => '343', 'value' => 'Диетолог'],
                ],
            ],
            'иммунология' => [
                'rubrics' => [
                    ['id' => '91'],
                ],
                'filters' => [
                    ['id' => '343', 'value' => 'Иммунолог'],
                ],
            ],
            'инфекционисты' => [
                'filters' => [
                    ['id' => '343', 'value' => 'Инфекционист'],
                ],
            ],
            'кардиология' => [
                'rubrics' => [
                    ['id' => '39'],
                ],
                'filters' => [
                    ['id' => '343', 'value' => 'Кардиолог'],
                ],
            ],
            'кардиохирургия' => [
                'rubrics' => [
                    ['id' => '39'],
                ],
                'filters' => [
                    ['id' => '444', 'value' => 'кардиохирургия'],
                ],
            ],
            'колопроктология' => [
                'rubrics' => [
                    ['id' => '50'],
                ],
                'filters' => [
                    ['id' => '343', 'value' => 'Проктолог'],
                ],
            ],
            'косметология' => [
                'rubrics' => [
                    ['id' => '67'],
                ],
                'filters' => [
                    ['id' => '343', 'value' => 'Косметолог'],
                ],
            ],
            'логопедия' => [
                'rubrics' => [
                    ['id' => '10158393'],
                ],
                'filters' => [
                    ['id' => '343', 'value' => 'Логопед'],
                ],
            ],
            'физиотерапия и ЛФК' => [
                'rubrics' => [
                    ['id' => '94'],
                ],
                'filters' => [
                    ['id' => '343', 'value' => 'Физиотерапевт'],
                ],
            ],
            'мануальная терапия' => [
                'rubrics' => [
                    ['id' => '109'],
                ],
                'filters' => [
                    ['id' => '343', 'value' => 'Мануальный терапевт'],
                ],
            ],
            'маммология' => [
                'rubrics' => [
                    ['id' => '112'],
                ],
                'filters' => [
                    ['id' => '343', 'value' => 'Маммолог'],
                ],
            ],
            'наркология' => [
                'rubrics' => [
                    ['id' => '10158423'],
                ],
                'filters' => [
                    ['id' => '343', 'value' => 'Нарколог'],
                ],
            ],
            'неврология' => [
                'rubrics' => [
                    ['id' => '43'],
                ],

                'filters' => [
                    ['id' => '343', 'value' => 'Невролог'],
                ],

            ],
            'нейрохирургия' => [
                'rubrics' => [
                    ['id' => '96'],
                ],
                'filters' => [
                    ['id' => '448', 'value' => 'нейрохирургия'],
                ],
            ],
            'неонатология' => [
                'rubrics' => [
                    ['id' => '32'],
                ],
                'filters' => [
                    ['id' => '343', 'value' => 'Неонатолог'],
                ],
            ],
            'нефрология' => [
                'filters' => [
                    ['id' => '343', 'value' => 'Нефролог'],
                ],

            ],
            'онкология' => [
                'filters' => [
                    ['id' => '343', 'value' => 'Онколог'],
                ],
                'rubrics' => [
                    ['id' => '44'],
                ],
            ],
            'ортопедия' => [
                'rubrics' => [
                    ['id' => '103'],
                ],
                'filters' => [
                    ['id' => '343', 'value' => 'Ортопед'],
                ],
            ],
            'оториноларингология' => [

                'rubrics' => [
                    ['id' => '46'],
                ],
                'filters' => [
                    ['id' => '343', 'value' => 'Оториноларинголог (ЛОР)'],
                ],
            ],
            'офтальмология' => [
                'rubrics' => [
                    ['id' => '47'],
                ],
                'filters' => [
                    ['id' => '343', 'value' => 'Офтальмолог'],
                ],
            ],
            'педиатрия' => [
                'rubrics' => [
                    ['id' => '48'],
                ],
                'filters' => [
                    ['id' => '343', 'value' => 'Педиатр'],
                ],
            ],
            'пластическая хирургия' => [
                'rubrics' => [
                    ['id' => '49'],
                ],
                'filters' => [
                    ['id' => '343', 'value' => 'Пластический хирург'],
                ],
            ],
            'психиатрия' => [
                'rubrics' => [
                    ['id' => '51'],
                ],
                'filters' => [
                    ['id' => '343', 'value' => 'Психиатр'],
                ],
            ],
            'психология и психотерапия' => [
                'rubrics' => [
                    ['id' => '10156310'],
                ],
                'filters' => [
                    ['id' => '343', 'value' => 'Психолог'],
                ],
            ],
            'пульмонология' => [

                'rubrics' => [
                    ['id' => '92'],
                ],
                'filters' => [
                    ['id' => '343', 'value' => 'Пульмонолог'],
                ],
            ],
            'сексология' => [
                'rubrics' => [
                    ['id' => '108'],
                ],
                'filters' => [
                    ['id' => '343', 'value' => 'Сексолог, сексопатолог'],
                ],
            ],
            'сомнология' => [
                'rubrics' => [
                    ['id' => '10158443'],
                ],
                'filters' => [
                    ['id' => '343', 'value' => 'Сомнолог'],
                ],
            ],
            'стоматология' => [
                'rubrics' => [
                    ['id' => '1'],
                ],
                'filters' => [
                    ['id' => '343', 'value' => 'Стоматолог'],
                ],
            ],
            'терапевтические услуги' => [
                'rubrics' => [
                    ['id' => '52'],
                ],
                'filters' => [
                    ['id' => '343', 'value' => 'Терапевт'],
                ],
            ],
            'травматология' => [
                'rubrics' => [
                    ['id' => '103'],
                ],
                'filters' => [
                    ['id' => '343', 'value' => 'Травматолог'],
                ],
            ],
            'трансфузиология' => [
                'rubrics' => [
                    ['id' => '10157693'],
                ],
            ],
            'трихология' => [
                'rubrics' => [
                    ['id' => '10158463'],
                ],
                'filters' => [
                    ['id' => '343', 'value' => 'Трихолог'],
                ],
            ],
            'урология' => [
                'rubrics' => [
                    ['id' => '54'],
                ],
                'filters' => [
                    ['id' => '343', 'value' => 'Уролог'],
                ],
            ],
            'флебология' => [
                'rubrics' => [
                    ['id' => '111'],
                ],
                'filters' => [
                    ['id' => '343', 'value' => 'Флеболог'],
                ],
            ],
            'хирургия' => [
                'rubrics' => [
                    ['id' => '96'],
                ],
                'filters' => [
                    ['id' => '343', 'value' => 'Хирург'],
                ],
            ],
            'эко' => [
                'rubrics' => [
                    ['id' => '114'],
                ],
                'filters' => [
                    ['id' => '343', 'value' => 'Репродуктолог, врач ЭКО'],
                ],
            ],
            'эндокринология' => [
                'rubrics' => [
                    ['id' => '55'],
                ],
                'filters' => [
                    ['id' => '343', 'value' => 'Эндокринолог'],
                ],
            ],
        ]);
        $this->setKitFilter($value, 'врачи-специалисты:', [
            'акушер-гинеколог' => [
                'filters' => [
                    ['id' => '343', 'value' => 'Гинеколог'],
                ],
            ],
            'аллерголог' => [
                'filters' => [
                    ['id' => '343', 'value' => 'Аллерголог'],
                ],
            ],
            'андролог' => [
                'filters' => [
                    ['id' => '343', 'value' => 'Андролог'],
                ],
            ],
            'анестезиолог-реаниматолог' => [
                'filters' => [
                    ['id' => '343', 'value' => 'Анестезиолог-реаниматолог'],
                ],
            ],
            'гастроэнтеролог' => [
                'filters' => [
                    ['id' => '343', 'value' => 'Гастроэнтеролог'],
                ],
            ],
            'гематолог' => [
                'filters' => [
                    ['id' => '343', 'value' => 'Гематолог'],
                ],
            ],
            'геронтолог' => [
                'filters' => [
                    ['id' => '343', 'value' => 'Геронтолог'],
                ],
            ],
            'гомеопат' => [
                'filters' => [
                    ['id' => '343', 'value' => 'Гомеопат'],
                ],
            ],
            'дерматолог' => [
                'filters' => [
                    ['id' => '343', 'value' => 'Дерматолог'],
                ],
            ],
            'диетолог' => [
                'filters' => [
                    ['id' => '343', 'value' => 'Диетолог'],
                ],
            ],
            'иммунолог' => [
                'filters' => [
                    ['id' => '343', 'value' => 'Иммунолог'],
                ],
            ],
            'инфекционист' => [
                'filters' => [
                    ['id' => '343', 'value' => 'Инфекционист'],
                ],
            ],
            'кардиолог' => [
                'filters' => [
                    ['id' => '343', 'value' => 'Кардиолог'],
                ],
            ],
            'косметолог' => [
                'filters' => [
                    ['id' => '343', 'value' => 'Косметолог'],
                ],
            ],
            'маммолог' => [
                'filters' => [
                    ['id' => '343', 'value' => 'Маммолог'],
                ],
            ],
            'мануальный терапевт' => [
                'filters' => [
                    ['id' => '343', 'value' => 'Мануальный терапевт'],
                ],
            ],
            'нарколог' => [
                'filters' => [
                    ['id' => '343', 'value' => 'Нарколог'],
                ],
            ],
            'невролог' => [
                'filters' => [
                    ['id' => '343', 'value' => 'Невролог'],
                ],
            ],
            'нейрохирург' => [
                'filters' => [
                    ['id' => '343', 'value' => 'Нейрохирург'],
                ],
            ],
            'нефролог' => [
                'filters' => [
                    ['id' => '343', 'value' => 'Нефролог'],
                ],
            ],
            'онколог' => [
                'filters' => [
                    ['id' => '343', 'value' => 'Онколог'],
                ],
            ],
            'ортопед' => [
                'filters' => [
                    ['id' => '343', 'value' => 'Ортопед'],
                ],
            ],
            'оториноларинголог' => [
                'filters' => [
                    ['id' => '343', 'value' => 'Оториноларинголог (ЛОР)'],
                ],
            ],
            'офтальмолог' => [
                'filters' => [
                    ['id' => '343', 'value' => 'Офтальмолог'],
                ],
            ],
            'педиатр' => [
                'filters' => [
                    ['id' => '343', 'value' => 'Педиатр'],
                ],
            ],
            'пластический хирург' => [
                'filters' => [
                    ['id' => '343', 'value' => 'Пластический хирург'],
                ],
            ],
            'проктолог' => [
                'filters' => [
                    ['id' => '343', 'value' => 'Проктолог'],
                ],
            ],
            'психолог' => [
                'filters' => [
                    ['id' => '343', 'value' => 'Психолог'],
                ],
            ],
            'психиатр' => [
                'filters' => [
                    ['id' => '343', 'value' => 'Психиатр'],
                ],
            ],
            'психотерапевт' => [
                'filters' => [
                    ['id' => '343', 'value' => 'Психотерапевт'],
                ],
            ],
            'пульмонолог' => [
                'filters' => [
                    ['id' => '343', 'value' => 'Пульмонолог'],
                ],
            ],
            'радиолог' => [
                'filters' => [
                    ['id' => '343', 'value' => ''],
                ],
            ],
            'ревматолог' => [
                'filters' => [
                    ['id' => '343', 'value' => 'Ревматолог'],
                ],
            ],
            'сексопатолог' => [
                'filters' => [
                    ['id' => '343', 'value' => 'Сексолог, сексопатолог'],
                ],
            ],
            'стоматолог' => [
                'filters' => [
                    ['id' => '343', 'value' => 'Стоматолог'],
                ],
            ],
            'терапевт' => [
                'filters' => [
                    ['id' => '343', 'value' => 'Терапевт'],
                ],
            ],
            'токсиколог' => [
                'rubrics' => [
                    ['id' => '10158423'],
                ],
            ],
            'травматолог' => [
                'filters' => [
                    ['id' => '343', 'value' => 'Травматолог'],
                ],
            ],
            'уролог' => [
                'filters' => [
                    ['id' => '343', 'value' => 'Уролог'],
                ],
            ],
            'хирург' => [
                'filters' => [
                    ['id' => '343', 'value' => 'Хирург'],
                ],
            ],
            'эндокринолог' => [
                'filters' => [
                    ['id' => '343', 'value' => 'Эндокринолог'],
                ],
            ],
        ]);
        $this->setKitFilter($value, 'методы диагностики:', [
            '3d-узи' => [
                'rubrics' => [
                    ['id' => '53'],
                ],
                'filters' => [
                    ['id' => '414', 'value' => 'плода при беременности'],
                ],
            ],
            'электрокардиография' => [
                'filters' => [
                    ['id' => '1563', 'value' => 'кардиограмма (ЭКГ)'],
                    ['id' => '517', 'value' => 'Кардиограмма (ЭКГ)'],
                ],
            ],
            'эндоскопия' => [
                'rubrics' => [
                    ['id' => '101'],
                ],
            ],
            'ангиография' => [
                'rubrics' => [
                    ['id' => '97'],
                ],
                'filters' => [
                    ['id' => '522', 'value' => 'ангиография'],
                ],
            ],
            'биопсия' => [
                'filters' => [
                    ['id' => '411', 'value' => 'гистология'],
                ],
            ],
            'маммография' => [
                'rubrics' => [
                    ['id' => '112'],
                ],
            ],
            'компьютерная томография' => [
                'rubrics' => [
                    ['id' => '10158253'],
                ],
                'filters' => [
                    ['id' => '517', 'value' => 'Компьютерная томография (КТ)'],
                ],
            ],
            'суточное мониторирование артериального давления' => [
                'rubrics' => [
                    ['id' => '10158283'],
                ],
                'filters' => [
                    ['id' => '1563', 'value' => 'холтеровское мониторирование'],
                ],
            ],
        ]);
        $this->setFilter('517', $value, 'лаборатория анализов', 'Лабораторная диагностика (анализы)');
        $this->setFilter('', $value, 'стационар', 'да', false, 105);
        $this->setKitFilter($value, 'исследования и анализы:', [
            'крови' => [
                'filters' => [
                    ['id' => '411', 'value' => 'анализы крови'],
                ],
            ],
            'мочи' => [
                'filters' => [
                    ['id' => '411', 'value' => 'анализы мочи'],
                ],
            ],
            'кала' => [
                'filters' => [
                    ['id' => '411', 'value' => 'анализы кала'],
                ],
            ],
            'УЗИ' => [

                'rubrics' => [
                    ['id' => '53'],
                ],
                'filters' => [
                    ['id' => '517', 'value' => 'Ультразвуковая диагностика (УЗИ)'],
                ],

            ],
            'МРТ' => [
                'rubrics' => [
                    ['id' => '10158263'],
                ],
                'filters' => [
                    ['id' => '517', 'value' => 'Магнитно-резонансная томография (МРТ)'],
                ],
            ],
            'рентген' => [
                'rubrics' => [
                    ['id' => '97'],
                ],
                'filters' => [
                    ['id' => '517', 'value' => 'Рентген'],
                ],

            ],
            'компьютерная томография' => [
                'rubrics' => [
                    ['id' => '10158253'],
                ],
                'filters' => [
                    ['id' => '517', 'value' => 'Компьютерная томография (КТ)'],
                ],
            ],
            'ЭКГ' => [

                'rubrics' => [
                    ['id' => '10158283'],
                ],
                'filters' => [
                    ['id' => '1563', 'value' => 'кардиограмма (ЭКГ)'],

                    ['id' => '517', 'value' => 'Кардиограмма (ЭКГ)'],
                ],

            ],
            'биопсия' => [
                'filters' => [
                    ['id' => '411', 'value' => 'гистология'],
                ],

            ],
            'колоноскопия' => [

                'rubrics' => [
                    ['id' => '50'],
                ],
                'filters' => [
                    ['id' => '521', 'value' => 'колоноскопия'],
                ],
            ],
            'ЭЭГ' => [

                'rubrics' => [
                    ['id' => '101'],
                ],
                'filters' => [
                    ['id' => '1563', 'value' => 'электроэнцефалография (ЭЭГ)'],
                ],

            ],
            'эндоскопия' => [
                'rubrics' => [
                    ['id' => '101'],
                ],
            ],
            'ректороманоскопия' => [
                'rubrics' => [
                    ['id' => '101'],
                ],
                'filters' => [
                    ['id' => '521', 'value' => 'ректороманоскопия'],
                ],
            ],
            'бронхоскопия' => [
                'rubrics' => [
                    ['id' => '101'],
                ],
                'filters' => [
                    ['id' => '521', 'value' => 'бронхоскопия'],
                ],
            ],
            'эзофагогастроскопия' => [
                'filters' => [
                    ['id' => '517', 'value' => 'ФГДС'],
                ],
            ],
        ]);
        $this->setKitFilter($value, 'вакцинация:', [
            'от бешенства' => [
                'filters' => [
                    ['id' => '347', 'value' => 'бешенство'],
                ],
            ],
            'АКДС и АДСМ' => [
                'filters' => [
                    ['id' => '347', 'value' => 'АКДС'],
                ],
            ],
            'ветряная оспа' => [
                'filters' => [
                    ['id' => '347', 'value' => 'ветряная оспа'],
                ],
            ],
            'вирус папилломы' => [
                'filters' => [
                    ['id' => '347', 'value' => 'папилломавирус (ВПЧ)'],
                ],
            ],
            'гепатит' => [
                'filters' => [
                    ['id' => '347', 'value' => 'гепатит А'],
                    ['id' => '347', 'value' => 'гепатит В'],
                ],
            ],
            'грипп' => [
                'filters' => [
                    ['id' => '347', 'value' => 'грипп'],
                ],
            ],
            'желтая лихорадка' => [
                'filters' => [
                    ['id' => '347', 'value' => 'желтая лихорадка'],
                ],
            ],
            'клещевой энцефалит' => [
                'filters' => [
                    ['id' => '347', 'value' => 'клещевой энцефалит'],
                ],
            ],
            'корь' => [
                'filters' => [
                    ['id' => '347', 'value' => 'корь, краснуха, паротит'],
                ],
            ],
            'краснуха' => [
                'filters' => [
                    ['id' => '347', 'value' => 'корь, краснуха, паротит'],
                ],
            ],
            'менингококковый менингит' => [
                'filters' => [
                    ['id' => '347', 'value' => 'менингококковая инфекция'],
                ],
            ],
            'полиомиелит' => [
                'filters' => [
                    ['id' => '347', 'value' => 'полиомиелит'],
                ],
            ],
            'столбняк' => [
                'filters' => [
                    ['id' => '347', 'value' => 'столбняк'],
                ],
            ],
            'туберкулёз' => [
                'filters' => [
                    ['id' => '347', 'value' => 'БЦЖ (туберкулез)'],
                ],
            ],
        ]);
        $this->setFilter('450', $value, 'выезд на дом', 'Вызов врача на дом');
        $this->setFilter('', $value, 'скорая помощь', 'да', false, 102);
        $this->setKitFilter($value, 'предоставляемые справки:', [

            'больничный лист' => [
                'filters' => [
                    ['id' => '1013', 'value' => 'больничный лист'],
                ],
            ],
            'водительская медкомиссия' => [
                'filters' => [
                    ['id' => '1013', 'value' => 'водительская медкомиссия'],
                ],
            ],
            'медицинская книжка' => [
                'filters' => [
                    ['id' => '1013', 'value' => 'санитарная книжка'],
                ],
            ],
            'медицинский осмотр' => [
                'filters' => [
                    ['id' => '1013', 'value' => 'профосмотры'],
                ],
            ],
            'обменная карта' => [
                'filters' => [
                    ['id' => '1013', 'value' => 'обменная карта'],
                ],
            ],
            'санаторно-курортная карта' => [
                'filters' => [
                    ['id' => '1013', 'value' => 'санаторно-курортная карта'],
                ],
            ],
            'справка в бассейн' => [
                'filters' => [
                    ['id' => '1013', 'value' => 'справка в бассейн'],
                ],
            ],
            'справка для поступления на работу, учебу' => [
                'filters' => [
                    ['id' => '1013', 'value' => 'медосмотры на допуск к работе'],
                ],
            ],
            '' => [
                'filters' => [
                    ['id' => '1013', 'value' => 'медсправки для ВУЗа'],
                ],
            ],
            'справка на оружие' => [
                'filters' => [
                    ['id' => '1013', 'value' => 'медосмотры на право ношения оружия'],
                ],
            ],
        ]);
        $this->setFilter('1363', $value, 'оплата картой', 'Есть', true, null, 'Нет');
        $this->setFilter('1373', $value, 'бесплатный wi-fi', 'Есть', true, null, 'Нет');
        $this->setFilter('1353', $value, 'гостевая парковка', 'Есть', true, null, 'Нет');
        $this->setKitFilter($value, 'ценовая категория:', [
            'все' => [
                'filters' => [
                    ['id' => '4963', 'value' => 'не переносим'],
                ],
            ],
            'economy' => [
                'filters' => [
                    ['id' => '4963', 'value' => 'эконом'],
                ],
            ],
            'premium' => [
                'filters' => [
                    ['id' => '4963', 'value' => 'премиум'],
                ],
            ],
            'standart' => [
                'filters' => [
                    ['id' => '4963', 'value' => 'стандарт'],
                ],
            ],
        ]);
        $this->setKitFilter($value, 'услуги:', [
            'терапия' => [
                'filters' => [
                    ['id' => '343', 'value' => 'Терапевт'],
                ],
            ],
            'протезирование' => [
                'rubrics' => [
                    ['id' => '15'],
                ],
            ],
            'хирургия' => [
                'filters' => [
                    ['id' => '343', 'value' => 'Хирург'],
                ],
            ],
            'рентгенография' => [
                'rubrics' => [
                    ['id' => '97'],
                ],
            ],
            'ортодонтия' => [
                'rubrics' => [
                    ['id' => '17'],
                ],
            ],
            'челюстно-лицевая хирургия' => [
                'filters' => [
                    ['id' => '448', 'value' => 'челюстно-лицевая хирургия'],
                ],
            ],
            'эстетическая стоматология' => [
                'rubrics' => [
                    ['id' => '10158493'],
                ],
                'filters' => [
                    ['id' => '343', 'value' => 'Стоматолог'],
                ],
            ],
        ]);
        $this->setFilter('538', $value, 'ароматерапия', 'ароматерапия');
        $this->setFilter('', $value, 'массаж', 'да', false, 10158383);
        $this->setFilter('', $value, 'массаж', 'да', false, 93);
        $this->setFilter('343', $value, 'гирудотерапия', 'Гирудотерапевт', false, 110);
        $this->setFilter('', $value, 'гирудотерапия', 'да', false, 10156316);
        $this->setFilter('343', $value, 'гирудотерапия', 'Гирудотерапевт');
        $this->setFilter('538', $value, 'гидроколонотерапия', 'гидроколонотерапия');
        $this->setFilter('470', $value, 'иглоукалывание', 'Рефлексотерапия (иглоукалывание)');
        $this->setKitFilter($value, 'диагностика:', [
            'полная' => [
                'filters' => [
                    ['id' => '427', 'value' => 'полное обследование'],
                ],
            ],
        ]);
        $this->setKitFilter($value, 'подбор очков:', [
            'простых' => [
                'filters' => [
                    ['id' => '427', 'value' => 'подбор очков'],
                ],
            ],
            'при астигматизме' => [
                'filters' => [
                    ['id' => '427', 'value' => 'подбор очков'],
                ],
            ],
        ]);
        $this->setFilter('427', 'аппланационная тонометрия', 'тонометрия');
        $this->setFilter('427', 'В-сканирование глазного яблока', 'УЗИ глаза');
        $this->setFilter('427', 'гониоскопия', 'офтальмоскопия');
        $this->setFilter('421', 'исправление', 'лечение косоглазия');
        $this->setFilter('407', 'лечение', 'лечение наркомании');
        $this->setFilter('407', 'работа с родными', 'консультирование родственников');
        $this->setFilter('407', 'ТЭС-терапия', 'ТЭС-терапия');
        $this->setFilter('407', 'плазмаферез', 'плазмаферез');
        $this->setFilter('517', 'лабораторная и инструментальная диагностика
', 'Лабораторная диагностика (анализы)');
        $this->setKitFilter($value, 'профиль:', [
            '' => [
                'filters' => [
                    ['id' => '', 'value' => ''],
                ],
            ],
            'гинекологическая' => [
                'filters' => [
                    ['id' => '343', 'value' => 'Гинеколог'],
                    ['id' => '456', 'value' => 'Гинекологическое'],
                ],
            ],
            'терапевтическая' => [
                'filters' => [
                    ['id' => '343', 'value' => 'Терапевт'],
                    ['id' => '456', 'value' => 'Терапевтическое'],
                ],
            ],
            'гематологическая' => [
                'filters' => [
                    ['id' => '343', 'value' => 'Гематолог'],
                    ['id' => '456', 'value' => 'Гематологическое'],
                ],
            ],
            'кардиологическая' => [
                'filters' => [
                    ['id' => '343', 'value' => 'Кардиолог'],
                    ['id' => '456', 'value' => 'Кардиологическое'],
                ],
            ],
            'нефрологическая' => [
                'filters' => [
                    ['id' => '343', 'value' => 'Нефролог'],
                ],
            ],
            'пульмонологическая' => [
                'filters' => [
                    ['id' => '343', 'value' => 'Пульмонолог'],
                    ['id' => '456', 'value' => 'Пульмонологическое'],
                ],
            ],
            'колопроктологическая' => [
                'filters' => [
                    ['id' => '343', 'value' => 'Проктолог'],
                    ['id' => '456', 'value' => 'Проктологическое'],
                ],
            ],
            'общая хирургия' => [
                'filters' => [
                    ['id' => '343', 'value' => 'Хирург'],
                ],
            ],
            'урологическая' => [
                'filters' => [
                    ['id' => '343', 'value' => 'Уролог'],
                    ['id' => '456', 'value' => 'Урологическое'],
                ],
            ],
            'нейрохирургическая' => [
                'rubrics' => [
                    ['id' => '96',],
                ],
                'filters' => [
                    ['id' => '448', 'value' => 'нейрохирургия'],
                ],
            ],
            'отоларингологическая' => [
                'filters' => [
                    ['id' => '343', 'value' => 'Оториноларинголог (ЛОР)'],
                    ['id' => '456', 'value' => 'Оториноларингологическое'],
                ],
            ],
            'травматологическая' => [
                'filters' => [
                    ['id' => '343', 'value' => 'Травматолог'],
                ],
            ],
            'ортопедическая' => [
                'filters' => [
                    ['id' => '343', 'value' => 'Ортопед'],
                    ['id' => '456', 'value' => 'Ортопедическое'],
                ],
            ],
            'инфекционная' => [
                'filters' => [
                    ['id' => '343', 'value' => 'Инфекционист'],
                ],
            ],
            'пластическая хирургия' => [
                'filters' => [
                    ['id' => '343', 'value' => 'Пластический хирург'],
                ],
            ],
            'психиатрическая' => [
                'filters' => [
                    ['id' => '343', 'value' => 'Психиатр'],
                ],
            ],
            'психоневрологическая' => [
                'filters' => [
                    ['id' => '343', 'value' => 'Психолог'],
                    ['id' => '456', 'value' => 'Неврологическое'],
                ],
            ],
            'онкологическая гастроэнтерологическая' => [
                'filters' => [
                    ['id' => '343', 'value' => 'Онколог'],
                    ['id' => '456', 'value' => 'Онкологическое'],
                    ['id' => '', 'value' => 'Гастроэнтерологическое'],
                ],
            ],
            'челюстно-лицевая хирургия' => [
                'filters' => [
                    ['id' => '448', 'value' => 'челюстно-лицевая хирургия'],
                ],
            ],
            'эндокринология и диабетология' => [
                'filters' => [
                    ['id' => '343', 'value' => 'Эндокринолог'],
                    ['id' => '456', 'value' => 'Эндокринологическое'],
                ],
            ],
            'общая эндокринология' => [
                'filters' => [
                    ['id' => '343', 'value' => 'Эндокринолог'],
                    ['id' => '456', 'value' => 'Эндокринологическое'],
                ],
            ],
            'иммунологическая' => [
                'filters' => [
                    ['id' => '343', 'value' => 'Иммунолог'],
                ],
            ],
            'офтальмологическая' => [
                'filters' => [
                    ['id' => '343', 'value' => 'Офтальмолог'],
                    ['id' => '456', 'value' => 'Офтальмологическое'],
                ],
            ],
        ]);
        $this->setKitFilter($value, 'отделения:', [
            'пластической хирургии' => [
                'filters' => [
                    ['id' => '343', 'value' => 'Пластический хирург'],
                ],
            ],
            'диагностики' => [
                'rubrics' => [
                    ['id' => '10158283',],
                ],
            ],
            'медицинской генетики' => [
                'filters' => [
                    ['id' => '343', 'value' => 'Генетик'],
                ],
            ],
            'гастроэнтерологии' => [
                'filters' => [
                    ['id' => '343', 'value' => 'Гастроэнтеролог'],
                    ['id' => '456', 'value' => 'Гастроэнтерологическое'],
                ],
            ],
            'колопроктологии' => [
                'filters' => [
                    ['id' => '343', 'value' => 'Проктолог'],
                    ['id' => '456', 'value' => 'Проктологическое'],
                ],
            ],
            'лечебной физкультуры и реабилитации' => [
                'filters' => [
                    ['id' => '', 'value' => 'Физиотерапевт'],
                ],
            ],
            'неврологии' => [
                'filters' => [
                    ['id' => '343', 'value' => 'Невролог'],
                    ['id' => '456', 'value' => 'Неврологическое'],
                ],
            ],
            'офтальмологии' => [
                'filters' => [
                    ['id' => '343', 'value' => 'Офтальмолог'],
                    ['id' => '456', 'value' => 'Офтальмологическое'],
                ],
            ],
            'педиатрии' => [
                'filters' => [
                    ['id' => '343', 'value' => 'Педиатр'],
                    ['id' => '456', 'value' => 'Педиатрическое'],
                ],
            ],
            'пульмонологии' => [
                'filters' => [
                    ['id' => '343', 'value' => 'Пульмонолог'],
                    ['id' => '456', 'value' => 'Пульмонологическое'],
                ],
            ],
            'травматологии и ортопедии' => [
                'filters' => [
                    ['id' => '343', 'value' => 'Ортопед'],
                    ['id' => '', 'value' => 'Травматолог'],
                ],
            ],
            'урологии' => [
                'filters' => [
                    ['id' => '343', 'value' => 'Уролог'],
                    ['id' => '456', 'value' => 'Урологическое'],
                ],
            ],
            'физиотерапии' => [
                'filters' => [
                    ['id' => '343', 'value' => 'Физиотерапевт'],
                ],
            ],
            'хирургии' => [
                'filters' => [
                    ['id' => '', 'value' => 'Хирург'],
                ],
            ],
            'психиатрии' => [
                'filters' => [
                    ['id' => '343', 'value' => 'Психиатр'],
                ],
            ],
            'эндокринологии' => [
                'filters' => [
                    ['id' => '343', 'value' => 'Эндокринолог'],
                    ['id' => '456', 'value' => 'Эндокринологическое'],
                ],
            ],
            'онкологии' => [
                'filters' => [
                    ['id' => '343', 'value' => 'Онколог'],
                    ['id' => '456', 'value' => 'Онкологическое'],
                ],
            ],
            'гинекологии' => [
                'filters' => [
                    ['id' => '343', 'value' => 'Гинеколог'],
                    ['id' => '456', 'value' => 'Гинекологическое'],
                ],
            ],
            'нефрологии' => [
                'filters' => [
                    ['id' => '343', 'value' => 'Нефролог'],
                ],
            ],
            'кардиологии' => [
                'filters' => [
                    ['id' => '343', 'value' => 'Кардиолог'],
                    ['id' => '456', 'value' => 'Кардиологическое'],
                ],
            ],
            'гематологии' => [
                'filters' => [
                    ['id' => '343', 'value' => 'Гематолог'],
                    ['id' => '456', 'value' => 'Гематологическое'],
                ],
            ],
            'наркологическое' => [
                'filters' => [
                    ['id' => '343', 'value' => 'Нарколог'],
                    ['id' => '456', 'value' => 'Наркологическое'],
                ],
            ],
        ]);
        $this->setFilter('456', $value, 'родильное отделение', 'Родильное');
        $this->setFilter('', $value, 'пункт сдачи донорской крови', 'да', false, 10157693);
        $this->setFilter('', $value, 'стационар дневного пребывания', 'да', false, 105);
        $this->setFilter('517', $value, 'УЗИ', 'Ультразвуковая диагностика (УЗИ)', false, 53);
        $this->setFilter('517', $value, 'лабораторные исследования', 'Лабораторная диагностика (анализы)');
        $this->setKitFilter($value, 'консультация врачей:', [
            'терапевт' => [
                'filters' => [
                    ['id' => '343', 'value' => 'Терапевт'],
                ],
            ],
            'окулист' => [
                'filters' => [
                    ['id' => '343', 'value' => 'Офтальмолог'],
                ],
            ],
            'отоларинголог' => [
                'filters' => [
                    ['id' => '343', 'value' => 'Оториноларинголог (ЛОР)'],
                ],
            ],
            'стоматолог' => [
                'filters' => [
                    ['id' => '343', 'value' => 'Стоматолог'],
                ],
            ],
        ]);
        $this->setKitFilter($value, 'ведение:', [
            'беременных женщин' => [
                'filters' => [
                    ['id' => '430', 'value' => 'ведение беременности'],
                ],
            ],
        ]);
        $this->setFilter('430', $value, 'детский гинеколог', 'детская гинекология');
        $this->setFilter('1683', $value, 'доставка лекарств', 'Есть', true, null, 'Нет');
        $this->setFilter('1693', $value, 'изготовление лекарств', 'Есть', true, null, 'Нет');
        $this->setFilter('1703', $value, 'гомеопатическая', 'Есть', true, null, 'Нет');


    }

    protected function setFiltersAndRubrics($value)
    {
        $this->setFiltersAndRubrics103($value);
//        $this->setFiltersAndRubricsRelaxBeauty($value);
//        $this->setFiltersAndRubricsRelaxFitness($value);
//       $this->setFiltersAndRubricsRelaxRest($value);
//        $this->setFiltersAndRubricsSome($value);
    }

    protected function setFiltersAndRubricsRelaxFitness($value)
    {
        $this->setKitFilter($value, 'виды занятий:', [
            'групповые занятия' => [
                'filters' => [
                    ['id' => '5023', 'value' => 'групповые занятия'],
                ],
            ],
            'детские занятия' => [
                'filters' => [
                    ['id' => '4653', 'value' => 'для детей'],
                ],
            ],
            'занятия для беременных' => [
                'filters' => [
                    ['id' => '4653', 'value' => 'для беременных'],
                ],
            ],
            'водные занятия' => [
                'filters' => [
                    ['id' => '5003', 'value' => 'аквааэробика'],
                ],
            ],
            'боевые искусства' => [
                'filters' => [
                    ['id' => '4993', 'value' => 'боевые искусства'],
                ],
            ],
            'игровой спорт' => [
                'filters' => [
                    ['id' => '4993', 'value' => 'спортивные игры'],
                ],
            ],

        ]);
        $this->setFilter('4123', $value, 'оплата картой', 'Есть', true, null, 'Нет');
        $this->setFilter('4403', $value, 'бассейн', 'Есть', false, 10328803);
        $this->setEasyFilter('5223', $value, 'бассейн', 'бассейны');
        $this->setKitFilter($value, 'аренда зала:', [
            'зал для пинг-понга' => [
                'filters' => [
                    ['id' => '4993', 'value' => 'спортивные игры'],
                    ['id' => '5013', 'value' => 'пин-понг'],
                ],
            ],
            'теннисный корт' => [
                'filters' => [
                    ['id' => '4993', 'value' => 'спортивные игры'],
                    ['id' => '5013', 'value' => 'для детей'],
                ],
            ],
        ]);
        $this->setKitFilter($value, 'групповые занятия:', [
            'йога' => [
                'filters' => [
                    ['id' => '5003', 'value' => 'йога'],
                    ['id' => '5023', 'value' => 'групповые занятия'],
                    ['id' => '4993', 'value' => 'фитнес'],
                ],
            ],
            'шейпинг' => [
                'filters' => [
                    ['id' => '5003', 'value' => 'шейпинг'],
                    ['id' => '5023', 'value' => 'групповые занятия'],
                    ['id' => '4993', 'value' => 'фитнес'],
                ],
            ],
            'пилатес' => [
                'filters' => [
                    ['id' => '5003', 'value' => 'пилатес'],
                    ['id' => '5023', 'value' => 'групповые занятия'],
                    ['id' => '4993', 'value' => 'фитнес'],
                ],
            ],
            'стрейчинг' => [
                'filters' => [
                    ['id' => '5003', 'value' => 'стретчинг'],
                    ['id' => '5023', 'value' => 'групповые занятия'],
                    ['id' => '4993', 'value' => 'фитнес'],
                ],
            ],
            'степ' => [
                'filters' => [
                    ['id' => '5003', 'value' => 'степ'],
                    ['id' => '5023', 'value' => 'групповые занятия'],
                    ['id' => '4993', 'value' => 'фитнес'],
                ],
            ],
            'калланетика' => [
                'filters' => [
                    ['id' => '5003', 'value' => 'калланетика'],
                    ['id' => '5023', 'value' => 'групповые занятия'],
                    ['id' => '4993', 'value' => 'фитнес'],
                ],
            ],
            'силовая аэробика' => [
                'filters' => [
                    ['id' => '4993', 'value' => 'фитнес'],
                    ['id' => '5003', 'value' => 'аэробика'],
                    ['id' => '5023', 'value' => 'групповые занятия'],
                    ['id' => '4993', 'value' => 'силовые тренировки'],
                ],
            ],
            'аэробика' => [
                'filters' => [
                    ['id' => '5003', 'value' => 'аэробика'],
                    ['id' => '5023', 'value' => 'групповые занятия'],
                    ['id' => '4993', 'value' => 'фитнес'],
                ],
            ],
            'ABL' => [
                'filters' => [
                    ['id' => '4993', 'value' => 'фитнес'],
                    ['id' => '5023', 'value' => 'групповые занятия'],
                    ['id' => '4993', 'value' => 'силовые тренировки'],
                ],
            ],
            'фитбол' => [
                'filters' => [
                    ['id' => '5003', 'value' => 'аэробика'],
                    ['id' => '5023', 'value' => 'групповые занятия'],
                    ['id' => '4993', 'value' => 'фитнес'],
                    ['id' => '4653', 'value' => 'для детей'],
                    ['id' => '4653', 'value' => 'для беременных'],
                    ['id' => '4653', 'value' => 'для людей старшего возраста'],
                    ['id' => '4653', 'value' => 'для инвалидов'],
                ],
            ],
            'interval' => [
                'filters' => [
                    ['id' => '5023', 'value' => 'групповые занятия'],
                    ['id' => '4993', 'value' => 'силовые тренировки'],
                    ['id' => '4993', 'value' => 'кардиотренировки'],
                    ['id' => '4993', 'value' => 'танцевальные упражнения'],
                    ['id' => '4993', 'value' => 'фитнес'],
                ],
            ],
            'свитчинг' => [
                'filters' => [
                    ['id' => '5023', 'value' => 'групповые занятия'],
                    ['id' => '5003', 'value' => 'функциональный тренинг'],
                    ['id' => '5003', 'value' => 'степ'],
                    ['id' => '4993', 'value' => 'фитнес'],
                ],
            ],
            'аэромикс' => [
                'filters' => [
                    ['id' => '5003', 'value' => 'степ'],
                    ['id' => '5023', 'value' => 'групповые занятия'],
                    ['id' => '4993', 'value' => 'фитнес'],
                ],
            ],
            'upper body' => [
                'filters' => [
                    ['id' => '4993', 'value' => 'силовые тренировки'],
                    ['id' => '5023', 'value' => 'групповые занятия'],
                    ['id' => '4993', 'value' => 'фитнес'],
                ],
            ],
            'lower body' => [
                'filters' => [
                    ['id' => '4993', 'value' => 'силовые тренировки'],
                    ['id' => '5023', 'value' => 'групповые занятия'],
                    ['id' => '4993', 'value' => 'фитнес'],
                ],
            ],
            'body flex' => [
                'filters' => [
                    ['id' => '5003', 'value' => 'бодифлекс'],
                    ['id' => '5023', 'value' => 'групповые занятия'],
                    ['id' => '4993', 'value' => 'фитнес'],
                ],
            ],
            'флекс' => [
                'filters' => [
                    ['id' => '5003', 'value' => 'бодифлекс'],
                    ['id' => '5023', 'value' => 'групповые занятия'],
                    ['id' => '4993', 'value' => 'фитнес'],
                ],
            ],
            'боди-памп' => [
                'filters' => [
                    ['id' => '4993', 'value' => 'силовые тренировки'],
                    ['id' => '5023', 'value' => 'групповые занятия'],
                    ['id' => '4993', 'value' => 'фитнес'],
                ],
            ],
            'iron grip' => [
                'filters' => [
                    ['id' => '4993', 'value' => 'силовые тренировки'],
                    ['id' => '5023', 'value' => 'групповые занятия'],
                    ['id' => '4993', 'value' => 'фитнес'],
                ],
            ],
            'слайд-аэробика' => [
                'filters' => [
                    ['id' => '5003', 'value' => 'аэробика'],
                    ['id' => '5023', 'value' => 'групповые занятия'],
                    ['id' => '4993', 'value' => 'фитнес'],
                ],
            ],


        ]);
        $this->setKitFilter($value, 'игровой спорт:', [
            'теннис' => [
                'filters' => [
                    ['id' => '5013', 'value' => 'теннис большой'],
                    ['id' => '4993', 'value' => 'спортивные игры'],
                ],
            ],
            'футбол' => [
                'filters' => [
                    ['id' => '5013', 'value' => 'футбол'],
                    ['id' => '4993', 'value' => 'спортивные игры'],
                ],
            ],
            'настольный теннис' => [
                'filters' => [
                    ['id' => '5013', 'value' => 'теннис настольный '],
                    ['id' => '4993', 'value' => 'спортивные игры'],
                ],
            ],
            'фехтование' => [
                'filters' => [
                    ['id' => '5013', 'value' => 'фехтование'],
                    ['id' => '4993', 'value' => 'спортивные игры'],
                ],
            ],
            'бадминтон' => [
                'filters' => [
                    ['id' => '5013', 'value' => 'бадминтон'],
                    ['id' => '4993', 'value' => 'спортивные игры'],
                ],
            ],
            'сквош' => [
                'filters' => [
                    ['id' => '5013', 'value' => 'сквош'],
                    ['id' => '4993', 'value' => 'спортивные игры'],
                ],
            ],
            'боулинг' => [
                'filters' => [
                    ['id' => '5013', 'value' => 'боулинг'],
                    ['id' => '4993', 'value' => 'спортивные игры'],
                ],
            ],
            'скалолазание' => [
                'filters' => [
                    ['id' => '5013', 'value' => 'скалолазание'],
                    ['id' => '4993', 'value' => 'спортивные игры'],
                ],
            ],
            'прыжки на батуте' => [
                'filters' => [
                    ['id' => '5013', 'value' => 'прыжки на батуте'],
                    ['id' => '4993', 'value' => 'спортивные игры'],
                ],
            ],
            'мини-футбол' => [
                'filters' => [
                    ['id' => '5013', 'value' => 'футбол'],
                    ['id' => '4993', 'value' => 'спортивные игры'],
                ],
            ],
            'волейбол' => [
                'filters' => [
                    ['id' => '5013', 'value' => 'волейбол'],
                    ['id' => '4993', 'value' => 'спортивные игры'],
                ],
            ],
            'баскетбол' => [
                'filters' => [
                    ['id' => '5013', 'value' => 'баскетбол'],
                    ['id' => '4993', 'value' => 'спортивные игры'],
                ],
            ],
            'бильярд' => [
                'filters' => [
                    ['id' => '5013', 'value' => 'бильярд'],
                    ['id' => '4993', 'value' => 'спортивные игры'],
                ],
            ],
            'гольф' => [
                'filters' => [
                    ['id' => '5013', 'value' => 'гольф'],
                    ['id' => '4993', 'value' => 'спортивные игры'],
                ],
            ],
            'стрит бол' => [
                'filters' => [
                    ['id' => '5013', 'value' => 'футбол'],
                    ['id' => '4993', 'value' => 'спортивные игры'],
                ],
            ],
            'пейнтбол' => [
                'filters' => [
                    ['id' => '5013', 'value' => 'пейнтбол'],
                    ['id' => '4993', 'value' => 'спортивные игры'],
                ],
            ],
        ]);
        $this->setKitFilter($value, 'водные занятия:', [
            'аквааэробика' => [
                'filters' => [
                    ['id' => '5003', 'value' => 'аквааэробика'],
                    ['id' => '4993', 'value' => 'фитнес'],
                ],
            ],
            'занятия для детей' => [
                'filters' => [
                    ['id' => '4653', 'value' => 'для детей'],
                ],
            ],
            'занятия для беременных' => [
                'filters' => [
                    ['id' => '4653', 'value' => 'для беременных'],
                ],
            ],
            'занятия для грудных детей' => [
                'filters' => [
                    ['id' => '4653', 'value' => 'для грудных детей'],
                ],
            ],
            'обучение плаванию взрослых' => [
                'filters' => [
                    ['id' => '4373', 'value' => 'профессиональное обучение'],
                ],
            ],
        ]);
        $this->setKitFilter($value, 'общеукрепляющие программы:', [
            'восстановление после родов' => [
                'filters' => [
                    ['id' => '4653', 'value' => 'для женщин после родов'],
                ],
            ],
            '50+' => [
                'filters' => [
                    ['id' => '4653', 'value' => 'для людей старшего возраста '],
                ],
            ],
            'занятия для беременных' => [
                'filters' => [
                    ['id' => '4653', 'value' => 'для беременных'],
                ],
            ],
            'анти-возрастные программы' => [
                'filters' => [
                    ['id' => '4653', 'value' => 'для людей старшего возраста '],
                ],
            ],
        ]);
        $this->setEasyFilter('4993', $value, 'танцы', 'танцевальные упражнения');
        $this->setEasyFilter('4653', $value, 'детский фитнес', 'для детей');
        $this->setKitFilter($value, 'занятия для детей:', [
            'восточные единоборства' => [
                'filters' => [
                    ['id' => '4653', 'value' => 'для детей'],
                    ['id' => '4993', 'value' => 'боевые искусства'],
                ],
            ],
            'аэробика' => [
                'filters' => [
                    ['id' => '5003', 'value' => 'аэробика'],
                    ['id' => '4993', 'value' => 'фитнес'],
                    ['id' => '4653', 'value' => 'для детей'],
                ],
            ],
            'хореография' => [
                'filters' => [
                    ['id' => '4653', 'value' => 'для детей'],
                    ['id' => '4993', 'value' => 'танцевальные упражнения'],
                ],
            ],
            'тренировки' => [
                'filters' => [
                    ['id' => '4653', 'value' => 'для детей'],
                    ['id' => '4993', 'value' => 'силовые тренировки'],
                ],
            ],
            'теннис' => [
                'filters' => [
                    ['id' => '4993', 'value' => 'спортивные игры'],
                    ['id' => '4653', 'value' => 'для детей'],
                ],
            ],
            'йога' => [
                'filters' => [
                    ['id' => '4653', 'value' => 'для детей'],
                    ['id' => '4993', 'value' => 'фитнес'],
                ],
            ],
            'детский бассейн' => [
                'filters' => [
                    ['id' => '4403', 'value' => 'бассейн'],
                    ['id' => '4653', 'value' => 'для детей'],
                ],
            ],
        ]);
        $this->setEasyFilter('4403', $value, 'детская комната', 'детская комната');
        $this->setKitFilter($value, 'боевые искусства:', [
            'бокс' => [
                'filters' => [
                    ['id' => '5233', 'value' => 'бокс'],
                    ['id' => '4993', 'value' => 'спортивные игры'],
                ],
            ],

            'каратэ' => [
                'filters' => [
                    ['id' => '5233', 'value' => 'каратэ'],
                    ['id' => '4993', 'value' => 'спортивные игры'],
                ],
            ],
            'айкидо' => [
                'filters' => [
                    ['id' => '5233', 'value' => 'айкидо'],
                    ['id' => '4993', 'value' => 'спортивные игры'],
                ],
            ],
            'джиу-джитсу' => [
                'filters' => [
                    ['id' => '5233', 'value' => 'джиу-джитсу'],
                    ['id' => '4993', 'value' => 'спортивные игры'],
                ],
            ],
            'кикбоксинг' => [
                'filters' => [
                    ['id' => '5233', 'value' => 'кикбоксинг'],
                    ['id' => '4993', 'value' => 'спортивные игры'],
                ],
            ],
            'рукопашный бой' => [
                'filters' => [
                    ['id' => '5233', 'value' => 'рукопашный бой'],
                    ['id' => '4993', 'value' => 'спортивные игры'],
                ],
            ],
            'таэквондо' => [
                'filters' => [
                    ['id' => '5233', 'value' => 'таэквондо'],
                    ['id' => '4993', 'value' => 'спортивные игры'],
                ],
            ],
            'дзюдо' => [
                'filters' => [
                    ['id' => '5233', 'value' => 'дзюдо'],
                    ['id' => '4993', 'value' => 'спортивные игры'],
                ],
            ],
            'самбо' => [
                'filters' => [
                    ['id' => '5233', 'value' => 'самбо'],
                    ['id' => '4993', 'value' => 'спортивные игры'],
                ],
            ],
            'самооборона' => [
                'filters' => [
                    ['id' => '5233', 'value' => 'самооборона'],
                    ['id' => '4993', 'value' => 'спортивные игры'],
                ],
            ],
            'вин чун' => [
                'filters' => [
                    ['id' => '5233', 'value' => 'вин чун'],
                    ['id' => '4993', 'value' => 'спортивные игры'],
                ],
            ],
            'кобудо' => [
                'filters' => [
                    ['id' => '5233', 'value' => 'кобудо'],
                    ['id' => '4993', 'value' => 'спортивные игры'],
                ],
            ],
            'ушу' => [
                'filters' => [
                    ['id' => '5233', 'value' => 'ушу'],
                    ['id' => '4993', 'value' => 'спортивные игры'],
                ],
            ],
            'тай-чи' => [
                'filters' => [
                    ['id' => '5233', 'value' => 'тай-чи'],
                    ['id' => '4993', 'value' => 'спортивные игры'],
                ],
            ],
            'хапкидо' => [
                'filters' => [
                    ['id' => '5233', 'value' => 'хапкидо'],
                    ['id' => '4993', 'value' => 'спортивные игры'],
                ],
            ],
            'кендо' => [
                'filters' => [
                    ['id' => '5233', 'value' => 'кендо'],
                    ['id' => '4993', 'value' => 'спортивные игры'],
                ],
            ],
            'бокс для женщин' => [
                'filters' => [
                    ['id' => '5233', 'value' => 'бокс'],
                    ['id' => '4993', 'value' => 'спортивные игры'],
                ],
            ],
            'боевое самбо' => [
                'filters' => [
                    ['id' => '5233', 'value' => 'самбо'],
                    ['id' => '4993', 'value' => 'спортивные игры'],
                ],
            ],
            'окинава тэ' => [
                'filters' => [
                    ['id' => '5233', 'value' => 'окинава тэ'],
                    ['id' => '4993', 'value' => 'спортивные игры'],
                ],
            ],
            'айки-кэн' => [
                'filters' => [
                    ['id' => '5233', 'value' => 'айки-кэн'],
                    ['id' => '4993', 'value' => 'спортивные игры'],
                ],
            ],
            'киокушинкай' => [
                'filters' => [
                    ['id' => '5233', 'value' => 'киокушинкай'],
                    ['id' => '4993', 'value' => 'спортивные игры'],
                ],
            ],
            'греко-римская борьба' => [
                'filters' => [
                    ['id' => '5233', 'value' => 'борьба греко-римская'],
                    ['id' => '4993', 'value' => 'спортивные игры'],
                ],
            ],
            'вольная борьба' => [
                'filters' => [
                    ['id' => '5233', 'value' => 'борьба вольная'],
                    ['id' => '4993', 'value' => 'спортивные игры'],
                ],
            ],
            'тайский бокс' => [
                'filters' => [
                    ['id' => '5233', 'value' => 'бокс тайский'],
                    ['id' => '4993', 'value' => 'спортивные игры'],
                ],
            ],
            'славяно-горицкая борьба' => [
                'filters' => [
                    ['id' => '5233', 'value' => 'борьба славяно-горицкая'],
                    ['id' => '4993', 'value' => 'спортивные игры'],
                ],
            ],
            'сумо' => [
                'filters' => [
                    ['id' => '5233', 'value' => 'сумо'],
                    ['id' => '4993', 'value' => 'спортивные игры'],
                ],
            ],
            'рестлинг' => [
                'filters' => [
                    ['id' => '5233', 'value' => 'рестлинг'],
                    ['id' => '4993', 'value' => 'спортивные игры'],
                ],
            ],
            'чанбара' => [
                'filters' => [
                    ['id' => '5233', 'value' => 'чанбара'],
                    ['id' => '4993', 'value' => 'спортивные игры'],
                ],
            ],
            'панкратион' => [
                'filters' => [
                    ['id' => '5233', 'value' => 'панкратион'],
                    ['id' => '4993', 'value' => 'спортивные игры'],
                ],
            ],
            'тансудо' => [
                'filters' => [
                    ['id' => '5233', 'value' => 'тансудо'],
                    ['id' => '4993', 'value' => 'спортивные игры'],
                ],
            ],
            'комбатан' => [
                'filters' => [
                    ['id' => '5233', 'value' => 'комбатан'],
                    ['id' => '4993', 'value' => 'спортивные игры'],
                ],
            ],
        ]);
        $this->setKitFilter($value, 'Танцевальные стили:', [
            'go-go' => [
                'filters' => [
                    ['id' => '5143', 'value' => 'Go-go Dance'],
                    ['id' => '4993', 'value' => 'танцевальные упражнения'],
                ],
            ],
            'hip-hop' => [
                'filters' => [
                    ['id' => '5133', 'value' => 'хип-хоп (Hip-Hop)'],
                    ['id' => '4993', 'value' => 'танцевальные упражнения'],
                ],
            ],
            'бальные' => [
                'filters' => [
                    ['id' => '5063', 'value' => 'бальные (спортивные) танцы'],
                    ['id' => '4993', 'value' => 'танцевальные упражнения'],
                ],
            ],
            'восточные' => [
                'filters' => [
                    ['id' => '5093', 'value' => 'восточные'],
                    ['id' => '4993', 'value' => 'танцевальные упражнения'],
                ],
            ],
            'латина' => [
                'filters' => [
                    ['id' => '5093', 'value' => 'латиноамериканские'],
                    ['id' => '4993', 'value' => 'танцевальные упражнения'],
                ],
            ],
            'современные' => [
                'filters' => [
                    ['id' => '5063', 'value' => 'современные танцы'],
                    ['id' => '4993', 'value' => 'танцевальные упражнения'],
                ],
            ],
            'стрип-дэнс' => [
                'filters' => [
                    ['id' => '5163', 'value' => 'стрип-пластика'],
                    ['id' => '4993', 'value' => 'танцевальные упражнения'],
                ],
            ],
            'хореография' => [
                'filters' => [
                    ['id' => '5153', 'value' => 'контемпорари'],
                    ['id' => '4993', 'value' => 'танцевальные упражнения'],
                ],
            ],
            'R&B' => [
                'filters' => [
                    ['id' => '5143', 'value' => 'RnB'],
                    ['id' => '4993', 'value' => 'танцевальные упражнения'],
                ],
            ],
            'tecktonik' => [
                'filters' => [
                    ['id' => '5143', 'value' => 'тектоник (Tecktonik), электродэнс'],
                    ['id' => '4993', 'value' => 'танцевальные упражнения'],
                ],
            ],
            'афро-джаз' => [
                'filters' => [
                    ['id' => '5153', 'value' => 'афро-джаз'],
                    ['id' => '4993', 'value' => 'танцевальные упражнения'],
                ],
            ],
            'балет' => [
                'filters' => [
                    ['id' => '5063', 'value' => 'балет'],
                    ['id' => '4993', 'value' => 'танцевальные упражнения'],
                ],
            ],
            'бачата' => [
                'filters' => [
                    ['id' => '5173', 'value' => 'бачата'],
                    ['id' => '4993', 'value' => 'танцевальные упражнения'],
                ],
            ],
            'брейк-дэнс' => [
                'filters' => [
                    ['id' => '5133', 'value' => 'брейк-данc (Break Dance)'],
                    ['id' => '4993', 'value' => 'танцевальные упражнения'],
                ],
            ],
            'буги-вуги' => [
                'filters' => [
                    ['id' => '5153', 'value' => 'буги-вуги'],
                    ['id' => '4993', 'value' => 'танцевальные упражнения'],
                ],
            ],
            'джаз' => [
                'filters' => [
                    ['id' => '5153', 'value' => 'джаз (Jazz)'],
                    ['id' => '4993', 'value' => 'танцевальные упражнения'],
                ],
            ],
            'импровизация' => [
                'filters' => [
                    ['id' => '5063', 'value' => 'танцевальная импровизация (трайбл)'],
                    ['id' => '4993', 'value' => 'танцевальные упражнения'],
                ],
            ],
            'индийские' => [
                'filters' => [
                    ['id' => '5093', 'value' => 'индийские'],
                    ['id' => '4993', 'value' => 'танцевальные упражнения'],
                ],
            ],
            'ирландские' => [
                'filters' => [
                    ['id' => '5093', 'value' => 'ирландские, британские, шотландские'],
                    ['id' => '4993', 'value' => 'танцевальные упражнения'],
                ],
            ],
            'исторические' => [
                'filters' => [
                    ['id' => '5063', 'value' => 'исторические танцы'],
                    ['id' => '4993', 'value' => 'танцевальные упражнения'],
                ],
            ],
            'капоэйра' => [
                'filters' => [
                    ['id' => '5163', 'value' => 'капоэйра'],
                    ['id' => '4993', 'value' => 'танцевальные упражнения'],
                ],
            ],
            'клубные' => [
                'filters' => [
                    ['id' => '5063', 'value' => 'современные танцы'],
                    ['id' => '4993', 'value' => 'танцевальные упражнения'],
                ],
            ],
            'контактная импровизация' => [
                'filters' => [
                    ['id' => '5063', 'value' => 'танцевальная импровизация (трайбл)'],
                    ['id' => '4993', 'value' => 'танцевальные упражнения'],
                ],
            ],
            'контемпорари' => [
                'filters' => [
                    ['id' => '5153', 'value' => 'контемпорари'],
                    ['id' => '4993', 'value' => 'танцевальные упражнения'],
                ],
            ],
            'крамп' => [
                'filters' => [
                    ['id' => '5133', 'value' => 'крамп'],
                    ['id' => '4993', 'value' => 'танцевальные упражнения'],
                ],
            ],
            'линди-хоп' => [
                'filters' => [
                    ['id' => '5153', 'value' => 'линди-хоп'],
                    ['id' => '4993', 'value' => 'танцевальные упражнения'],
                ],
            ],
            'локинг' => [
                'filters' => [
                    ['id' => '5133', 'value' => 'локинг'],
                    ['id' => '4993', 'value' => 'танцевальные упражнения'],
                ],
            ],
            'модерн' => [
                'filters' => [
                    ['id' => '5153', 'value' => 'модерн'],
                    ['id' => '4993', 'value' => 'танцевальные упражнения'],
                ],
            ],
            'меренге' => [
                'filters' => [
                    ['id' => '5173', 'value' => 'меренге'],
                    ['id' => '4993', 'value' => 'танцевальные упражнения'],
                ],
            ],
            'народные' => [
                'filters' => [
                    ['id' => '5063', 'value' => 'народные (этнические) танцы'],
                    ['id' => '4993', 'value' => 'танцевальные упражнения'],
                ],
            ],
            'поппинг' => [
                'filters' => [
                    ['id' => '5133', 'value' => 'паппинг (Poppin)'],
                    ['id' => '4993', 'value' => 'танцевальные упражнения'],
                ],
            ],
            'реггетон' => [
                'filters' => [
                    ['id' => '5173', 'value' => 'реггетон'],
                    ['id' => '4993', 'value' => 'танцевальные упражнения'],
                ],
            ],
            'рок-н-ролл' => [
                'filters' => [
                    ['id' => '5163', 'value' => 'рок-н-ролл'],
                    ['id' => '4993', 'value' => 'танцевальные упражнения'],
                ],
            ],
            'сальса' => [
                'filters' => [
                    ['id' => '5173', 'value' => 'сальса'],
                    ['id' => '4993', 'value' => 'танцевальные упражнения'],
                ],
            ],
            'свинг' => [
                'filters' => [
                    ['id' => '5153', 'value' => 'свинг'],
                    ['id' => '4993', 'value' => 'танцевальные упражнения'],
                ],
            ],
            'танго' => [
                'filters' => [
                    ['id' => '5113', 'value' => 'танго классическое'],
                    ['id' => '4993', 'value' => 'танцевальные упражнения'],
                ],
            ],
            'танец на шесте' => [
                'filters' => [
                    ['id' => '5163', 'value' => 'танец на пилоне (Pole Dance)'],
                    ['id' => '4993', 'value' => 'танцевальные упражнения'],
                ],
            ],

            'фламенко' => [
                'filters' => [
                    ['id' => '5173', 'value' => 'фламенко'],
                    ['id' => '4993', 'value' => 'танцевальные упражнения'],
                ],
            ],
            'хастл' => [
                'filters' => [
                    ['id' => '5133', 'value' => 'хастл (Hustle)'],
                    ['id' => '4993', 'value' => 'танцевальные упражнения'],
                ],
            ],
            'хаус' => [
                'filters' => [
                    ['id' => '5133', 'value' => 'хаус (House)'],
                    ['id' => '4993', 'value' => 'танцевальные упражнения'],
                ],
            ],
            'шотландские' => [
                'filters' => [
                    ['id' => '5093', 'value' => 'ирландские, британские, шотландские'],
                    ['id' => '4993', 'value' => 'танцевальные упражнения'],
                ],
            ],
        ]);
        $this->setFilter('4093', $value, 'бесплатный wi-fi', 'Есть', true, null, 'Нет');
        $this->setKitFilter($value, 'залы:', [
            'тренажерный зал' => [
                'filters' => [
                    ['id' => '5223', 'value' => 'тренажерные залы'],
                ],
                'rubrics' => [
                    ['id' => '10328773'],
                ],
            ],
            'кардиозал' => [
                'filters' => [
                    ['id' => '4993', 'value' => 'кардиотренировки'],
                ],
            ],
            'зал восточных единоборств' => [
                'filters' => [
                    ['id' => '4993', 'value' => 'боевые искусства'],
                ],
            ],
            'зал бокса' => [
                'filters' => [
                    ['id' => '4993', 'value' => 'боевые искусства'],
                ],
            ],
        ]);
        $this->setEasyFilter('5053', $value, 'личный тренер', 'персональный тренер');
        $this->setKitFilter($value, 'спортивная медицина:', [
            'диетолог' => [
                'filters' => [
                    ['id' => '5053', 'value' => 'консультации диетолога'],
                ],
            ],
            'консультация врача' => [
                'filters' => [
                    ['id' => '5053', 'value' => 'консультации врача'],
                ],
            ],
            'функциональная диагностика' => [
                'filters' => [
                    ['id' => '5053', 'value' => 'фитнес-диагностика'],
                ],
            ],
            'фитнес-тестирование' => [
                'filters' => [
                    ['id' => '5053', 'value' => 'фитнес-диагностика'],
                ],
            ],
            'анализ состава тела' => [
                'filters' => [
                    ['id' => '5053', 'value' => 'фитнес-диагностика'],
                ],
            ],
        ]);
        $this->setEasyFilter('4403', $value, 'фито-бар', 'фитнес-бар');
        $this->setFilter('4113', $value, 'гостевая парковка', 'Есть', true, null, 'Нет');

        if ($this->mainRubric == '10328783')
        {
            $this->setKitFilter($value, 'целевая аудитория:', [
                'для детей' => [
                    'filters' => [
                        ['id' => '4653', 'value' => 'для детей'],
                    ],
                ],
                'для взрослых' => [
                    'filters' => [
                        ['id' => '4653', 'value' => 'для мужчин'],
                        ['id' => '4653', 'value' => 'для женщин'],
                    ],
                ],
                'для взрослых и детей' => [
                    'filters' => [
                        ['id' => '4653', 'value' => 'для детей'],
                        ['id' => '4653', 'value' => 'для мужчин'],
                        ['id' => '4653', 'value' => 'для женщин'],
                        ['id' => '4653', 'value' => 'для подростков'],
                    ],
                ],
                'для пожилых людей' => [
                    'filters' => [
                        ['id' => '4653', 'value' => 'для людей старшего возраста'],
                    ],
                ],
            ]);
            $this->setFilter('5073', $value, 'постановка свадебного танца', 'Есть', true, null, 'Нет');
            $this->setEasyFilter('5053', $value, 'личный тренер', 'персональный тренер');
            $this->setEasyFilter('4403', $value, 'душ', 'душевые');
        }
        if ($this->mainRubric = '10328793')
        {
            $this->setEasyFilter('4653', $value, 'детская йога', 'для детей');
            $this->setEasyFilter('4653', $value, 'йога для беременных', 'для беременных');
            $this->setEasyFilter('4403', $value, ' кафе', 'кафе');
            $this->setKitFilter($value, 'йога:', [
                'пилатес' => [
                    'filters' => [
                        ['id' => '5183', 'value' => 'пилатес'],
                    ],
                ],
                'кундалини' => [
                    'filters' => [
                        ['id' => '5183', 'value' => 'кундалини'],
                    ],
                ],
                'хатха' => [
                    'filters' => [
                        ['id' => '5183', 'value' => 'хатха'],
                    ],
                ],
                'айенгар' => [
                    'filters' => [
                        ['id' => '5183', 'value' => 'айенгар'],
                    ],
                ],
                'для беременных' => [
                    'filters' => [
                        ['id' => '4653', 'value' => 'для беременных'],
                    ],
                ],
                'аштанга' => [
                    'filters' => [
                        ['id' => '5183', 'value' => 'аштанга виньяса'],
                    ],
                ],
                'сахаджа' => [
                    'filters' => [
                        ['id' => '5183', 'value' => 'сахаджа'],
                    ],
                ],
                'раджа' => [
                    'filters' => [
                        ['id' => '5183', 'value' => 'раджа'],
                    ],
                ],
                'йогалатес' => [
                    'filters' => [
                        ['id' => '5183', 'value' => 'йогалатес'],
                    ],
                ],
                'тантра' => [
                    'filters' => [
                        ['id' => '5183', 'value' => 'тантра'],
                    ],
                ],
                'мантра' => [
                    'filters' => [
                        ['id' => '5183', 'value' => 'мантра'],
                    ],
                ],
                'крийя' => [
                    'filters' => [
                        ['id' => '5183', 'value' => 'крия'],
                    ],
                ],
                'гуру' => [
                    'filters' => [
                        ['id' => '5183', 'value' => 'гуру'],
                    ],
                ],
                'даосская' => [
                    'filters' => [
                        ['id' => '5183', 'value' => 'даосская'],
                    ],
                ],
                'шивананда' => [
                    'filters' => [
                        ['id' => '5183', 'value' => 'шивананда'],
                    ],
                ],
                'цигун' => [
                    'filters' => [
                        ['id' => '', 'value' => 'цигун'],
                    ],
                ],
                'пранаяма' => [
                    'filters' => [
                        ['id' => '', 'value' => 'прана'],
                    ],
                ],
                'лайя' => [
                    'filters' => [
                        ['id' => '', 'value' => 'лайя'],
                    ],
                ],
                'карма' => [
                    'filters' => [
                        ['id' => '', 'value' => 'карма'],
                    ],
                ],
                'янтра' => [
                    'filters' => [
                        ['id' => '', 'value' => 'янтра'],
                    ],
                ],
                'бхакти' => [
                    'filters' => [
                        ['id' => '', 'value' => 'бхакти'],
                    ],
                ],
                'горячая' => [
                    'filters' => [
                        ['id' => '5183', 'value' => 'горячая'],
                    ],
                ],
                'нидра' => [
                    'filters' => [
                        ['id' => '5183', 'value' => 'нидра (йога сна)'],
                    ],
                ],
                'йога 23' => [
                    'filters' => [
                        ['id' => '5183', 'value' => 'йога 23'],
                    ],
                ],
                'йогатерапия' => [
                    'filters' => [
                        ['id' => '5183', 'value' => 'йога 23'],
                        ['id' => '5183', 'value' => 'йогатерапия'],
                    ],
                ],
                'женская' => [
                    'filters' => [
                        ['id' => '4653', 'value' => 'для женщин'],
                    ],
                ],
                'для пожилых' => [
                    'filters' => [
                        ['id' => '4653', 'value' => 'для людей страшего возраста'],
                    ],
                ],
            ]);
        }
        if ($this->mainRubric = '10328803')
        {
            $this->setKitFilter($value, 'бассейн:', [
                'крытый' => [
                    'filters' => [
                        ['id' => '5213', 'value' => 'закрытые'],
                    ],
                ],
                'открытый' => [
                    'filters' => [
                        ['id' => '5213', 'value' => 'открытые'],
                    ],
                ],
                'открытый и крытый' => [
                    'filters' => [
                        ['id' => '5213', 'value' => 'закрытые'],
                        ['id' => '5213', 'value' => 'открытые'],
                    ],
                ],
            ]);
        }
    }

    protected function setFiltersAndRubricsRelaxRest($value)
    {
        //
        $this->setKitFilter($value, 'кухня:', $this->kitchen);
        $this->setFilter('4123', $value, 'оплата картой', 'Есть', true, null, 'Нет');
        $this->setFilter('4093', $value, 'бесплатный wi-fi', 'Есть', true, null, 'Нет');
        $this->setKitFilter($value, 'парковка:', [
            'VIP' => [
                'filters' => [
                    ['id' => '4113', 'value' => 'Есть'],
                ],
            ],
            'автоматическая' => [
                'filters' => [
                    ['id' => '4113', 'value' => 'Есть'],
                ],
            ],
            'бесплатная' => [
                'filters' => [
                    ['id' => '4113', 'value' => 'Есть'],
                ],
            ],
            'гостевая' => [
                'filters' => [
                    ['id' => '4113', 'value' => 'Есть'],
                ],
            ],
            'длительная' => [
                'filters' => [
                    ['id' => '4113', 'value' => 'Есть'],
                ],
            ],
            'для большегрузного транспорта' => [
                'filters' => [
                    ['id' => '4113', 'value' => 'Есть'],
                ],
            ],
            'для легковых автомобилей' => [
                'filters' => [
                    ['id' => '4113', 'value' => 'Есть'],
                ],
            ],
            'краткосрочная' => [
                'filters' => [
                    ['id' => '4113', 'value' => 'Есть'],
                ],
            ],
            'крытая' => [
                'filters' => [
                    ['id' => '4113', 'value' => 'Есть'],
                ],
            ],
            'многоуровневая' => [
                'filters' => [
                    ['id' => '4113', 'value' => 'Есть'],
                ],
            ],
            'наземная' => [
                'filters' => [
                    ['id' => '4113', 'value' => 'Есть'],
                ],
            ],
            'неохраняемая' => [
                'filters' => [
                    ['id' => '4113', 'value' => 'Есть'],
                ],
            ],
            'открытая' => [
                'filters' => [
                    ['id' => '4113', 'value' => 'Есть'],
                ],
            ],
            'охраняемая' => [
                'filters' => [
                    ['id' => '4113', 'value' => 'Есть'],
                ],
            ],
            'платная' => [
                'filters' => [
                    ['id' => '4113', 'value' => 'Есть'],
                ],
            ],
            'подземная' => [
                'filters' => [
                    ['id' => '4113', 'value' => 'Есть'],
                ],
            ],
            'предварительное бронирование' => [
                'filters' => [
                    ['id' => '4113', 'value' => 'Есть'],
                ],
            ],
            'перехватывающая' => [
                'filters' => [
                    ['id' => '4113', 'value' => 'Есть'],
                ],
            ],
            'уличная' => [
                'filters' => [
                    ['id' => '4113', 'value' => 'Есть'],
                ],
            ],
            'коммерческая' => [
                'filters' => [
                    ['id' => '4113', 'value' => 'Есть'],
                ],
            ],
        ]);
        $this->setEasyFilter('5263', $value, 'фейс-контроль', 'фейс-контроль (Face Control)');
        $this->setEasyFilter('5263', $value, 'дресс-код', 'дресс-код (Dress Code)');
        $this->setEasyFilter('5263', $value, 'вход свободный', 'свободный');
        $this->setKitFilter($value, 'музыка:', [
            'живая' => [
                'filters' => [
                    ['id' => '4003', 'value' => 'живая музыка'],
                ],
            ],
            'звёзды эстрады' => [
                'filters' => [
                    ['id' => '4003', 'value' => 'шоу-программа'],
                ],
            ],
        ]);
        $this->setEasyFilter('4003', $value, 'танцпол', 'танцпол');
        $this->setEasyFilter('5253', $value, 'стриптиз', 'стриптиз-клубы');
        $this->setFilter('4113', $value, 'гостевая парковка', 'Есть', true, null, 'Нет');
        $this->setEasyFilter('4073', $value, 'vip-зона', 'VIP-зал');
        $this->setEasyFilter('4073', $value, 'чилаут', 'чилаут');
        $this->setEasyFilter('4003', $value, 'шоу-программа', 'шоу-программа');
        $this->setEasyFilter('4003', $value, 'караоке', 'караоке');
        $this->setFilter('5353', $value, 'для детей', 'Есть', true, null, 'Нет');
        $this->setEasyFilter('4953', $value, 'ресторан', 'рестораны');

        if ($this->mainRubric == 10328813)
        {
            $this->setFilter('5243', $value, 'ресторан', 'ночные клубы', false, 10328523);
        }
        if ($this->mainRubric == 10328823)
        {
            $this->setFilter('5243', $value, 'ресторан', 'боулинг', false, 10328523);
            $this->setFilter('4953', $value, 'кафе', 'кафе', false, 10328533);
            $this->setEasyFilter('4953', $value, 'кафе', 'боулинг');
        }
        $this->setKitFilter($value, 'бассейн:', [
            'купель' => [
                'filters' => [
                    ['id' => '5323', 'value' => 'купель'],
                ],
            ],
            'джакузи' => [
                'filters' => [
                    ['id' => '5323', 'value' => 'джакузи'],
                ],
            ],
            'аквагорка' => [
                'filters' => [
                    ['id' => '5323', 'value' => 'аквагорка'],
                ],
            ],
            'водопад' => [
                'filters' => [
                    ['id' => '5323', 'value' => 'водопад'],
                ],
            ],
            'водосток' => [
                'filters' => [
                    ['id' => '5323', 'value' => 'водосток'],
                ],
            ],
            'гейзер' => [
                'filters' => [
                    ['id' => '5323', 'value' => 'гейзер'],
                ],
            ],
            'гидромассажная ванна' => [
                'filters' => [
                    ['id' => '5323', 'value' => 'гидромассажная ванна'],
                ],
            ],
            'фонтан' => [
                'filters' => [
                    ['id' => '5323', 'value' => 'фонтан'],
                ],
            ],
        ]);
        $this->setKitFilter($value, 'парная:', [
            'русская' => [
                'filters' => [
                    ['id' => '4453', 'value' => 'русская (паровая) баня'],
                ],
            ],
            'турецкая' => [
                'filters' => [
                    ['id' => '4453', 'value' => 'турецкая баня (хамам)'],
                ],
            ],
            'на дровах' => [
                'filters' => [
                    ['id' => '4453', 'value' => 'баня на дровах'],
                ],
            ],
            'японская' => [
                'filters' => [
                    ['id' => '4453', 'value' => 'японская баня'],
                ],
            ],
            'финская' => [
                'filters' => [
                    ['id' => '4453', 'value' => 'финская сауна'],
                ],
            ],
            'инфракрасная' => [
                'filters' => [
                    ['id' => '4453', 'value' => 'инфракрасная сауна'],
                ],
            ],
        ]);
        $this->setKitFilter($value, 'развлечения:', [
            'кинотеатр' => [
                'filters' => [
                    ['id' => '5333', 'value' => 'кинотеатр'],
                ],
            ],
            'аттракционы' => [
                'filters' => [
                    ['id' => '5333', 'value' => 'аттракционы'],
                ],
            ],
            'бильярд' => [
                'filters' => [
                    ['id' => '5013', 'value' => 'бильярд'],
                ],
            ],
            'боулинг' => [
                'filters' => [
                    ['id' => '5013', 'value' => 'боулинг'],
                ],
            ],
            'аниматоры' => [
                'filters' => [
                    ['id' => '5333', 'value' => 'аниматоры'],
                ],
            ],
            'концертный зал' => [
                'filters' => [
                    ['id' => '5333', 'value' => 'концертный зал'],
                ],
            ],
            'сухой бассейн с шариками' => [
                'filters' => [
                    ['id' => '5333', 'value' => 'сухой бассейн с шариками'],
                ],
            ],
            '5D-кинотеатр' => [
                'filters' => [
                    ['id' => '5333', 'value' => '5D-кинотеатр'],
                ],
            ],
            'игровой лабиринт' => [
                'filters' => [
                    ['id' => '5333', 'value' => 'игровые лабиринты'],
                ],
            ],
            'компьютерный клуб' => [
                'filters' => [
                    ['id' => '5333', 'value' => 'компьютерный клуб'],
                ],
            ],
            'тир' => [
                'filters' => [
                    ['id' => '5013', 'value' => 'тир'],
                ],
            ],
            'q-zar' => [
                'filters' => [
                    ['id' => '5013', 'value' => 'лазертаг'],
                ],
            ],
            'картинг' => [
                'filters' => [
                    ['id' => '5013', 'value' => 'картинг'],
                ],
            ],
            'роллердром' => [
                'filters' => [
                    ['id' => '5013', 'value' => 'роллердром'],
                ],
            ],
            'каток' => [
                'filters' => [
                    ['id' => '5013', 'value' => 'каток'],
                ],
            ],
            'игровые автоматы' => [
                'filters' => [
                    ['id' => '5333', 'value' => 'игровые автоматы'],
                ],
            ],
        ]);
        $this->setFilter('4113', $value, 'парковка', 'Есть', true, null, 'Нет');
        $this->setKitFilter($value, 'типы пейнтбольных площадок:', [
            'закрытые площадки' => [
                'filters' => [
                    ['id' => '5363', 'value' => 'закрытая'],
                ],
            ],
            'открытые площадки' => [
                'filters' => [
                    ['id' => '5363', 'value' => 'открытая'],
                ],
            ],
            'лесные площадки' => [
                'filters' => [
                    ['id' => '5363', 'value' => 'лесная'],
                ],
            ],
        ]);
        $this->setKitFilter($value, 'тип пейнтбола:', [
            'развлекательный' => [
                'filters' => [
                    ['id' => '5343', 'value' => 'тактический (сценарный)'],
                    ['id' => '5343', 'value' => 'спортивный'],
                ],
            ],
            'сценарный' => [
                'filters' => [
                    ['id' => '5323', 'value' => 'тактический (сценарный)'],
                ],
            ],
            'лесной' => [
                'filters' => [
                    ['id' => '5323', 'value' => 'спортивный'],
                ],
            ],

        ]);
        $this->setEasyFilter("5373", $value, 'школа пейнтбола', 'обучение');
        $this->setFilter('5353', $value, 'детский клуб', 'Есть', true, null, 'Нет');
        $this->setEasyFilter("5373", $value, 'ночной пейнтбол', 'ночные');
        $this->setEasyFilter("5373", $value, 'спортивный пейнтбол', 'спортивный');
        $this->setEasyFilter("5373", $value, 'зимний пейнтбол', 'зимние');
        $this->setEasyFilter("5373", $value, 'проведение турниров', 'турниры');
        $this->setEasyFilter("5443", $value, 'прокат игрового оборудования', 'игровое оборудование');
    }

    /**
     * @param $value
     * @return bool
     */
    protected function setFiltersAndRubricsRelaxBeauty($value)
    {
        $this->setKitFilter($value, 'услуги салона:',
            [
                'маникюр' => [
                    'filters' => [
                        ['id' => '4263', 'value' => 'маникюр'],
                        ['id' => '4733', 'value' => 'маникюр женский'],
                        ['id' => '4893', 'value' => 'маникюр, педикюр'],
                    ],
                    'rubrics' => [
                        ['id' => '10328713'],
                    ],
                ],
                'педикюр' => [
                    'filters' => [
                        ['id' => '4263', 'value' => 'педикюр'],
                        ['id' => '4733', 'value' => 'педикюр женский'],
                        ['id' => '4893', 'value' => 'маникюр, педикюр'],
                    ],
                    'rubrics' => [
                        ['id' => '10328713'],
                    ],
                ],
                'массаж' => [
                    'filters' => [
                        ['id' => '4263', 'value' => 'массаж'],
                        ['id' => '4383', 'value' => 'массаж'],
                        ['id' => '4543', 'value' => 'массаж'],
                    ],
                    'rubrics' => [
                        ['id' => '10328703'],
                    ],
                ],
                'наращивание волос' => [
                    'filters' => [
                        ['id' => '4793', 'value' => 'наращивание'],
                    ],
                ],
                'наращивание ногтей' => [
                    'filters' => [
                        ['id' => '4733', 'value' => 'наращивание, коррекция ногтей'],
                        ['id' => '4473', 'value' => 'наращивание, коррекция ногтей'],
                        ['id' => '4483', 'value' => 'наращивание, коррекция ногтей'],
                        ['id' => '4263', 'value' => 'наращивание, коррекция ногтей'],
                        ['id' => '4733', 'value' => 'наращивание, коррекция ногтей'],
                    ],
                ],
                'наращивание ресниц' => [
                    'filters' => [
                        ['id' => '4293', 'value' => 'наращивание ресниц'],
                    ],
                ],
                'лечение волос и кожи головы' => [
                    'filters' => [
                        ['id' => '4793', 'value' => 'уход, лечение, диагностика'],
                    ],
                ],
                'солярий' => [
                    'filters' => [
                        ['id' => '4983', 'value' => 'солярии'],
                    ],
                    'rubrics' => [
                        ['id' => '10328753'],
                        ['id' => '10328673'],
                    ],
                ],
                'детские стрижки' => [
                    'filters' => [
                        ['id' => '4793', 'value' => 'стрижка детская'],
                    ],
                ],
                'женские стрижки' => [
                    'filters' => [
                        ['id' => '4793', 'value' => 'стрижка женская'],
                    ],
                ],
                'мужские стрижки' => [
                    'filters' => [
                        ['id' => '4793', 'value' => 'стрижка мужская'],
                    ],
                ],
                'косметология' => [
                    'filters' => [
                        ['id' => '4263', 'value' => 'косметология'],
                    ],
                    'rubrics' => [
                        ['id' => '10328663'],
                    ],
                ],
                'макияж' => [
                    'filters' => [
                        ['id' => '4893', 'value' => 'макияж'],
                    ],
                ],
                'пилинг' => [
                    'filters' => [
                        ['id' => '4383', 'value' => 'пилинг, скрабирование'],
                    ],
                ],
                'тату и пирсинг' => [
                    'filters' => [
                        ['id' => '4983', 'value' => 'салоны татуировки и пирсинга'],
                    ],
                    'rubrics' => [
                        ['id' => '10328693'],
                    ],
                ],
                'уход за лицом' => [
                    'filters' => [
                        ['id' => '4423', 'value' => 'лицо'],
                    ],
                ],
                'уход за телом' => [
                    'filters' => [
                        ['id' => '4423', 'value' => 'тело'],
                    ],
                ],
            ]
        );
        $this->setFilter('4263', $value, 'педикюр', 'педикюр', false, 10328713);
        $this->setEasyFilter('4733', $value, 'педикюр', 'педикюр женский');
        $this->setEasyFilter('4893', $value, 'педикюр', 'маникюр, педикюр');
        $this->setFilter('4983', $value, 'парикмахерская', 'парикмахерские', false, 10328733);
        $this->setKitFilter($value, 'для ногтей:',
            [
                'женский маникюр' => [
                    'filters' => [
                        ['id' => '4733', 'value' => 'маникюр женский'],
                    ],
                ],
                'женский педикюр' => [
                    'filters' => [
                        ['id' => '4733', 'value' => 'педикюр женский'],
                    ],
                ],
                'мужской маникюр' => [
                    'filters' => [
                        ['id' => '4733', 'value' => 'маникюр мужской'],
                    ],
                ],
                'мужской педикюр' => [
                    'filters' => [
                        ['id' => '4733', 'value' => 'педикюр мужской'],
                    ],
                ],
                'детский маникюр' => [
                    'filters' => [
                        ['id' => '4733', 'value' => 'маникюр детский'],
                    ],
                ],
                'наращивание ногтей' => [
                    'filters' => [
                        ['id' => '4733', 'value' => 'наращивание, коррекция ногтей'],
                        ['id' => '4473', 'value' => 'наращивание, коррекция ногтей'],
                        ['id' => '4483', 'value' => 'наращивание, коррекция ногтей'],
                        ['id' => '4263', 'value' => 'наращивание, коррекция ногтей'],
                        ['id' => '4733', 'value' => 'наращивание, коррекция ногтей'],
                    ],
                ],
                'дизайн ногтей' => [
                    'filters' => [
                        ['id' => '4733', 'value' => 'дизайн'],
                    ],
                ],
                'артдизайн' => [
                    'filters' => [
                        ['id' => '4733', 'value' => 'дизайн'],
                    ],
                ],
                'парафинотерапия' => [
                    'filters' => [
                        ['id' => '4473', 'value' => 'парафинотерапия'],
                        ['id' => '4483', 'value' => 'парафинотерапия'],
                        ['id' => '4543', 'value' => 'парафинотерапия'],
                    ],
                ],
                'горячий маникюр' => [
                    'filters' => [
                        ['id' => '4473', 'value' => 'горячий'],
                    ],
                ],
                'spa-маникюр' => [
                    'filters' => [
                        ['id' => '4473', 'value' => 'SPA'],
                    ],
                ],
                'перманентный маникюр' => [
                    'filters' => [
                        ['id' => '4743', 'value' => 'перманентное'],
                    ],
                ],
            ]
        );
        $this->setFilter('4263', $value, 'маникюр', 'маникюр', false, 10328713);
        $this->setEasyFilter('4733', $value, 'маникюр', 'маникюр женский');
        $this->setEasyFilter('4893', $value, 'маникюр', 'маникюр, педикюр');
        $this->setKitFilter($value, 'эпиляция:',
            [
                'восковая эпиляция' => [
                    'filters' => [
                        ['id' => '4353', 'value' => 'депиляция воском'],
                    ],
                ],
                'лазерная эпиляция' => [
                    'filters' => [
                        ['id' => '4353', 'value' => 'лазерная эпиляция'],
                    ],
                ],
                'сахарная эпиляция' => [
                    'filters' => [
                        ['id' => '4353', 'value' => 'шугаринг'],
                    ],
                ],
                'фотоэпиляция' => [
                    'filters' => [
                        ['id' => '4353', 'value' => 'фотоэпиляция'],
                    ],
                ],
                'электроэпиляция' => [
                    'filters' => [
                        ['id' => '4353', 'value' => 'электроэпиляция'],
                    ],
                ],
                'энзимная эпиляция' => [
                    'filters' => [
                        ['id' => '4353', 'value' => 'энзимная эпиляция'],
                    ],
                ],
            ]);
        $this->setFilter('4263', $value, 'солярий', 'солярий', false, 10328753);
        $this->setEasyFilter('4263', $value, 'солярий', 'солярий');
        $this->setKitFilter($value, 'парикмахерские услуги:',
            [
                'мелирование' => [
                    'filters' => [
                        ['id' => '4823', 'value' => 'мелирование'],
                    ],
                ],
                'окрашивание' => [
                    'filters' => [
                        ['id' => '4793', 'value' => 'окрашивание женское'],
                        ['id' => '4793', 'value' => 'окрашивание мужское'],
                        ['id' => '4823', 'value' => 'окраска'],
                    ],
                ],
                'тонирование' => [
                    'filters' => [
                        ['id' => '4823', 'value' => 'тонирование'],
                    ],
                ],
                'выпрямление волос' => [
                    'filters' => [
                        ['id' => '4793', 'value' => 'выпрямление'],
                    ],
                ],
                'химическая завивка' => [
                    'filters' => [
                        ['id' => '4813', 'value' => 'химическая завивка'],
                    ],
                ],
                'для детей' => [
                    'filters' => [
                        ['id' => '4793', 'value' => 'стрижка детская'],
                    ],
                ],
                'вечерняя/свадебная укладка' => [
                    'filters' => [
                        ['id' => '4803', 'value' => 'вечерняя'],
                        ['id' => '4803', 'value' => 'свадебная'],
                    ],
                ],
                'укладка' => [
                    'filters' => [
                        ['id' => '4793', 'value' => 'укладка'],
                    ],
                ],
                'стрижка' => [
                    'filters' => [
                        ['id' => '4793', 'value' => 'стрижка женская'],
                    ],
                ],
                'наращивание волос' => [
                    'filters' => [
                        ['id' => '4793', 'value' => 'наращивание'],
                    ],
                ],
                'лечение волос' => [
                    'filters' => [
                        ['id' => '4793', 'value' => 'уход, лечение, диагностика'],
                    ],
                ],
                'стрижка горячими ножницами' => [
                    'filters' => [
                        ['id' => '4793', 'value' => 'термострижка'],
                    ],
                ],
                'колорирование' => [
                    'filters' => [
                        ['id' => '4823', 'value' => 'колорирование'],
                    ],
                ],
                'французские косички' => [
                    'filters' => [
                        ['id' => '4803', 'value' => 'плетение косичек, дреды'],
                    ],
                ],
                'африканские косички' => [
                    'filters' => [
                        ['id' => '4803', 'value' => 'плетение косичек, дреды'],
                    ],
                ],
                'элюминирование' => [
                    'filters' => [
                        ['id' => '4843', 'value' => 'элюминирование'],
                    ],
                ],
                'биоламинирование' => [
                    'filters' => [
                        ['id' => '4843', 'value' => 'биоламинирование'],
                    ],
                ],
                'диагностика состояния волос' => [
                    'filters' => [
                        ['id' => '4793', 'value' => 'уход, лечение, диагностика'],
                    ],
                ],
                'дреды' => [
                    'filters' => [
                        ['id' => '4803', 'value' => 'плетение косичек, дреды'],
                    ],
                ],
                'fire cut' => [
                    'filters' => [
                        ['id' => '4793', 'value' => 'термострижка'],
                    ],
                ],
                'био-выпрямление волос' => [
                    'filters' => [
                        ['id' => '4853', 'value' => 'биовыравнивание'],
                    ],
                ],
                'био-завивка волос' => [
                    'filters' => [
                        ['id' => '4813', 'value' => 'биозавивка'],
                    ],
                ],
                'глазирование' => [
                    'filters' => [
                        ['id' => '4843', 'value' => 'глазирование'],
                    ],
                ],
                'декапирование' => [
                    'filters' => [
                        ['id' => '4823', 'value' => 'декалирование (смывка)'],
                    ],
                ],
                'химическое распрямление' => [
                    'filters' => [
                        ['id' => '4853', 'value' => 'химическое'],
                    ],
                ],
                'экранирование' => [
                    'filters' => [
                        ['id' => '4873', 'value' => 'экранирование (shining)'],
                    ],
                ],
                'брондирование' => [
                    'filters' => [
                        ['id' => '4863', 'value' => 'брондирование'],
                    ],
                ],
            ]);
        $this->setFilter('4983', $value, 'косметология', 'салоны красоты', false, 10328643);
        $this->setEasyFilter('4263', $value, 'косметология', 'косметология');
        $this->setKitFilter($value, 'для фигуры:',
            [
                'обертывание' => [
                    'filters' => [
                        ['id' => '4383', 'value' => 'обертывания'],
                    ],
                ],
                'пилинг' => [
                    'filters' => [
                        ['id' => '4583', 'value' => 'косметические процедуры'],
                    ],
                ],
                'лифтинг' => [
                    'filters' => [
                        ['id' => '4583', 'value' => 'термолифтинг тела'],
                    ],
                ],
                'миостимуляция' => [
                    'filters' => [
                        ['id' => '4583', 'value' => 'аппаратный массаж'],
                    ],
                ],
                'мезотерапия' => [
                    'filters' => [
                        ['id' => '4583', 'value' => 'косметические процедуры'],
                    ],
                ],
                'элос-технология' => [
                    'filters' => [
                        ['id' => '4583', 'value' => 'косметические процедуры'],
                    ],
                ],
                'вакуумное воздействие' => [
                    'filters' => [
                        ['id' => '4583', 'value' => 'аппаратный массаж'],
                    ],
                ],
                'ударно-волновая терапия' => [
                    'filters' => [
                        ['id' => '4583', 'value' => 'аппаратный массаж'],
                    ],
                ],
                'ультразвуковое воздействие' => [
                    'filters' => [
                        ['id' => '4583', 'value' => 'аппаратный массаж'],
                    ],
                ],
                'гидромассаж' => [
                    'filters' => [
                        ['id' => '4583', 'value' => 'электромиостимуляция мышц'],
                        ['id' => '4693', 'value' => 'гидромассаж'],
                    ],
                ],
            ]);
        $this->setFilter('4983', $value, 'spa', 'SPA-салоны', false, 10328653);
        $this->setFilter('4383', $value, 'флоатинг', 'флоатинг');
        $this->setFilter('4983', $value, 'тату и пирсинг', 'салоны татуировки и пирсинга', false, 10328693);
        $this->setKitFilter($value, 'виды тату:',
            [
                'татуаж' => [
                    'filters' => [
                        ['id' => '4613', 'value' => 'перманентный макияж'],
                    ],
                ],
                'татуировки' => [
                    'filters' => [
                        ['id' => '4613', 'value' => 'татуировка'],
                    ],
                ],
                'коррекция тату' => [
                    'filters' => [
                        ['id' => '4613', 'value' => 'коррекция'],
                    ],
                ],
                'выведение тату' => [
                    'filters' => [
                        ['id' => '4613', 'value' => 'удаление'],
                    ],
                ],
            ]);
        $this->setKitFilter($value, 'массаж:',
            [
                'LPG ролико-вакуумный массаж' => [
                    'filters' => [
                        ['id' => '4693', 'value' => 'LPG-массаж'],
                    ],
                ],
                'relax-массаж' => [
                    'filters' => [
                        ['id' => '4643', 'value' => 'расслабляющий'],
                    ],
                ],
                'антицеллюлитный массаж' => [
                    'filters' => [
                        ['id' => '4273', 'value' => 'антицеллюлитные программы'],
                    ],
                ],
                'гидромассаж' => [
                    'filters' => [
                        ['id' => '4693', 'value' => 'гидромассаж'],
                    ],
                ],
                'испанский хиромассаж' => [
                    'filters' => [
                        ['id' => '4443', 'value' => 'хиромассаж'],
                        ['id' => '4723', 'value' => 'испанский'],
                    ],

                ],
                'китайский оздоровительный массаж' => [
                    'filters' => [
                        ['id' => '4443', 'value' => 'китайский'],
                    ],
                ],
                'лимфодренажный массаж' => [
                    'filters' => [
                        ['id' => '4683', 'value' => 'лимфодренажный '],
                    ],
                ],
                'массаж воротниковой зоны' => [
                    'filters' => [
                        ['id' => '4663', 'value' => 'шейно-воротниковая зона'],
                        ['id' => '4663', 'value' => 'общий'],
                    ],
                ],
                'общий медицинский массаж' => [
                    'filters' => [
                        ['id' => '4643', 'value' => 'медицинский, лечебный'],
                        ['id' => '4663', 'value' => 'общий'],
                    ],
                ],
                'общий спортивный массаж' => [
                    'filters' => [
                        ['id' => '4663', 'value' => 'общий'],
                        ['id' => '4643', 'value' => 'спортивный'],
                    ],
                ],
                'стоун-терапия' => [
                    'filters' => [
                        ['id' => '4443', 'value' => 'стоунтерапия'],
                    ],
                ],
                'тайский массаж' => [
                    'filters' => [
                        ['id' => '4443', 'value' => 'тайский'],
                    ],
                ],
                'точечный японский массаж' => [
                    'filters' => [
                        ['id' => '4683', 'value' => 'точечный '],
                        ['id' => '4683', 'value' => 'японский, асахи'],
                    ],
                ],
            ]);
        $this->setKitFilter($value, 'виды солярия:',
            [
                'вертикальный' => [
                    'filters' => [
                        ['id' => '4593', 'value' => 'вертикальный'],
                    ],
                ],
                'горизонтальный' => [
                    'filters' => [
                        ['id' => '4593', 'value' => 'горизонтальный'],
                    ],
                ],
                'турбосолярий' => [
                    'filters' => [
                        ['id' => '4593', 'value' => 'турбосолярий'],
                    ],
                ],
                'загар без солнца' => [
                    'filters' => [
                        ['id' => '4593', 'value' => 'моментальный загар'],
                    ],
                ],
                'кресло-солярий' => [
                    'filters' => [
                        ['id' => '4593', 'value' => 'кресло-солярий'],
                    ],
                ],
            ]);
        $this->setKitFilter($value, 'косметологические услуги:',
            [
                'коррекция бровей' => [
                    'filters' => [
                        ['id' => '4293', 'value' => 'коррекция бровей'],
                    ],
                ],
                'окрашивание бровей/ресниц' => [
                    'filters' => [
                        ['id' => '4293', 'value' => 'окраска бровей, ресниц'],
                    ],
                ],
                'наращивание ресниц' => [
                    'filters' => [
                        ['id' => '4293', 'value' => 'наращивание ресниц'],
                    ],
                ],
                'очищение кожи' => [
                    'filters' => [
                        ['id' => '4513', 'value' => 'механическая'],
                    ],
                ],
                'аппаратная косметология лица' => [
                    'filters' => [
                        ['id' => '4493', 'value' => 'аппаратная'],
                    ],
                ],
                'удаление новообразований' => [
                    'filters' => [
                        ['id' => '4553', 'value' => 'лечение гемангиом'],
                    ],
                ],
                'перманентный макияж' => [
                    'filters' => [
                        ['id' => '4893', 'value' => 'макияж перманентный'],
                        ['id' => '4613', 'value' => 'перманентный макияж'],
                    ],
                ],
                'лифтинг' => [
                    'filters' => [
                        ['id' => '4563', 'value' => 'лифтинг (подтяжка) лица'],
                    ],
                ],
                'фотоомоложение' => [
                    'filters' => [
                        ['id' => '4553', 'value' => 'фотоомоложение'],
                    ],
                ],
                'лечение акне' => [
                    'filters' => [
                        ['id' => '4553', 'value' => 'лечение акне (угревой сыпи)'],
                    ],
                ],
                'удаление рубцов' => [
                    'filters' => [
                        ['id' => '4553', 'value' => 'удаление рубцов, шрамов'],
                    ],
                ],
                'удаление сосудистых дефектов' => [
                    'filters' => [
                        ['id' => '4553', 'value' => 'лечение сосудистых звездочек'],
                    ],
                ],
                'мезотерапия' => [
                    'filters' => [
                        ['id' => '4573', 'value' => 'мезотерапия волос'],
                        ['id' => '4573', 'value' => 'мезотерапия лица'],
                        ['id' => '4563', 'value' => 'безинъекционная мезотерапия'],
                    ],
                ],
                'anti-age процедуры' => [
                    'filters' => [
                        ['id' => '4273', 'value' => 'антивозрастные программы'],
                    ],
                ],
                'LPG' => [
                    'filters' => [
                        ['id' => '4693', 'value' => 'LPG-массаж'],
                    ],
                ],
                'аква-грим' => [
                    'filters' => [
                        ['id' => '4343', 'value' => 'боди-арт'],
                    ],
                ],
                'бальнеотерапия' => [
                    'filters' => [
                        ['id' => '4383', 'value' => 'ванны (бальнеотерапия)'],
                    ],
                ],
                'безинъекционная мезотерапия' => [
                    'filters' => [
                        ['id' => '4563', 'value' => 'безинъекционная мезотерапия'],
                    ],
                ],
                'биоревитализация' => [
                    'filters' => [
                        ['id' => '4573', 'value' => 'биоревитализация'],
                        ['id' => '4563', 'value' => 'биоревитализация'],
                    ],
                ],
                'визаж' => [
                    'filters' => [
                        ['id' => '4893', 'value' => 'макияж'],
                    ],
                ],
                'контурная пластика' => [
                    'filters' => [
                        ['id' => '4573', 'value' => 'пластика лица'],
                    ],
                ],
                'миостимуляция' => [
                    'filters' => [
                        ['id' => '4563', 'value' => 'электромиостимуляция'],
                        ['id' => '4583', 'value' => 'электромиостимуляция мышц'],
                    ],
                ],
                'инъекционная липосакция' => [
                    'filters' => [
                        ['id' => '4583', 'value' => 'липосация без операции'],
                    ],
                ],
                'плазмалифтинг' => [
                    'filters' => [
                        ['id' => '4573', 'value' => 'плазмолифтинг (аутологичное клеточное омоложение)'],
                    ],
                ],
                'диспорт' => [
                    'filters' => [
                        ['id' => '4573', 'value' => 'ботулинотерапия (Диспорт)'],
                    ],
                ],
                'коллаген' => [
                    'filters' => [
                        ['id' => '4563', 'value' => 'коллагенарий'],
                    ],
                ],
                'ультразвуковая липосакция' => [
                    'filters' => [
                        ['id' => '4583', 'value' => 'липосация без операции'],
                    ],
                ],
                'лазерная липосакция' => [
                    'filters' => [
                        ['id' => '4583', 'value' => 'липосация без операции'],
                    ],
                ],
                'вакуумный массажер' => [
                    'filters' => [
                        ['id' => '4693', 'value' => 'вакуумный'],
                    ],
                ],
                'безоперационный лифтинг кожи' => [
                    'filters' => [
                        ['id' => '4573', 'value' => 'плазмолифтинг (аутологичное клеточное омоложение)'],
                    ],
                ],
            ]);
        $this->setEasyFilter('4083', $value, 'подарочный сертификат', 'подарочные сертификаты');
        $this->setKitFilter($value, 'ценовая категория:',
            [
                'economy' => [
                    'filters' => [
                        ['id' => '4963', 'value' => 'эконом'],
                    ],
                ],
                'premium' => [
                    'filters' => [
                        ['id' => '4963', 'value' => 'премиум'],
                    ],
                ],
                'standart' => [
                    'filters' => [
                        ['id' => '4963', 'value' => 'стандарт'],
                    ],
                ],
            ]);
        $this->setFilter('4093', $value, 'бесплатный wi-fi', 'Есть', true, null, 'Нет');
        $this->setFilter('4113', $value, 'гостевая парковка', 'Есть', true, null, 'Нет');
        $this->setFilter('4123', $value, 'оплата картой', 'Есть', true, null, 'Нет');
        $this->setKitFilter($value, 'пирсинг:',
            [
                'ушей' => [
                    'filters' => [
                        ['id' => '4333', 'value' => 'уши'],
                    ],
                ],
                'лица' => [
                    'filters' => [
                        ['id' => '4333', 'value' => 'брови'],
                        ['id' => '4333', 'value' => 'нос'],
                    ],
                ],
                'языка' => [
                    'filters' => [
                        ['id' => '4333', 'value' => 'язык'],
                    ],
                ],
                'пупка' => [
                    'filters' => [
                        ['id' => '4333', 'value' => 'пупок'],
                    ],
                ],
                'груди' => [
                    'filters' => [
                        ['id' => '4333', 'value' => 'интим'],
                    ],
                ],
                'интимный' => [
                    'filters' => [
                        ['id' => '4333', 'value' => 'интим'],
                    ],
                ],
            ]);
        $this->setKitFilter($value, 'spa:',
            [
                'ароматерапия' => [
                    'filters' => [
                        ['id' => '4273', 'value' => 'арома-SPA'],
                        ['id' => '4273', 'value' => 'аромамассаж'],
                    ],
                ],
                'spa-маникюр' => [
                    'filters' => [
                        ['id' => '4473', 'value' => 'SPA'],
                    ],
                ],
                'spa-программы' => [
                    'filters' => [
                        ['id' => '4393', 'value' => 'индивидуальные'],
                    ],
                ],
                'spa-капсула' => [
                    'filters' => [
                        ['id' => '4383', 'value' => 'SPA-капсула'],
                    ],
                ],
                'spa-педикюр' => [
                    'filters' => [
                        ['id' => '4483', 'value' => 'SPA'],
                    ],
                ],
            ]);
        $this->setEasyFilter('4403', $value, 'сауна', 'бассейн');
        $this->setEasyFilter('4453', $value, 'сауна', 'финская сауна');
        $this->setEasyFilter('4453', $value, 'инфракрасная кабина', 'инфракрасная сауна');
        $this->setEasyFilter('4693', $value, 'гидромассажный бассейн', 'гидромассаж');
        $this->setKitFilter($value, 'парная:',
            [
                'русская' => [
                    'filters' => [
                        ['id' => '4453', 'value' => 'русская (паровая) баня'],
                    ],
                ],
                'турецкая' => [
                    'filters' => [
                        ['id' => '4453', 'value' => 'турецкая баня (хамам)'],
                    ],
                ],
                'на дровах' => [
                    'filters' => [
                        ['id' => '4453', 'value' => 'баня на дровах'],
                    ],
                ],
                'японская' => [
                    'filters' => [
                        ['id' => '4453', 'value' => 'японская баня'],
                    ],
                ],
                'финская' => [
                    'filters' => [
                        ['id' => '4453', 'value' => 'финская сауна'],
                    ],
                ],
                'инфракрасная' => [
                    'filters' => [
                        ['id' => '4453', 'value' => 'инфракрасная сауна'],
                    ],
                ],

            ]);
        $this->setEasyFilter('4613', $value, 'шрамирование', 'перманентный макияж');
        $this->setKitFilter($value, 'обучение:',
            [
                'татуажу' => [
                    'filters' => [
                        ['id' => '4373', 'value' => 'профессиональное обучение'],
                    ],
                ],
                'пирсингу' => [
                    'filters' => [
                        ['id' => '4373', 'value' => 'профессиональное обучение'],
                    ],
                ],
                'татуировке' => [
                    'filters' => [
                        ['id' => '4373', 'value' => 'профессиональное обучение'],
                    ],
                ],
            ]);
        $this->setKitFilter($value, 'виды косметологии:',
            [
                'аппаратная косметология' => [
                    'filters' => [
                        ['id' => '4493', 'value' => 'аппаратная'],
                    ],
                ],
                'лазерная косметология' => [
                    'filters' => [
                        ['id' => '4493', 'value' => 'лазерная'],
                    ],
                ],
                'эстетическая косметология' => [
                    'filters' => [
                        ['id' => '4493', 'value' => 'эстетическая'],
                    ],
                ],
            ]);
        $this->setEasyFilter('4273', $value, 'услуги коррекции фигуры', 'для похудения, коррекция фигуры');
        $this->setEasyFilter('4263', $value, 'услуги эпиляции', 'эпиляция, депиляция');
        $this->setEasyFilter('4893', $value, 'услуги эпиляции', 'эпиляция, депиляция');
        $this->setKitFilter($value, 'макияж:',
            [
                'свадебный' => [
                    'filters' => [
                        ['id' => '4923', 'value' => 'свадебный'],
                    ],
                ],
                'вечерний' => [
                    'filters' => [
                        ['id' => '4923', 'value' => 'вечерний'],
                    ],
                ],
                'естественный' => [
                    'filters' => [
                        ['id' => '4923', 'value' => 'дневной'],
                    ],
                ],
                'для фотосессий' => [
                    'filters' => [
                        ['id' => '4923', 'value' => 'для фото-, видеосъемки'],
                    ],
                ],
                'деловой' => [
                    'filters' => [
                        ['id' => '4923', 'value' => 'дневной'],
                    ],
                ],
                'для показов' => [
                    'filters' => [
                        ['id' => '4923', 'value' => 'театральный, карнавальный, подиумный'],
                    ],
                ],
                'стилизация' => [
                    'filters' => [
                        ['id' => '4923', 'value' => 'театральный, карнавальный, подиумный'],
                    ],
                ],
            ]);
        $this->setEasyFilter('4793', $value, 'стайлинг', 'укладка');
        $this->setFilter('4913', $value, 'шоппинг-сопровождение', 'Есть', true, null, 'Нет');
        $this->setKitFilter($value, 'услуги:',
            [
                'выезд мастера' => [
                    'filters' => [
                        ['id' => '4373', 'value' => 'выезд мастера на дом'],
                    ],
                ],
                'индивидуальное обучение' => [
                    'filters' => [
                        ['id' => '4373', 'value' => 'профессиональное обучение'],
                    ],
                ],
                'мастер-классы' => [
                    'filters' => [
                        ['id' => '4373', 'value' => 'профессиональное обучение'],
                    ],
                ],
                'подготовка стилистов' => [
                    'filters' => [
                        ['id' => '4373', 'value' => 'профессиональное обучение'],
                    ],
                ],
            ]);
        $this->setFilter('4903', $value, 'мужской стиль', 'Есть', true, null, 'Нет');
        $this->setEasyFilter('4373', $value, 'выезд на дом', 'выезд мастера на дом');
        $this->setEasyFilter('4373', $value, 'мастер-классы', 'профессиональное обучение');
        $this->setEasyFilter('4373', $value, 'фотосессия', 'фотосъемка');

        return true;
    }

    /**
     * @param $value
     * @return bool
     */
    protected function setFiltersAndRubricsSome($value)
    {
        $this->setKitFilter($value, 'кухня:', $this->kitchen);

        if (is_int(stripos($value, 'тип заведения:')))
        {
            $type = str_ireplace('тип заведения: ', '', $value);
            $type = explode(", ", $type);
            foreach ($type as $placeType)
            {
                if (is_int(stripos($placeType, 'банкетный зал')))
                {
                    $this->setEasyRubric(10328633);
                }

                $this->setEasyFilterRubric(4953, $placeType, 'бар', 'бары, пабы', 10328573);
                $this->setEasyFilter(4033, $placeType, 'винотека', 'винное');
                $this->setEasyFilter(4173, $placeType, 'гриль-бар', 'гриль-бар');
                $this->setEasyFilter(4133, $placeType, 'детское кафе', 'детское (семейное) кафе');
                $this->setEasyFilter(4133, $placeType, 'закусочная', 'закусочные');
                $this->setEasyFilterRubric(4953, $placeType, 'караоке-бар', 'караоке-клубы', 10328623);
                $this->setEasyFilterRubric(5243, $placeType, 'караоке-бар', 'караоке-клубы', 10328623);
                $this->setEasyFilterRubric(4953, $placeType, 'кафе', 'Кафе', 10328533);
                $this->setEasyFilter(4133, $placeType, 'кафетерий', 'классическое кафе');
                $this->setEasyFilterRubric(4953, $placeType, 'кейтеринг', 'кейтеринг', 10328603);
                $this->setEasyFilter(4183, $placeType, 'кондитерская', 'кондитерская');
                $this->setEasyFilter(4183, $placeType, 'кофейня', 'кофейня');
                $this->setEasyFilterRubric(4953, $placeType, 'паб', 'Бары, пабы', 10328573);
                $this->setEasyFilter(4033, $placeType, 'пивной ресторан', 'пивное');
                $this->setEasyFilterRubric(4953, $placeType, 'пиццерия', 'Пиццерии', 10328553);
                $this->setEasyFilterRubric(4953, $placeType, 'ресторан', 'Рестораны', 10328523);
                $this->setEasyFilter(4063, $placeType, 'ресторан на воде', 'на воде');
                $this->setEasyFilter(4033, $placeType, 'рыбный ресторан', 'рыбное');
                $this->setEasyFilter(4173, $placeType, 'спорт-бар', 'спорт-бар');
                $this->setEasyFilter(4033, $placeType, 'стейк-хаус', 'стейк-хаус');
                $this->setEasyFilter(4033, $placeType, 'суши-бар', 'суши');
                $this->setEasyFilterRubric(4953, $placeType, 'фаст-фуд', 'Рестораны быстрого питания', 10328563);
                $this->setEasyFilter(4183, $placeType, 'чайный клуб', 'чайная');
                $this->setEasyFilter(4133, $placeType, 'фудкорт', 'фудкорт');
            }
        }

        $this->setFilter('4093', $value, 'бесплатный wi-fi', "Есть", true, null, "Нет");
        $this->setFilter('3983', $value, 'доставка еды', 'доставка', false, 10328593);
        $this->setFilter('4073', $value, 'летняя веранда', 'летняя терраса', false);
        $this->setFilter('4123', $value, 'оплата картой', "Есть", true, null, "Нет");
        $this->setFilter('4013', $value, 'заведение для некурящих', 'зал для некурящих', true, null, 'зал для курящих');
        $this->setFilter('4003', $value, 'караоке', 'караоке');
        $this->setFilter('3983', $value, 'детская комната', 'детская комната');
        $this->setFilter('4113', $value, 'парковка', "Есть", true, null, "Нет");
        $this->setFilter('4173', $value, 'спортивные трансляции', 'спорт-бар', false);
        $this->setFilter('4163', $value, 'завтрак', 'завтрак');
        $this->setFilter('4153', $value, 'игры', 'настольные игры');
        $this->setFilter('33743', $value, 'проектор', 'да', true);

        return true;
    }

    protected function getPlaceMetro($x, $y)
    {
        $url = self::YANDEX_METRO_GEOCODE . $x . ',' . $y;
        // Отправляем запрос на геокодер
        $response = Yii::app()->curl->get($url);

        $response = CJSON::decode($response);

        $metro = $response['response']['GeoObjectCollection']['featureMember'][0]['GeoObject']['name'];
        if (is_int(stripos($metro, 'метро')))
        {

            $metro = str_ireplace("метро", '', $metro);
            $metro = preg_replace("!станция!uis", '', $metro);
            $metro = str_ireplace("станция", '', $metro);
            $metro = trim($metro);

            $this->filtersCustom[count($this->filtersCustom)] = [
                'id' => '3843',
                'value' => $metro,
            ];
        }
        else
        {
            $metro = null;
        }

        $url = self::YANDEX_OKRUG_GEOCODE . $x . ',' . $y;
        // Отправляем запрос на геокодер
        $response = Yii::app()->curl->get($url);

        $response = CJSON::decode($response);

        $raion = ['Академический (ЮЗАО)',
            'Алексеевский (СВАО)',
            'Алтуфьевский (СВАО)',
            'Арбат (ЦАО)',
            'Аэропорт (САО)',
            'Бабушкинский (СВАО)',
            'Басманный (ЦАО)',
            'Беговой (САО)',
            'Бескудниковский (САО)',
            'Бибирево (СВАО)',
            'Бирюлево Восточное (ЮАО)',
            'Бирюлево Западное (ЮАО)',
            'Богородское (ВАО)',
            'Братеево (ЮАО)',
            'Бутырский (СВАО)',
            'Вешняки (ВАО)',
            'Внуково (ЗАО)',
            'Внуковское (НАО)',
            'Войковский (САО)',
            'Вороновское (ТАО)',
            'Воскресенское (НАО)',
            'Восточное Дегунино (САО)',
            'Восточное Измайлово (ВАО)',
            'Восточный (ВАО)',
            'Выхино-Жулебино (ЮВАО)',
            'Гагаринский (ЮЗАО)',
            'Головинский (САО)',
            'Гольяново (ВАО)',
            'Даниловский (ЮАО)',
            'Десёновское (НАО)',
            'Дмитровский (САО)',
            'Донской (ЮАО)',
            'Дорогомилово (ЗАО)',
            'Замоскворечье (ЦАО)',
            'Западное Дегунино (САО)',
            'Зюзино (ЮЗАО)',
            'Зябликово (ЮАО)',
            'Ивановское (ВАО)',
            'Измайлово (ВАО)',
            'Капотня (ЮВАО)',
            'Киевский (ТАО)',
            'Кленовское (ТАО)',
            'Кокошкино (НАО)',
            'Коньково (ЮЗАО)',
            'Коптево (САО)',
            'Косино-Ухтомский (ВАО)',
            'Котловка (ЮЗАО)',
            'Краснопахорское (ТАО)',
            'Красносельский (ЦАО)',
            'Крылатское (ЗАО)',
            'Крюково (ЗАО)',
            'Кузьминки (ЮВАО)',
            'Кунцево (ЗАО)',
            'Куркино (СЗАО)',
            'Левобережный (САО)',
            'Лефортово (ЮВАО)',
            'Лианозово (СВАО)',
            'Ломоносовский (ЮЗАО)',
            'Лосиноостровский (СВАО)',
            'Люблино (ЮВАО)',
            'Марушкинское (НАО)',
            'Марфино (СВАО)',
            'Марьина Роща (СВАО)',
            'Марьино (ЮВАО)',
            'Матушкино (ЗАО)',
            'Метрогородок (ВАО)',
            'Мещанский (ЦАО)',
            'Митино (СЗАО)',
            'Михайлово-Ярцевское (ТАО)',
            'Можайский (ЗАО)',
            'Молжаниновский (САО)',
            'Москворечье-Сабурово (ЮАО)',
            'Московский (НАО)',
            'Мосрентген (НАО)',
            'Нагатино-Садовники (ЮАО)',
            'Нагатинский Затон (ЮАО)',
            'Нагорный (ЮАО)',
            'Некрасовка (ЮВАО)',
            'Нижегородский (ЮВАО)',
            'Новогиреево (ВАО)',
            'Новокосино (ВАО)',
            'Ново-Переделкино (ЗАО)',
            'Новофедоровское (ТАО)',
            'Обручевский (ЮЗАО)',
            'Орехово-Борисово Северное (ЮАО)',
            'Орехово-Борисово Южное (ЮАО)',
            'Останкинский (СВАО)',
            'Отрадное (СВАО)',
            'Очаково-Матвеевское (ЗАО)',
            'Первомайское (ТАО)',
            'Перово (ВАО)',
            'Печатники (ЮВАО)',
            'Покровское-Стрешнево (СЗАО)',
            'Преображенское (ВАО)',
            'Пресненский (ЦАО)',
            'Проспект Вернадского (ЗАО)',
            'Раменки (ЗАО)',
            'Роговское (ТАО)',
            'Ростокино (СВАО)',
            'Рязановское (НАО)',
            'Рязанский (ЮВАО)',
            'Савёлки (ЗАО)',
            'Савеловский (САО)',
            'Свиблово (СВАО)',
            'Северное Бутово (ЮЗАО)',
            'Северное Измайлово (ВАО)',
            'Северное Медведково (СВАО)',
            'Северное Тушино (СЗАО)',
            'Северный (СВАО)',
            'Силино (ЗАО)',
            'Сокол (САО)',
            'Соколиная Гора (ВАО)',
            'Сокольники (ВАО)',
            'Солнцево (ЗАО)',
            'Сосенское (НАО)',
            'Старое Крюково (ЗАО)',
            'Строгино (СЗАО)',
            'Таганский (ЦАО)',
            'Тверской (ЦАО)',
            'Текстильщики (ЮВАО)',
            'Теплый Стан (ЮЗАО)',
            'Тимирязевский (САО)',
            'Троицк (ТАО)',
            'Тропарево-Никулино (ЗАО)',
            'Филевский Парк (ЗАО)',
            'Фили-Давыдково (ЗАО)',
            'Филимонковское (НАО)',
            'Хамовники (ЦАО)',
            'Ховрино (САО)',
            'Хорошево-Мневники (СЗАО)',
            'Хорошевский (САО)',
            'Царицыно (ЮАО)',
            'Черемушки (ЮЗАО)',
            'Чертаново Северное (ЮАО)',
            'Чертаново Центральное (ЮАО)',
            'Чертаново Южное (ЮАО)',
            'Щаповское (ТАО)',
            'Щербинка (НАО)',
            'Щукино (СЗАО)',
            'Южное Бутово (ЮЗАО)',
            'Южное Медведково (СВАО)',
            'Южное Тушино (СЗАО)',
            'Южнопортовый (ЮВАО)',
            'Якиманка (ЦАО)',
            'Ярославский (СВАО)',
            'Ясенево (ЮЗАО)',
        ];

        $okrugi = [
            'Восточный (ВАО)',
            'Западный (ЗАО)',
            'Зеленоградский (ЗАО)',
            'Новомосковский (НАО)',
            'Северный (САО)',
            'Северо-Восточный (СВАО)',
            'Северо-Западный (СЗАО)',
            'Троицкий (ТАО)',
            'Центральный (ЦАО)',
            'Юго-Восточный (ЮВАО)',
            'Юго-Западный (ЮЗАО)',
            'Южный (ЮАО)',
        ];

        $ocrug = $response['response']['GeoObjectCollection']['featureMember'][0]['GeoObject']['metaDataProperty']['GeocoderMetaData']['AddressDetails']['Country']['AdministrativeArea']['Locality']['DependentLocality']['DependentLocalityName'];


        $region = isset($response['response']['GeoObjectCollection']['featureMember'][0]['GeoObject']['metaDataProperty']['GeocoderMetaData']['AddressDetails']['Country']['AdministrativeArea']['Locality']['DependentLocality']['DependentLocality']['DependentLocalityName']) ? $response['response']['GeoObjectCollection']['featureMember'][0]['GeoObject']['metaDataProperty']['GeocoderMetaData']['AddressDetails']['Country']['AdministrativeArea']['Locality']['DependentLocality']['DependentLocality']['DependentLocalityName'] : $response['response']['GeoObjectCollection']['featureMember'][1]['GeoObject']['metaDataProperty']['GeocoderMetaData']['AddressDetails']['Country']['AdministrativeArea']['Locality']['DependentLocality']['DependentLocality']['DependentLocalityName'];
        $region = trim(str_ireplace('район', '', $region));

        foreach ($raion as $key => $value)
        {
            if (is_int(stripos($value, $region)))
            {
                $this->filtersCustom[count($this->filtersCustom)] = [
                    'id' => '3863',
                    'value' => $value,
                ];
                break;
            }
        }

        $ocrug = str_ireplace("административный округ", '', $ocrug);

        foreach ($okrugi as $key => $value)
        {
            if (is_int(stripos($value, $ocrug)))
            {
                $this->filtersCustom[count($this->filtersCustom)] = [
                    'id' => '3853',
                    'value' => $value,
                ];
                break;
            }
        }


        return $metro;
    }

    protected function destroyArray($array, $name)
    {

        $result = [];
        foreach ($array as $key => $value)
        {
            if ($name == 'placesRubrics')
            {
                $rub = $this->findName($value['rubric_id'], $this->rubricsArray);
                $result['placesRubrics'] .= $rub . "|";
            }
            else
            {
                if (is_array($value))
                {

                    $result = array_merge($result, $this->destroyArray($value, $name . '-' . $key));
                }
                else
                {
                    if ($key == 'id' && is_int(stripos($name, 'filter')))
                    {

                        $result[$name . '/' . $key . '|' . $this->findName($value, $this->filterArray)] = $value;
                    }
                    else
                    {
                        $result[$name . '/' . $key] = $value;
                    }
                }
            }
        }

        return $result;
    }


    protected function exportPlace($data)
    {
        $result = [];
        DebugHelper::printD($data);
        foreach ($data as $name => $array)
        {
            $result = array_merge($result, $this->destroyArray($array, $name));
        }

        $final[0] = $result['placeInfo/title'];
        $final[1] = $result['placeInfo/text_type'];
        $final[2] = $result['placesRubrics'];
        $final[2 + 1] = $result['address/address'];
        $final[4] = $result['address/building'];

        foreach ($result as $key => $item)
        {
            if ($key != 'placeInfo/title' && $key != 'placeInfo/text_type' && $key != 'placesRubrics' && $key != 'address/address' && $key != 'address/building')
            {
                $final[$key] = "[" . $key . "]=>[" . $item . "]";
            }
        }

        foreach ($final as &$row)
        {
            $row = str_ireplace(';', ',', $row);
        }

        $str = '';
        foreach ($final as $item)
        {
            $str .= iconv("UTF8", "CP1251//TRANSLIT//IGNORE", $item) . ";";
        }

        $str .= PHP_EOL;

        return $str;

    }
}